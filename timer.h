/* 
 * File:   timer.h
 * Author: cls
 *
 * Created on June 23, 2015, 10:39 AM
 */

#ifndef TIMER_H
#define	TIMER_H

// Timer structure
typedef struct
{
    unsigned short int count;
    unsigned short int period;
    unsigned reset   :1;
    unsigned         :15;
    unsigned done    :1;
    unsigned         :15;
} timer_t;

// Define the number of timers and timer names:
#define NUMBER_OF_TIMERS            9

#define MAIN_LOOP_TIMER             0
#define DETECTOR_OPS_TIMER          1
#define HUNDRETH_SECOND_TIMER       2
#define TENTH_SECOND_TIMER          3
#define HALF_SECOND_TIMER           4
#define ONE_SECOND_TIMER            5
#define COMPENSATION_TIMER          6
#define EEPROM_WRITE_TIMER          7
#define BUTTON_S1_TIMER             8

// Define timer periods in millisconds
#define MAIN_LOOP_TIMER_PERIOD            10    // 10 ms
#define DETECTOR_OPS_PERIOD_5ms            5    //  5 ms
#define DETECTOR_OPS_PERIOD_2ms            2    //  2 ms
#define HUNDRETH_SECOND_TIMER_PERIOD      10
#define TENTH_SECOND_TIMER_PERIOD        100
#define HALF_SECOND_TIMER_PERIOD         500
#define ONE_SECOND_TIMER_PERIOD         1000
#define COMPENSATION_TIMER_PERIOD       1000    //  1 sec
#define EEPROM_WRITE_TIMER_PERIOD          5    //  5 ms
#define BUTTON_S1_TIMER_PERIOD          2000    //  2 sec



// Global variables
volatile timer_t timers[NUMBER_OF_TIMERS];
#define HALF_SECOND_BLINK   timers[HALF_SECOND_TIMER].done


// Global function declarations
inline void update_timers(void);
void initalize_timers(void);



#endif	/* TIMER_H */

