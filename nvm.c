#include <xc.h>
#include "nvm.h"
#include "main.h"
#include "ctl.h"
#include "eeprom.h"
#include "CAN/Header/CO_OD.h"
#include "CAN/Header/application.h"



#define PARAM_RESET_START_DELAY     100 // 100 loop cycles, or 1 second
#define PARAM_RESET_LED_DELAY       100 // 100 loop cycles, or 1 second



systemData_t systemData;
static unsigned int paramResetTimer = 0;
static unsigned char resetDone = 0;
static unsigned char waitingToWrite = FALSE;



// ------------------------------------------------------------------------------------------------
//  Load data saved in external EEPROM to local RAM.
//  Check data for errors and load the defaults if needed.
//
int load_saved_parameters(void)
{
    unsigned char calcCrc, strLen;
    unsigned int sizeWithoutCrc, i;

    // Calculate the size of the system data structure not including the CRC field.
    // "sizeof()" may return a value larger then expected due to memory and compiler limitations,
    // so "offsetof()" is used to determine the size of the structure not including the CRC field.
    //size = sizeof(systemData_t);
    sizeWithoutCrc = offsetof(systemData_t, crc);

    // Load System operating parameters
    read_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

    // <editor-fold defaultstate="collapsed" desc="Check for un-initialized memory (all bytes read 0xFF)">
    // If memory is un-initialized, load the defaults.
    unsigned char *tmpPtr = (unsigned char*)&systemData;
    unsigned char tmpChar;
    unsigned char initailized = FALSE;
    for (i=0; i<sizeWithoutCrc; i++)
    {
        tmpChar = *(tmpPtr+i);
        if (tmpChar != 0xFF)
        {
            initailized = TRUE;
            break;
        }
    }
    
    if (initailized == FALSE)
    {
        // Load default values for first time use.
        systemData.systemID = SYSTEM_ID_DEF;
        systemData.softwareVersion = SOFTWARE_VERSION_DEF;

        // Default sensor settings
        systemData.sensorGap = OD_sensorGap;
        systemData.sensorMaxBeam = OD_sensorMaxBeam;
        systemData.sensorType = OD_sensorType;
        strLen = sizeof(OD_manufacturerDeviceName);
        for (i=0; i<strLen; i++)
            systemData.sensorString[i] = OD_manufacturerDeviceName[i];

        // Default calibration data
        for(i=0; i<RCV_ARRAY_SIZE; i++)
            systemData.receiveArray[i] = OD_calibrationReceiveValues[i] = 200;
        for(i=0; i<XMT_ARRAY_SIZE; i++)
            systemData.transmitArray[i] = OD_calibrationPowerLevels[i] = XMT_LEVEL_DEFAULT[systemData.sensorGap][systemData.sensorType];
        systemData.sensorBlockageLevel = OD_calibrationOpacity = 0;

        // Calculate the new CRC.
        calcCrc = crc8_slow((unsigned char*)&systemData, sizeWithoutCrc);
        systemData.crc = calcCrc;

        // Save default system data to EEPROM (including new crc).
        write_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

        delay_us(100);

        // Reload System operating parameters
        read_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));
    }
    // </editor-fold>
    
    // Calculate the expected CRC value for the data.
    calcCrc = crc8_slow((unsigned char*)&systemData, sizeWithoutCrc);

    // If CRC values do not match, try up to 5 times to load the system data (and get a matching CRC)...
    unsigned char readAttempts = 1;
    while(calcCrc != systemData.crc)
    {
        read_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));
        sizeWithoutCrc = offsetof(systemData_t, crc);
        calcCrc = crc8_slow((unsigned char*)&systemData, sizeWithoutCrc);
        readAttempts++;
        if (readAttempts >= 5)  
            break;
    }

    // If the CRC value still does not match, then the saved data is corrupt.
    if(calcCrc != systemData.crc)
    {
        // Copy stored data to Object Dictionary RAM anyway
        OD_sensorType = systemData.sensorType;
        OD_sensorGap = systemData.sensorGap;
        OD_sensorMaxBeam = systemData.sensorMaxBeam;
        // Copy sensor description.
        strLen = sizeof(OD_manufacturerDeviceName);
        for (i=0; i<strLen; i++)
            OD_manufacturerDeviceName[i] = systemData.sensorString[i];
        // Load calibration data
        for(i=0; i<XMT_ARRAY_SIZE; i++)
            OD_calibrationPowerLevels[i] = systemData.transmitArray[i];
        for(i=0; i<RCV_ARRAY_SIZE; i++)
            OD_calibrationReceiveValues[i] = systemData.receiveArray[i];
        OD_calibrationOpacity = systemData.sensorBlockageLevel;

        // There has been a fatal memory error
        return -1;
    }

    // Saved data has loaded correctly.  Initialize non-volatile CANOpen variables.

    // Copy stored data to Object Dictionary RAM
    OD_sensorType = systemData.sensorType;
    OD_sensorGap = systemData.sensorGap;
    OD_sensorMaxBeam = systemData.sensorMaxBeam;
    // Copy sensor description.
    strLen = sizeof(OD_manufacturerDeviceName);
    for (i=0; i<strLen; i++)
        OD_manufacturerDeviceName[i] = systemData.sensorString[i];
    // Load detector calibration data
    for(i=0; i<XMT_ARRAY_SIZE; i++)
        OD_calibrationPowerLevels[i] = systemData.transmitArray[i];
    for(i=0; i<RCV_ARRAY_SIZE; i++)
        OD_calibrationReceiveValues[i] = systemData.receiveArray[i];
    OD_calibrationOpacity = systemData.sensorBlockageLevel;
    
    return 0;
}


// ------------------------------------------------------------------------------------------------
//
int load_default_parameters(void)
{
    unsigned char calcCrc, strLen;
    unsigned int sizeWithoutCrc, i;

    // Calculate the size of the system data structure not including the CRC field.
    // "sizeof()" may return a value larger then expected due to memory and compiler limitations,
    // so "offsetof()" is used to determine the size of the structure not including the CRC field.
    //size = sizeof(systemData_t);
    sizeWithoutCrc = offsetof(systemData_t, crc);

    // Load saved operating parameters
    read_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

    // Load default values for first time use.
    systemData.systemID = SYSTEM_ID_DEF;
    systemData.softwareVersion = SOFTWARE_VERSION_DEF;

    // Default sensor settings
    //systemData.sensorGap = OD_sensorGap;
    //systemData.sensorMaxBeam = OD_sensorMaxBeam;
    //systemData.sensorType = OD_sensorType;
    //strLen = sizeof(OD_manufacturerDeviceName);
    //for (i=0; i<strLen; i++)
    //    systemData.sensorString[i] = OD_manufacturerDeviceName[i];

    // Default calibration data
    for(i=0; i<RCV_ARRAY_SIZE; i++)
        systemData.receiveArray[i] = 200;
    for(i=0; i<XMT_ARRAY_SIZE; i++)
        systemData.transmitArray[i] = XMT_LEVEL_DEFAULT[systemData.sensorGap][systemData.sensorType];
    systemData.sensorBlockageLevel = 0;

    // Calculate the new CRC.
    calcCrc = crc8_slow((unsigned char*)&systemData, sizeWithoutCrc);
    systemData.crc = calcCrc;

    // Save default system data to EEPROM (including new crc).
    write_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

    delay_ms(5);

    // Reload System operating parameters
    read_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

    // Calculate the expected CRC value for the data.
    calcCrc = crc8_slow((unsigned char*)&systemData, sizeWithoutCrc);

    // If CRC values do not match...
    if(calcCrc != systemData.crc)
    {
        // Memory Read/Write Failure.
        return -1;
    }
    else
    {
        // Copy stored data to Object Dictionary (RAM)
        OD_sensorType = systemData.sensorType;
        OD_sensorGap = systemData.sensorGap;
        OD_sensorMaxBeam = systemData.sensorMaxBeam;
        // Copy sensor description.
        strLen = sizeof(OD_manufacturerDeviceName);
        for (i=0; i<strLen; i++)
            OD_manufacturerDeviceName[i] = systemData.sensorString[i];
        // Copy Calibration Data
        for (i=0; i<XMT_ARRAY_SIZE; i++)
            OD_calibrationPowerLevels[i] = systemData.transmitArray[i];
        for (i=0; i<RCV_ARRAY_SIZE; i++)
            OD_calibrationReceiveValues[i] = systemData.receiveArray[i];
        OD_calibrationOpacity = systemData.sensorBlockageLevel;
        
        return 0;
    }
}

/*
// ------------------------------------------------------------------------------------------------
//  Save local data to EEPROM.
//
void save_all_parameters(void)
{
    unsigned char calcCrc, strLen;
    unsigned int sizeWithoutCrc, i;

    // Load System operating parameters
    read_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

    // Calculate the size of the data structure not including the CRC field.
    // "sizeof()" may return a value larger then expected due to memory and compilier limitations,
    // so "offsetof()" is used to determine the size of the structure not including the CRC field.
    //size = sizeof(systemData_t);
    sizeWithoutCrc = offsetof(systemData_t, crc);

    // Calculate the expected CRC value
    calcCrc = crc8_slow(&systemData, sizeWithoutCrc);

    // Compare the expected CRC value to the stored CRC value.
    // If they do not match, the the saved data is corrupt and will be overwritten by the defaults.
    if ( calcCrc != systemData.crc )
    {
        // Reset calibration arrays, load default sensor type and length.
        load_default_system_data_to_RAM();
        systemData.systemID = SYSTEM_ID_DEF;
        systemData.softwareVersion = SOFTWARE_VERSION_DEF;

        // Calculate the new CRC.
        calcCrc = crc8_slow(&systemData, sizeWithoutCrc);
        systemData.crc = calcCrc;

        // Save system data to EEPROM (including new crc).
        write_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));
    }


    // Copy saved values to Object Dictionary RAM
    //OD_CANNodeID = 0x63;                    // Default Device ID is always 99 (0x63) on power-up.
    //OD_sensorType = systemData.sensorType;
    //OD_sensorGap = systemData.sensorGap;
    //OD_sensorMaxBeam = systemData.sensorMaxBeam;

    //strLen = sizeof(OD_manufacturerDeviceName);
    //for (i=0; i<strLen; i++)
    //    OD_manufacturerDeviceName[i] = systemData.sensorString[i];

}
*/

// ------------------------------------------------------------------------------------------------
//
void check_for_system_reset(unsigned char buttonPress)
{
    unsigned char resetParams = FALSE;
    //unsigned char calcCrc;
    //unsigned int sizeWithoutCrc;

    // Load and save default parameters if the button is pressed for more then 1 second.
    if (buttonPress)
    {
        if (paramResetTimer == PARAM_RESET_START_DELAY)
            resetParams = TRUE;
        else
            paramResetTimer++;
    }
    else
    {
        if (paramResetTimer > PARAM_RESET_LED_DELAY)
            paramResetTimer = PARAM_RESET_LED_DELAY;

        if (paramResetTimer == 0)
        {
            resetParams = FALSE;
            resetDone = FALSE;
        }
        else
            paramResetTimer--;
    }

    if (resetParams)
    {
        // Red LED on
        LED1_RED = 1;
        LED1_GREEN = 0;

        if (!resetDone)
        {
            // Reset CAN state machine.
            OD_CANNodeID = 0x63;                  // Default Device ID = 99 (0x63)
            reset_CAN_node();

            resetDone = TRUE;
        }
    }
}


// ------------------------------------------------------------------------------------------------
//
void check_NVM_values(void)
{
    unsigned char newValues = FALSE;
    unsigned char i, strLen, calcCrc;
    unsigned int sizeWithoutCrc;

    // Check if values have been updated.
    if(systemData.sensorType != OD_sensorType)
    {
        systemData.sensorType = OD_sensorType;
        newValues = TRUE;

        // Update the Unique Device ID based on the type of sensor that is connected.
        if (OD_sensorType == led)
            OD_IDENT.identDevice = 0xB9;    // Infrared Sensor ID = 185.
        else
            OD_IDENT.identDevice = 0xA9;    // Ultrasonic Sensor ID = 169.
    }

    if(systemData.sensorGap != OD_sensorGap)
    {
        systemData.sensorGap = OD_sensorGap;
        newValues = TRUE;
    }

    if(systemData.sensorMaxBeam != OD_sensorMaxBeam)
    {
        systemData.sensorMaxBeam = OD_sensorMaxBeam;
        newValues = TRUE;
    }

    strLen = sizeof(OD_manufacturerDeviceName);
    for (i=0; i<strLen; i++)
    {
        if(systemData.sensorString[i] != OD_manufacturerDeviceName[i])
        {
            systemData.sensorString[i] = OD_manufacturerDeviceName[i];
            newValues = TRUE;
        }
        // Exit loop if the end of the string is detected.
        if(OD_manufacturerDeviceName[i] == 0x00)
            break;
    }
    
    // Save the new values
    if(newValues)
    {
        // Fill in trailing 0's after first null character in string.
        strLen = sizeof(OD_manufacturerDeviceName);
        while(i<strLen)
        {
            systemData.sensorString[i] = OD_manufacturerDeviceName[i] = 0x00;
            i++;
        }

        // Calculate the new crc value for system data.
        sizeWithoutCrc = offsetof(systemData_t, crc);
        calcCrc = crc8_slow((unsigned char*)&systemData, sizeWithoutCrc);
        systemData.crc = calcCrc;

        //if(eepromTaskIsIdle())
        //{
            // Save system parameters to EEPROM.
            //start_eeprom_write_task((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

            write_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

            // Clear the wait flag.
        //    waitingToWrite = FALSE;
        //}
        //else
        //{
            // EEPROM is busy - set flag to write later.
        //    waitingToWrite = TRUE;
        //}
    }

/*
    if ( (waitingToWrite == TRUE) && eepromTaskIsIdle() )
    {
        // Perform the delayed write operation
        start_eeprom_write_task((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

        // Clear the wait flag.
        waitingToWrite = FALSE;
    }
*/
}