/* 
 * File:   detector.h
 * Author: cls
 *
 * Created on June 23, 2015, 11:21 AM
 */

#ifndef DETECTOR_H
#define	DETECTOR_H


// PEAK_DETECTOR_WINDOW = (L * V * T)
// L = acoustic path length (Transducer air gap in inches)
// V = sound velocity = 74 usec per inch @ 68 degrees F
// T = -20 deg F adjustment = sqrt(293 deg K / 244 deg K) = 1.1

//#define ULT_PEAK_DETECT_CAPTURE_DELAY	500     // 500us
//#define TIME_OF_FLIGHT_8_INCH           592     // 8 inch gap sensor
//#define TIME_OF_FLIGHT_4_INCH           296     // 4 inch gap sensor
//#define TIME_OF_FLIGHT_3_INCH           222     // 3 inch gap sensor
//#define TIME_OF_FLIGHT_1P5_INCH         111     // 1.5 inch gap sensor

#define IR_PEAK_DETECT_CAPTURE_DELAY     50     //  50us

#define COMP_ARRAY_SIZE                  16

#define POINT_SOURCE_GUIDING_RANGE       64     // ~ 1.6mm
#define POINT_SOURCE_SENSING_RANGE      127     // ~ 3.6mm

#define EDGE_LOSS_SLOP                    4     // ~ 0.1mm

#define MINIMUM_BLOCKAGE                 13     // Minimim amount of blockage needed to consider 
                                                // a valid edge found (5% of 255 = 12.75).

// *** Special types ***
typedef enum {
    INIT = 0,
    EDGE_A,
    EDGE_B,
    COMPENSATION,
    IDLE
} sensorState_t;

typedef struct {
    unsigned char beamA;
    unsigned char lastBeamA;
    unsigned char blockageA;
    unsigned char lastBlockageA;
    unsigned char edgeAFound;
    unsigned int edgeAPosition;

    unsigned char beamB;
    unsigned char lastBeamB;
    unsigned char blockageB;
    unsigned char lastBlockageB;
    unsigned char edgeBFound;
    unsigned int edgeBPosition;

    unsigned char compBeam;
    unsigned char compArray[COMP_ARRAY_SIZE];
    unsigned char compIndex;
    unsigned char compensation;

    unsigned char maxBeam;
    unsigned char minBlockage;
    unsigned int maxPosition;
    unsigned int minPosition;
    unsigned int maxGuidePoint;
    unsigned int minGuidePoint;

    sensorState_t state;
    sensorState_t lastState;
    unsigned char newPositionVals;
    unsigned char type;
} sensor_t;

typedef enum {
    INITALIZING = 0,
    FIND_WEB,
    GUIDING,
    GUIDING_ON_SENSOR_1,
    GUIDING_ON_SENSOR_2,
    BOTH_EDGES_LOST
} systemState_t;



// *** Variables - Global Scope ***
sensor_t sensorOne;
enum
{
    NO_COMP,
    SELECT_COMP_BEAM,
    READ_COMP_BEAM
} compState;




// *** Functions - Global Scope ***
void detector_operations(void);
void select_beam(unsigned char beam);
unsigned char get_peakDetect_signal_8bit(unsigned char beam);
unsigned int get_peakDetect_signal_10bit(unsigned char beam);



#endif	/* DETECTOR_H */

