#include "timer.h"



// ------------------------------------------------------------------------------------------------
//  Updates the timer array used for general-purpose timing.  Timers count up from zero and reset
//  when count equals period.
//
inline void update_timers(void)
{
    unsigned char i;

    for(i=0; i<NUMBER_OF_TIMERS; i++)
    {
        if(timers[i].reset)
        {
            // Reset timer.
            timers[i].count = 0;
            // Clear flag.
            timers[i].reset = 0;
        }
        else
        {
            // Increment timer.
            timers[i].count++;

            // Check elapsed time.
            if(timers[i].count >= timers[i].period)
            {
                timers[i].count = 0;    // Reset timer
                timers[i].done = 1;     // Set flag - time is up!
            }
        }
    }
}


// ------------------------------------------------------------------------------------------------
//
void initalize_timers(void)
{
    unsigned char i;

    // Configure all timer periods.
    timers[MAIN_LOOP_TIMER].period = MAIN_LOOP_TIMER_PERIOD;
    timers[DETECTOR_OPS_TIMER].period = DETECTOR_OPS_PERIOD_5ms;
    timers[HUNDRETH_SECOND_TIMER].period = HUNDRETH_SECOND_TIMER_PERIOD;
    timers[TENTH_SECOND_TIMER].period = TENTH_SECOND_TIMER_PERIOD;
    timers[HALF_SECOND_TIMER].period = HALF_SECOND_TIMER_PERIOD;
    timers[ONE_SECOND_TIMER].period = ONE_SECOND_TIMER_PERIOD;
    timers[COMPENSATION_TIMER].period = COMPENSATION_TIMER_PERIOD;
    timers[EEPROM_WRITE_TIMER].period = EEPROM_WRITE_TIMER_PERIOD;
    timers[BUTTON_S1_TIMER].period = BUTTON_S1_TIMER_PERIOD;


    // Reset and start all timers.
    for(i=0; i<NUMBER_OF_TIMERS; i++)
    {
        timers[i].count = 0;
        timers[i].done = 0;
    }

}
