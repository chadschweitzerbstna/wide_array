#include <xc.h>

#include <proc/p32mz1024eff100.h>

#include <peripheral/system.h>
#include <stddef.h>
#include "main.h"
#include "timer.h"
#include "delay.h"
#include "eeprom.h"
#include "nvm.h"
#include "CAN/Stack/CO_PDO.h"
#include "ctl.h"
#include "detector.h"
#include "calibrate.h"
#include "analog.h"
#include "CAN/Header/CO_OD.h"
#include "can.h"
#include "CAN/Header/application.h"
#include "CAN/Header/CO_driver.h"
#include "CAN/Stack/CO_NMT_Heartbeat.h"

// <editor-fold defaultstate="collapsed" desc="Configuration Bits for the PIC32">
// DEVCFG0
#pragma config DEBUG =      OFF
#pragma config ICESEL =     ICS_PGx2
#pragma config PWP =        0xff
#pragma config BWP =        OFF
#pragma config CP =         ON
// DEVCFG1
#pragma config FNOSC =      PRIPLL
#pragma config FSOSCEN =    ON
#pragma config IESO =       ON
#pragma config POSCMOD =    HS
#pragma config OSCIOFNC =   OFF
#pragma config FPBDIV =     DIV_8
#pragma config FCKSM =      CSDCMD
#pragma config WDTPS =      PS32768//16384//PS8192
#pragma config FWDTEN =     OFF
// DEVCFG2
#pragma config FPLLIDIV =   DIV_4
#pragma config FPLLMUL =    MUL_20
#pragma config FPLLODIV =   DIV_1
#pragma config UPLLIDIV =   DIV_4
#pragma config UPLLEN =     OFF
// DEVCFG3
#pragma config USERID =     0xffff
#pragma config FSRSSEL =    PRIORITY_7
#pragma config FMIIEN =     ON
#pragma config FETHIO =     ON
#pragma config FCANIO =     ON
#pragma config FUSBIDIO =   ON
#pragma config FVBUSONIO =  ON
// </editor-fold>

// Global variables:
unsigned int ledTimer = 0;
unsigned char lastMaterialSetupControlWord = 0;
unsigned char detOpCnt = 0;
volatile unsigned long buttonDebounce = 0;
volatile unsigned char buttonPressed = 0;       // High when pushbutton is pressed (after debounce time)
volatile unsigned char buttonPressedOneShot = 0; // Flag which is set on button state change from 
                                                // "not pressed" to "pressed".

// Global function declarations:
void sample_pushbutton(unsigned long msCounter);
void init_SPI3(void);
void check_for_calibration_cmd(void);
void init_timer1(void);
void __attribute__((vector(_TIMER_1_VECTOR), interrupt(ipl4AUTO), nomips16)) _timer1_interrupt(void);
void init_IO(void);
void init_interrupts(void);
void update_led(unsigned char nmtState, unsigned char err);
void update_sensor_range_objects(unsigned char maxBeam);
static void adjust_detector_loop_time(void);
unsigned char execute_on_button_press(void (*functionPtr1)(void), unsigned char buttonState);
void red_led_blink_memory_error(unsigned long msCounter);
void __attribute__((noreturn)) SoftReset(void);



// ------------------------------------------------------------------------------------------------
//  Main program
//
void main (void)
{
    // Configure processor wait states and cache.
    SYSTEMConfig((80000000ul), SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);

    // Disable JTAG and trace port (Frees-up these pins for other functions).
    DDPCONbits.JTAGEN = 0;
    DDPCONbits.TROEN = 0;

    // Clear watchdog timer.
    WDTCONbits.WDTCLR = 1;

    // Watchdog timer on.
    WDTCONbits.ON = 1;

    // NOTE: FOR DEBUG ONLY - Check reset source
//    unsigned long tmpLong;
//    tmpLong = RCON;
//    if (tmpLong != 0x0080)
//        RCON = 0x0000;

    // Initialize IO pins and set outputs to default state.
    init_IO();

    // Red LED on during initialization.
    //LED1_RED = 1;

    // Clear watchdog timer.
    WDTCONbits.WDTCLR = 1;
    
    // Initialize SPI for EEPROM.
    init_SPI3();

    // Initialize ADC.
    init_ADC();

    // Initialize timer 1 to generate an interrupt every millisecond.
    init_timer1();

    // Initialize multi-vectored interrupts.
    init_interrupts();

    // Enable all interrupts.
    __builtin_enable_interrupts();

    // Reset global millisecond counter.
    msCounter = 0;

    // Initialize and start all general-purpose timers
    initalize_timers();

    // Clear watchdog timer.
    WDTCONbits.WDTCLR = 1;

    // Wait for power to stabilize.
    delay_ms(100);

    // Clear watchdog timer.
    WDTCONbits.WDTCLR = 1;

    // Check EEPROM data.
    // Read saved data - if checksum is ok, load saved data to RAM.
    // If checksum is bad, load and save defaults.
    // NOTE: Timers and SPI must be initialized before reading/writing EEPROM.

    int status = 0;
    if(buttonPressed)
    {
        // Turn on the RED LED while the parameters are being defaulted.
        LED1_RED = 1;
        delay_ms(1000);
        
        // Clear watchdog timer.
        WDTCONbits.WDTCLR = 1;
        
        // <editor-fold defaultstate="collapsed" desc="Restore the Default Sensor Calibration Data">
        // Restore the default sensor calibration data. Note: Sensor length, gap, description, etc
        // are NOT overwritten when the defaults are loaded. This only writes default calibration 
        // values.
        status = load_default_parameters();

        if(status < 0)
        {
            // Clear button press one-shot flag.
            buttonPressedOneShot = FALSE;
            
            // Memory Error
            while(1)
            {           
                // Blink Red LED rapidly 3-times, pause, repeat.
                red_led_blink_memory_error(msCounter);
                
                // If pressed (after being released), reboot.
                if(buttonPressed && buttonPressedOneShot)
                {
                    LED1_RED = 0;
                    LED1_GREEN = 1;
                    while(buttonPressed)
                        WDTCONbits.WDTCLR = 1;
                    delay_ms(1000);
                    SoftReset();
                }

                // Clear watchdog timer.
                WDTCONbits.WDTCLR = 1;
            }
        }
        else
        {
            // Default calibration data loaded successfully. Display Green LED while button is held.
            LED1_RED = 0;
            LED1_GREEN = 1;
            while(buttonPressed)
                WDTCONbits.WDTCLR = 1;  // Clear watchdog timer.
            delay_ms(1000);
        }
        // </editor-fold>
    }
    else
    {
        // <editor-fold defaultstate="collapsed" desc="Load Saved Parameters from EEPROM">
        // Pushbutton is not pressed - load saved parameters from EEPROM.
        status = load_saved_parameters();

        // Check for errors
        if (status < 0)
        {
            // Memory Error
            while(1)
            {           
                // Blink Red LED rapidly 3-times, pause, repeat.
                red_led_blink_memory_error(msCounter);

                // If pressed, load the default parameters and reboot.
                if(buttonPressed == TRUE)
                {
                    LED1_RED = 0;
                    LED1_GREEN = 1;
                    while(buttonPressed)
                        WDTCONbits.WDTCLR = 1;
                    delay_ms(1000);
                    load_default_parameters();
                    SoftReset();
                }
                
                // Clear watchdog timer.
                WDTCONbits.WDTCLR = 1;
            }
        }
        // </editor-fold>
    }

    // Update the Unique Device ID based on the type of sensor that is connected.
    if (OD_sensorType == led)
        OD_IDENT.identDevice = 0xB9;    // Infrared Sensor ID = 185.
    else
        OD_IDENT.identDevice = 0xA9;    // Ultrasonic Sensor ID = 169.

    // Initialize CANopen processes.
    init_CANopen();
    reset_CAN_node();

    // Initialize sensor variables.
    sensorResetState = SENSOR_RESET_START;
    
    // Update min and max ranges for the sensor and object dictionary entries.
    update_sensor_range_objects(systemData.sensorMaxBeam);
    
    // Determine how many edges to get.
    if (systemData.sensorMaxBeam > 1)
        twoEdgeValues = 1;     // Wide Array - get two edges.
    else
        twoEdgeValues = 0;     // Point Source - only one edge value to get.

    // Initialize the EEPROM state machine.
    init_eeprom_task();

    // *** Main while loop ***
    while(1)
    {
        // <editor-fold defaultstate="collapsed" desc="Operations to do once every 10 milliseconds">
        // *** Operations to do once every 10 milliseconds ***
        if(timers[MAIN_LOOP_TIMER].done)
        {
            // Clear flag
            timers[MAIN_LOOP_TIMER].done = 0;

            // Clear watchdog timer.
            WDTCONbits.WDTCLR = 1;

            // If calibrating, do calibration tasks as required every 10ms.
            if (calState != CAL_IDLE)
                calibration_task();

            // Update the on-board bi-color LED.
            update_led(NMT.operatingState, OD_errorRegister);
            
            // Update min and max ranges for the sensor and object dictionary entries.
            update_sensor_range_objects(systemData.sensorMaxBeam);

            // Determine how many edges to get.
            if (systemData.sensorMaxBeam > 1)
                twoEdgeValues = 1;     // Wide Array - get two edges.
            else
                twoEdgeValues = 0;     // Point Source - only one edge value to get.

            // Reset Parameters and CAN if the white button (S1) is held for more then 2 seconds.
            check_for_system_reset(buttonPressed);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Detector operations (every 5ms or 2ms)">
        if ( timers[DETECTOR_OPS_TIMER].done )
        {
            // Clear flag
            timers[DETECTOR_OPS_TIMER].done = 0;

            // Do detector operations every 5ms when not calibrating.
            if (calState == CAL_IDLE)
                detector_operations();
        }

        // Adjust the detector loop period as needed depending on the detector type.
        adjust_detector_loop_time();
        // </editor-fold>

        // If the millisecond counter has been updated, set the msTick flag.
        // This flag is used by the various system tasks (below) to perform periodic operations
        // once every millisecond.
        if (msCounter != lastMsCounter)
        {
            lastMsCounter = msCounter;  // Update the previous value
            msTickFlag = 1;             // Set the msTick flag
        }

        // <editor-fold defaultstate="collapsed" desc="Operations to do as often as possible">
        // *** Operations to do as often as possible ***
        // Start the calibration process if the host has sent the calibration command.
        check_for_calibration_cmd();
        
        // CANopen operations
        do_CANopen_tasks(msTickFlag, POSITION_A, POSITION_B); // Pass the values of Edge 1 and Edge 2

        // Clear the msTickFlag.  The flag is only used for a single iteration through the main loop.
        msTickFlag = 0;

        // Check if non-volatile values in Object Directory have been updated
        // and need to be saved to EEPROM (only check when not calibrating).
        if(calState == CAL_IDLE)
            check_NVM_values();

        // This process handles reading and writing data bytes to external EEPROM.
        // An write to external EEPROM can take several milliseconds to complete (there is a 5ms
        // minimum write cycle time) so a state machine is used to handle reads and writes without
        // blocking other processes.
        //do_eeprom_task();
        // </editor-fold>
    }
  
}


// ------------------------------------------------------------------------------------------------
//  Controller loop-time varies between 2 and 5ms depending on the detector type.
//  Ultrasonic detectors sample every 5ms (for lower noise), and IR detectors sample every 2ms.
//
static void adjust_detector_loop_time(void)
{
    unsigned int oldPeriod, newPeriod;

    // Copy the current detector operations period.
    oldPeriod = timers[DETECTOR_OPS_TIMER].period;

    if (systemData.sensorType == led)
        newPeriod = DETECTOR_OPS_PERIOD_2ms;
    else
        newPeriod = DETECTOR_OPS_PERIOD_5ms;

    // Update the main loop period if necessary.
    if (newPeriod != oldPeriod)
    {
        __builtin_disable_interrupts();
        timers[DETECTOR_OPS_TIMER].period = newPeriod;
        __builtin_enable_interrupts();
    }
}


// ------------------------------------------------------------------------------------------------
//  Timer 1 compare-match interrupt.
//  This interrupt occurs every 1 ms.
//
void __attribute__((vector(_TIMER_1_VECTOR), interrupt(ipl4AUTO), nomips16)) _timer1_interrupt(void)
{
    // Increment global millisecond counter - ok to roll-over.
    msCounter++;

    // Increment the LED timer
    ledTimer++;
    if (ledTimer > 999)
        ledTimer = 0;

    // Set the flag to trigger CAN 1ms processes
    CAN_1msFlag = 1;

    // Update all of the general-purpose timers.
    update_timers();

    // Read/update the pushbutton state
    sample_pushbutton(msCounter);
    
    // Clear Timer 1 interrupt flag
    IFS0bits.T1IF = 0;
}



// ------------------------------------------------------------------------------------------------
//  Initialize timer 1 to generate a 1ms interrupt.
//
void init_timer1(void)
{
    // Disable and reset timer and interrupt.
    IEC0bits.T1IE = 0;      // Timer 1 interrupt off
    IFS0bits.T1IF = 0;      // Clear interrupt flag
    T1CON = 0x00000000;     // Timer configuration reset and stop
    TMR1 = 0;               // Clear count

    // Set timer clock source and period register to achieve a 1ms period.
    T1CONbits.TCKPS0 = 1;   // Divide peripheral clock by 8
    PR1 = 1250;             // 1250 counts @ 1250kHz clock = 1 ms

    // Set timer 1 interrupt on compare match.
    IPC1bits.T1IP = 4;      // Set interrupt priority level to 4
    IPC1bits.T1IS = 1;      // Set interrupt sub-priority level to 1
    IEC0bits.T1IE = 1;      // Enable timer 1 interrupt

    // Clear and Enable timer
    T1CONbits.ON = 1;
}


// ------------------------------------------------------------------------------------------------
//  Configure SPI3 for communication with the EEPROM chip (Microchip 25LC640-I/SN).
//
void init_SPI3(void)
{
    unsigned char tmpChar;

    // Configure SPI3
    SPI3CON = 0;            // Stop and reset SPI3 to the defaults.
    tmpChar = SPI3BUF;      // Clear the receive buffer, just to be safe.
    SPI3BRG = 0x2;          // SPI clock frequency = (FPB/2) / (BRG+1).
                            // With FPB = 10MHz and BRG = 2, FSCK = 1.66MHz.
    SPI3STATCLR = 0x40;     // Clear the Overflow Bit, just in case.
    SPI3CONbits.MSTEN = 1;  // Master mode
    SPI3CONbits.ON = 1;     // SPI ON
}


// ------------------------------------------------------------------------------------------------
//
//
void init_interrupts(void)
{
    // Reset interrupt control register.
    INTCON = 0x00000000;

    // Enable interrupt controller multi-vector mode.
    INTCONbits.MVEC = 1;
}


// ------------------------------------------------------------------------------------------------
//  Initialize all the IO pins.
//
void init_IO(void)
{
    // NOTE: All pins default to inputs at power-up.
    // Pins with analog functionality also default to analog.

    // Set all the analog pins to digital:
    AD1PCFG = 0xFFFF;

    // Select individual analog inputs:
    AD1PCFGbits.PCFG0 = 0;  // AN0 - PEAK DETECT

    // Select individual digital outputs:
    TRISGbits.TRISG6 = 0;  // /EECS
    TRISGbits.TRISG7 = 0;  // /FLCS
    TRISBbits.TRISB2 = 0;   // PEAK RESET
    TRISBbits.TRISB5 = 0;   // VBUSON
    TRISDbits.TRISD1 = 0;   // SCK3 output
    TRISDbits.TRISD3 = 0;   // SDO3 output
    TRISDbits.TRISD10 = 0;  // LED1 (RED)
    TRISDbits.TRISD11 = 0;  // LED1 (GREEN)
    
    TRISEbits.TRISE0 = 0;   // RCVA1
    TRISEbits.TRISE1 = 0;   // RCVA2
    TRISEbits.TRISE2 = 0;   // XMTA1
    TRISEbits.TRISE3 = 0;   // XMTA2
    TRISEbits.TRISE4 = 0;   // MUXA1
    TRISEbits.TRISE5 = 0;   // MUXA0
    TRISEbits.TRISE6 = 0;   // INDA1 - Detector A indicator green
    TRISEbits.TRISE7 = 0;   // INDA2 - Detector A indicator red

    // Set all outputs to their default state:
    VBUSON = 0;
    FLCS = 1;       // Flash Memory chip select (active low)
    EECS = 1;       // EEPROM chip select (active low)
    INDA1 = 0;
    INDA2 = 0;
    LED1_RED = 0;
    LED1_GREEN = 0;
    SDO3 = 0;
    SCK3 = 0;

    MUXA0 = 0;
    MUXA1 = 0;
    XMTA1 = 0;
    XMTA2 = 0;
    RCVA1 = 0;
    RCVA2 = 0;

    // Reset peak detect circuit.
    PEAK_RESET = 1;

    // Set proper initial state to energize Detector.
    PORTE &= XMT_RCV_A_CLEAR_and;
    PORTE |= XMT_RCV_A_LOW_or;
    MUXA0 = 1;              // BLOCKAGE_SELECT
}

// ------------------------------------------------------------------------------------------------
//  Check and debounce the pushbutton input
//
void sample_pushbutton(unsigned long msCounter)
{
    static unsigned long buttonDebounce = 0;
    static unsigned long lastCounter;
    static unsigned char lastState;
    
    // Sample the pushbutton every 10ms.
    if((msCounter-lastCounter) >= 10)
    {
        lastCounter = msCounter;
        
        // Check if S1 (the small white button) is being pressed down.
        if (!BUTTON_S1)
        {
            if (buttonDebounce == BUTTON_DEBOUNCE_COUNTS)
                buttonPressed = TRUE;
            else
                buttonDebounce++;
        }
        else
        {
            if (buttonDebounce == 0)
                buttonPressed = FALSE;
            else
                buttonDebounce--;
        }  
    }
    
    // Set one-shot flag
    if((buttonPressed == TRUE) && (lastState == FALSE))
        buttonPressedOneShot = TRUE;
        
    // Update last button press state.
    lastState = buttonPressed;
}


// ------------------------------------------------------------------------------------------------
//
void update_led(unsigned char nmtState, unsigned char err)
{
    // led timer counts to 999ms and then starts again at 0.

    // Green LED
    switch (nmtState)
    {
        case CO_NMT_PRE_OPERATIONAL:
            // Blinking Green LED
            if (ledTimer > 500)
                LED1_GREEN = 1;
            else
                LED1_GREEN = 0;
            break;

        case CO_NMT_OPERATIONAL:
            // Green LED On
            LED1_GREEN = 1;
            break;

        case CO_NMT_STOPPED:
            // Single Flash
            if (ledTimer > 900)
                LED1_GREEN = 1;
            else
                LED1_GREEN = 0;
            break;

        default:
            // LED off
            LED1_GREEN = 0;
            break;
    }

    // Red LED
    if ( (err) && (LED1_GREEN == 0) )
        LED1_RED = 1;
    else
        LED1_RED = 0;
    
}


// ------------------------------------------------------------------------------------------------
//
void check_for_calibration_cmd(void)
{
    // Calibration word state machine.
    if (OD_materialSetupControlWord != lastMaterialSetupControlWord)
    {
        // State change - time to do something.
        lastMaterialSetupControlWord = OD_materialSetupControlWord;

        // TODO: FOR DEBUG ONLY - Setup word history.
        //static unsigned char setupWordBuffer[16];
        //static unsigned char setupWordIndex;
        //setupWordBuffer[setupWordIndex++] = OD_materialSetupControlWord;
        //if (setupWordIndex == 16)
        //    setupWordIndex = 0;

        switch (OD_materialSetupControlWord)
        {
            case 0x55:  // Entering Calibration - Prepare for UNBLOCKED Calibration
                CalibrationProgress = 0;
                CalibrationValue = 0;
                OD_fieldValue[0] = 0;     // 9100,1 - Field Value
                OD_fieldValue[1] = 0;
                // Force MUX PDO (scannerlist) to Field Value index (object 0x9100)
                // so that field value is the next message sent to the EKR500 display.
                if (MUXPDO.scannerIndex < 13)
                    MUXPDO.scannerIndex = 13;
                break;
                
            case 0x02:  // Start doing UNBLOCKED calibration (or "Light" in BST terms)
                calState = INIT_CAL_UNBLOCKED;
                break;

            case 0x05:  // Exit Unblocked Calibration - Prepare for Blocked Calibration
                OD_fieldValue[0] = 0;     // 9100,1 - Field Value
                OD_fieldValue[1] = 0;
                // Force MUX PDO (scannerlist) to Field Value index (object 0x9100)
                // so that field value is the next message sent to the EKR500 display.
                if (MUXPDO.scannerIndex < 13)
                    MUXPDO.scannerIndex = 13;
                calState = INIT_CAL_BLOCKED;
                break;

            case 0x03:  // Start doing BLOCKED calibration (or "Dark" in BST terms)
                //calState = INIT_CAL_BLOCKED;
                //calSensor = 0;
                // Capture the blocked value.
                if (calState == STREAM_BLOCKED_VALUE)
                    calState = GET_BLOCKED_VALUE;
                break;

            case 0x06:  // Exit calibration - save BLOCKED calibration data.
                // Capture the blocked value.
                //if (calState == STREAM_BLOCKED_VALUE)
                //    calState = GET_BLOCKED_VALUE;


                CalibrationProgress = 0;
                CalibrationValue = 0;
                OD_fieldValue[0] = 0;     // 9100,1 - Field Value
                OD_fieldValue[1] = 0;
                MUXPDO.scannerIndex = 13;
                break;
        }
    }
    else
    {
        switch (OD_materialSetupControlWord)
        {
            case 0x55:  // In Calibration Mode - Ready for UNBLOCKED Calibration to start.
                break;

            case 0x02:  // Doing UNBLOCKED calibration (or "Light" in BST terms)
                // Update Field Value as this is used for the progress bar on the EKR500 display.
                OD_fieldValue[0] = CalibrationProgress;     // 9100,1 - Field Value
                OD_fieldValue[1] = CalibrationProgress;
                // Force MUX PDO (scannerlist) to Field Value index (object 0x9100)
                // so that field value is the next message sent to the EKR500 display.
                if (MUXPDO.scannerIndex < 13)
                    MUXPDO.scannerIndex = 13;
                break;

            case 0x05:  // Exit Unblocked Calibration - Prepare for Blocked Calibration
                // Update Field Value for blocked signal strenght on the EKR500 display.
                OD_fieldValue[0] = CalibrationValue;     // 9100,1 - Field Value
                OD_fieldValue[1] = CalibrationValue;
                // Force MUX PDO (scannerlist) to Field Value index (object 0x9100)
                // so that field value is the next message sent to the EKR500 display.
                if (MUXPDO.scannerIndex < 13)
                    MUXPDO.scannerIndex = 13;
                break;

            case 0x03:  // Doing BLOCKED calibration (or "Dark" in BST terms)
                // Update Field Value for blocked signal strenght on the EKR500 display.
                //OD_fieldValue[0] = CalibrationValue;     // 9100,1 - Field Value
                //OD_fieldValue[1] = CalibrationValue;
                // Force MUX PDO (scannerlist) to Field Value index (object 0x9100)
                // so that field value is the next message sent to the EKR500 display.
                //if (MUXPDO.scannerIndex < 13)
                //    MUXPDO.scannerIndex = 13;
                break;

            case 0x06:  // Exit calibration - save BLOCKED calibration data.
                break;
        }

    }

}


// ------------------------------------------------------------------------------------------------
//
void update_sensor_range_objects(unsigned char maxBeam)
{
    unsigned long tmpLong;
    unsigned int tmpInt;

    // Update min and max ranges.
    // First calculate the max guide point in thousandths of an inch.
    if (maxBeam == 1)                           // Point Source
        tmpInt = POINT_SOURCE_SENSING_RANGE;    // Max range in thou.
    else                                        // Wide Array
    {
        tmpInt = maxBeam;
        tmpInt = ((tmpInt-3)*200)/2;            // Array range based on number of beams.
    }

    // Now convert from thousandths of an inch to micrometers
    sensorMaxInMm = tmpInt;                     // Implicit conversion to signed long.
    sensorMaxInMm &= 0x7FFFFFFF;
    sensorMaxInMm = sensorMaxInMm * 127;
    sensorMaxInMm = sensorMaxInMm / 5;

    // If the value is larger then 5mm, round down to the nearest whole millimeter.
    if (sensorMaxInMm > 5000)
    {
        // Round down to the nearest mm for the min and max values,ie: 83.0 not 83.3.
        //tmpLong = sensorMaxInMm % 1000;
        //sensorMaxInMm -= tmpLong;

        // Round down to the nearest 0.5mm for the min and max values.
        tmpLong = sensorMaxInMm % 500;
        sensorMaxInMm -= tmpLong;
    }

    // Calculate the Minimum.
    sensorMinInMm = -sensorMaxInMm;

    // Update the Min and Max range for CAN Objects (in millimeters).
    OD_processValueMAX[0] = sensorMaxInMm;      // 9123,1 - Process Value MAX, edge 1
    OD_processValueMAX[1] = sensorMaxInMm;      // 9123,2 - Process Value MAX, edge 2
    OD_processValueMIN[0] = sensorMinInMm;      // 9121,1 - Process Value MIN
    OD_processValueMIN[1] = sensorMinInMm;      // 9121,2

    // Update the min and max guidepoint (in thousandths of an inch) used to determine edge loss.
    tmpLong = sensorMaxInMm;                    // Implicit conversion to unsigned long.
    tmpLong = tmpLong * 5;
    tmpLong = tmpLong / 127;
    tmpLong = tmpLong + 0x4000;
    sensorMaxInThou = tmpLong;
    sensorMinInThou = 0x8000 - sensorMaxInThou;
}


// ------------------------------------------------------------------------------------------------
// Blink Red LED rapidly 3-times, pause, repeat.
//
void red_led_blink_memory_error(unsigned long msCounter)
{
    static unsigned long lastCounterValue = 0;
    static unsigned char state = 0;
    
    switch(state)
    {
        case 1:     // LED ON for 50ms
        case 3:
        case 5:
            LED1_RED = 1;
            if((msCounter - lastCounterValue) >= 75)
            {
                state++;
                lastCounterValue = msCounter;
            }
            break;
            
        case 2:     // LED OFF for 175ms
        case 4:    
            LED1_RED = 0;
            if((msCounter - lastCounterValue) >= 300)
            {
                state++;
                lastCounterValue = msCounter;
            }
            break;

        case 6:     // LED OFF for 1225ms
            LED1_RED = 0;
            if((msCounter - lastCounterValue) >= 1500)
            {
                state++;
                lastCounterValue = msCounter;
            }
            break;
        
        default:
            lastCounterValue = msCounter;
            state = 1;
            break;
    }
}


// ------------------------------------------------------------------------------------------------
// Check for on-board pushbutton to be pressed. 
// If pressed, execute the designated function.
unsigned char execute_on_button_press(void (*functionPtr1)(void), unsigned char buttonState)
{
    static unsigned char buttonPress;
    
    // Debounce the button signal
    if(buttonState)
        buttonPress++;
    else
        buttonPress = 0;
    
    // Respond to button press
    if(buttonPress >= 10)
    {
        (*functionPtr1)();  // Execute the designated function.
        return 1;           // Return True.
    }
    else
        return 0;           // Return False.
}
                    

// ------------------------------------------------------------------------------------------------
// Perform a soft reset of the device
//
void __attribute__((noreturn)) SoftReset(void)
{
    __builtin_disable_interrupts();
    /* perform a system unlock sequence ,starting critical sequence*/
    SYSKEY = 0x00000000; //write invalid key to force lock
    SYSKEY = 0xAA996655; //write key1 to SYSKEY
    SYSKEY = 0x556699AA; //write key2 to SYSKEY
    /* set SWRST bit to arm reset */
    RSWRSTSET = 1;
    /* read RSWRST register to trigger reset */
    unsigned int dummy;
    dummy = RSWRST;
    /* prevent any unwanted code execution until reset occurs*/
    while(1);
}