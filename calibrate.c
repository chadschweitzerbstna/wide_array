#include "xc.h"
#include "main.h"
#include "calibrate.h"
#include "ctl.h"
#include "detector.h"
#include "nvm.h"
#include "analog.h"
#include "timer.h"
#include "eeprom.h"
#include "CAN/Header/CO_OD.h"


/* States for the sensor calibration process

    CAL_IDLE                   <--- Normal operation - no calibration occuring

    INIT_CAL_UNBLOCKED,
    GET_COURSE_UNBLOCKED_VALUE,  
    GET_FINE_UNBLOCKED_VALUE,     <-- Sensor UN-BLOCKED calibration routines
    SELECT_NEXT_BEAM,            

    INIT_CAL_BLOCKED,
    STREAM_BLOCKED_VALUE       <--- Sensor BLOCKED calibration routines
    GET_BLOCKED_VALUE          
*/

static unsigned char *pXmtLevel,
                     *pXmtArray,
                     *pRcvArray;

static unsigned char sensor_length,
                     sensor_type,
                     receiveTargetValue,
                     i,
                     beamNum,
                     valueWasHigh,
                     sensorBlockedFlag,
                     rcv_Min,
                     rcvTargetValue,
                     blockedArray[16],
                     blockedIndex;
                 
static unsigned int rcv_Ave,
                    rcv_Sum,
                    blockedVal,
                    unblockedVal;

//static unsigned char compensationReference[];

/*
    To start calibration process, set the sensor number (calSensor = 1 or 2) and
    set "calState" to INIT_CAL_UNBLOCKED or INIT_CAL_BLOCKED.
    The state will then change automatically as various aspects of the sensor calibration are performed.
    "calState" will return to CAL_IDLE when calibration is complete.
*/


// ------------------------------------------------------------------------------------------------
//  The calibration process state machine routine.
//
void calibration_task(void)
{
    unsigned char rcv_Sample;
    unsigned int tmpInt;
    unsigned char tmpChar;

    // Perform the appropriate calibration task.
    switch(calState)
    {
        case CAL_IDLE: // Idle state - calibration not in progress.
            break;

        case INIT_CAL_UNBLOCKED:
        case INIT_CAL_BLOCKED:
            // Blocked and unblocked calibration initialize the same way.

            // Initialize the local variables and pointers.
            pXmtArray = (unsigned char *)&systemData.transmitArray[0];
            pRcvArray = (unsigned char *)&systemData.receiveArray[0];
            sensor_length = systemData.sensorMaxBeam;
            sensor_type = systemData.sensorType;
            if ((sensor_type == led) && (systemData.sensorGap == 1))
                receiveTargetValue = MODIFIED_RCV_TARGET_VALUE;
            else
                receiveTargetValue = RCV_TARGET_VALUE;

            // Always start with beam number 0
            beamNum = 0;

            // Start sampling routine
            if (calState == INIT_CAL_UNBLOCKED)
            {
                calState = GET_COURSE_UNBLOCKED_VALUE; // Do un-blocked calibration
                sensorBlockedFlag = FALSE;
                CalibrationValueMin = 0xFFFF;
                CalibrationValueMax = 0x0000;
            }
            else
            {
                //calState = GET_BLOCKED_VALUE;   // Do blocked calibration
                calState = STREAM_BLOCKED_VALUE;
                sensorBlockedFlag = TRUE;
            }
            CalibrationProgress = 0;
            break;

       case GET_COURSE_UNBLOCKED_VALUE:
            // Cycle through the TX/RX pairs, first getting the approximate TX power needed to
            // achieve a rough RX signal >= 80% ( 255 * 0.8 = 204 ).
            if (calState != lastCalState)   // State change - initialize routine.
            {
                // Update the previous state variable.
                lastCalState = calState;

                // Align the transmit power level pointer with the appropriate location in the
                // transmit array.  Start at the minimum power level for this particular sensor.
                pXmtLevel = pXmtArray + beamNum;
                *pXmtLevel = XMT_LEVEL_MIN[sensor_type];

                // Select the first beam.
                select_beam(beamNum);
                break;
            }

            // Read the TX value.
            rcv_Sample = get_peakDetect_signal_8bit(beamNum);

            // If the signal level is >= 80%, or we are at the maximum TX level,
            // then this step is done.  Otherwise increase the transmit level and try again.
            if( (rcv_Sample >= receiveTargetValue) || (*pXmtLevel == XMT_LEVEL_MAX[sensor_type]) )
                calState = GET_FINE_UNBLOCKED_VALUE;    // Done!  Next state.
            else
                (*pXmtLevel)++;                         // Increase the power and try again.

            // Select the beam again.
            select_beam(beamNum);
            break;

        case GET_FINE_UNBLOCKED_VALUE:
            // Now fine-tune the power level to achieve exactly 80% power on the RX signal.
            if (calState != lastCalState)       
            {
                // First time here (state change).
                lastCalState = calState;    // Update the previous state variable.
                valueWasHigh = FALSE;       // Clear value high flag
                i = 0;
                rcv_Sum = 0;
            }

            // Get the average of 16 samples
            if (i<16)
            {
                rcv_Sum += get_peakDetect_signal_8bit(beamNum);  // Get result and add to sum.
                i++;
            }
            else
            {
                // Divide the sum by 16.
                rcv_Ave = rcv_Sum >> 4;

                // Reset index and sum.
                i = 0;
                rcv_Sum = 0;

                // Check average result, adjust power and repeat as necessary until
                // the signal exactly matches the target value (Typically 80% of 255).
                if(rcv_Ave > receiveTargetValue)
                {
                    // Reduce power and try again.
                    if(*pXmtLevel > XMT_LEVEL_MIN[sensor_type])
                    {
                        (*pXmtLevel)--;             // Reduce TX power and try again.
                        valueWasHigh = TRUE;        // Set flag for high value.
                    }
                    else                            // Transmit power already at the minimum,
                        calState = SELECT_NEXT_BEAM;// go to the next beam.
                }
                else if(rcv_Ave < receiveTargetValue)
                {
                    // Increase power, unless the previous reading was already high, or the
                    // transmit power is already at the max.  If so, then move to the next beam.
                    if( (valueWasHigh == TRUE) || (*pXmtLevel == XMT_LEVEL_MAX[sensor_type]) )
                        calState = SELECT_NEXT_BEAM;// Select the next beam.
                    else
                        (*pXmtLevel)++;             // Increase the power and try again.
                }
                else // The received signal is perfect - move to the next beam.
                    calState = SELECT_NEXT_BEAM;
            }
            // Select beam.
            select_beam(beamNum);
            break;

        case SELECT_NEXT_BEAM:
            // Update the previous state variable.
            if (calState != lastCalState)
                lastCalState = calState;

            // Save the receive results from the current beam before selecting the next beam.
            // (Transmit data is saved automatically by nature of the pointer usage).
            *(pRcvArray + beamNum) = rcv_Ave;

            // Update the minimum and maximum values for Display.
            if(rcv_Ave < CalibrationValueMin)
                CalibrationValueMin = rcv_Ave;

            if(rcv_Ave > CalibrationValueMax)
                CalibrationValueMax = rcv_Ave;

            CalibrationProgress = beamNum * 100;
            CalibrationProgress = CalibrationProgress/(systemData.sensorMaxBeam+1);

            // Check if there are any more beams left to process.
            if(beamNum < sensor_length)
            {
                beamNum++;                              // Select next beam
                calState = GET_COURSE_UNBLOCKED_VALUE;  // Repeat COARSE & FINE processes.
            }
            else
            {
                // No more beams to select.  UN-BLOCKED calibration is complete.
                // Trigger a sensor reset (which will also initialize the compensation array).
                sensorResetState = SENSOR_RESET_START;

                // Save data to EEPROM (including new crc).
                tmpChar = crc8_slow((unsigned char*)&systemData, offsetof(systemData_t,crc));
                systemData.crc = tmpChar;
                write_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

                // Update Object Dictionary arrays with new calibration data
                for (i=0; i<XMT_ARRAY_SIZE; i++)
                    OD_calibrationPowerLevels[i] = systemData.transmitArray[i];
                for (i=0; i<RCV_ARRAY_SIZE; i++)
                    OD_calibrationReceiveValues[i] = systemData.receiveArray[i];
                
                // UN-BLOCKED calibration done.
                calState = CAL_IDLE;

                CalibrationProgress = 100;
            }
            break;
 
        case STREAM_BLOCKED_VALUE:
            // Get the received signal strength from beam 0 while blocked, using the known power
            // level from the un-blocked calibration process above (which achieved 80% signal strength).
            if (calState != lastCalState) // First time here (state change)
            {
                lastCalState = calState;
                
                // Clear the rolling average buffer.
                for (i=0; i<16; i++)
                    blockedArray[i] = 0;
                blockedIndex = 0;

                // Select the beam (always Beam 0 for BLOCKED calibration).
                select_beam(0);
                break;
            }

            // Maintain a rolling average of signal readings.
            blockedArray[blockedIndex++] = get_peakDetect_signal_8bit(0); // Get signal strength and add to sum.
            if (blockedIndex >= 16)         // Roll-over index when needed.
                blockedIndex = 0;
            select_beam(0);                 // Select the beam again (always Beam 0 for BLOCKED).

            // Calculate the Rolling average for 16 samples.
            rcv_Sum = 0;
            for (i=0; i<16; i++)
                rcv_Sum += blockedArray[i];
            rcv_Sum = rcv_Sum >> 4;
            blockedVal = rcv_Sum;

            // CALIBRATION OPACITY for Display = ( 100 - ( (blocked * 100) / unblocked ) ).
            unblockedVal = systemData.receiveArray[0];  // Beam 0 received signal when unblocked
            tmpInt = blockedVal * 100;
            if (unblockedVal > 0)       // Protect from divide-by-zero
                tmpInt = tmpInt / unblockedVal;
            if (tmpInt > 100)
                tmpInt = 100;
            tmpInt = 100 - tmpInt;
            CalibrationValue = tmpInt;
            break;

        case GET_BLOCKED_VALUE:
            // Save the value from above.
            if (calState != lastCalState)
                lastCalState = calState;

            // We now have the received signal values for beam 0 for both blocked
            // and unblocked scenarios, both at the same transmit power level.
            unblockedVal = systemData.receiveArray[0];  // Beam 0 received signal when unblocked

            // Normalize the blocked value and store the result.
            // SENSOR_x_BLOCK_LEVEL = (blocked * 255) / unblocked.
            tmpInt = blockedVal * 255;
            if (unblockedVal > 0)             // Protect from divide-by-zero
                tmpInt = tmpInt / unblockedVal;
            if (tmpInt > 255)
                tmpInt = 255;
            systemData.sensorBlockageLevel = tmpInt;

            // Save data to EEPROM (including new crc).
            tmpChar = crc8_slow((unsigned char*)&systemData, offsetof(systemData_t, crc));
            systemData.crc = tmpChar;
            write_eeprom((unsigned char*)&systemData, SYSTEM_DATA_ADDR, sizeof(systemData));

            // Update Object Dictionary arrays with new calibration data
            OD_calibrationOpacity = systemData.sensorBlockageLevel;
            
            // BLOCKED calibration done
            // Trigger a sensor reset (which will also initialize the compensation array).
            sensorResetState = SENSOR_RESET_START;

            // Done with calibration!
            calState = CAL_IDLE;
            break;

        default:
            // Unknown state - switch to IDLE state.
            calState = CAL_IDLE;
            break;
    }   // end of state machine

}
