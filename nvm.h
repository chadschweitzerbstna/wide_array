/* 
 * File:   nvm.h
 * Author: cls
 *
 * Created on June 23, 2015, 9:12 AM
 */

#ifndef NVM_H
#define	NVM_H


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  CAN Data (stored in eeprom)
//
// *** See CAN files ***
// From CO_OD.h:
//      struct sCO_OD_EEPROM
//      {
//          unsigned char   FirstWord;
//          unsigned char   CANNodeID;     // Object 2101
//          unsigned char   LastWord;
//          unsigned char   CRC;
//      };


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  System and User Data (stored in eeprom)
//
#define RCV_ARRAY_SIZE      128
#define XMT_ARRAY_SIZE      128

typedef struct {
    unsigned int softwareVersion;                   // 4 bytes
    unsigned int systemID;                          // 4 bytes
    unsigned char receiveArray[RCV_ARRAY_SIZE];     // 128 bytes
    unsigned char transmitArray[XMT_ARRAY_SIZE];    // 128 bytes
    unsigned char sensorBlockageLevel;              // 1 byte
    unsigned char sensorType;                       // 1 byte
    unsigned char sensorGap;                        // 1 byte
    unsigned char sensorMaxBeam;                    // 1 byte
    unsigned char sensorString[32];                 // 32 bytes max
    unsigned char crc;                              // 1 byte
} systemData_t;

extern systemData_t systemData;



// Global Function Prototypes
void load_default_system_data_to_RAM(void);
int load_saved_parameters(void);
void check_for_system_reset(unsigned char buttonPress);
int load_default_parameters(void);


#endif	/* NVM_H */