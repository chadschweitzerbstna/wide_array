/* 
 * File:   main.h
 * Author: cls
 *
 * Created on June 23, 2015, 9:12 AM
 */

#ifndef MAIN_H
#define	MAIN_H

#include <proc/p32mz1024eff100.h>

// **** Part-specific definitions ****
#define SYSCLK                  80000000
#define PBCLK                   10000000

#define FLASH_SPEED_HZ          30000000    // Max Flash speed
#define PB_BUS_MAX_FREQ_HZ      80000000    // Max Peripheral bus speed

#define FW_VERSION_STRING       "3.0.0"     // NOTE: must be exactly 5 characters
#define DEFAULT_SENSOR_STRING   "uninitialized"
#define SYSTEM_ID_DEF           1
#define SOFTWARE_VERSION_DEF    1


#define TRUE                    1
#define FALSE                   0

// Friendly names for outputs:
#define EECS        LATGbits.LATG6  // EEPROM chip select (active low)
#define FLCS        LATGbits.LATG7  // Flash Memory chip select (active low)
#define PEAK_RESET  LATBbits.LATB2  // PEAK RESET
#define VBUSON      LATBbits.LATB5  // USB Vbus ON
#define SCK3        LATDbits.LATD1  // SPI3 SCK
#define SDO3        LATDbits.LATD3  // SPI3 SDO
#define LED1_RED    LATDbits.LATD10 // LED 1 RED
#define LED1_GREEN  LATDbits.LATD11 // LED 1 GREEN

#define RCVA1       LATEbits.LATE0  // Detector receive 1
#define RCVA2       LATEbits.LATE1  // Detector receive 2
#define XMTA1       LATEbits.LATE2  // Detector transmit 1
#define XMTA2       LATEbits.LATE3  // Detector transmit 2
#define MUXA1       LATEbits.LATE4  // MUXA1
#define MUXA0       LATEbits.LATE5  // MUXA0
#define INDA1       LATEbits.LATE6  // Detector A indicator green
#define INDA2       LATEbits.LATE7  // Detector A indicator red


// Friendy names for inputs:
#define USBID       PORTFbits.RF3   // USBID pin
#define SDI3        PORTDbits.RD2   // SPI3 SDI
#define BUTTON_S1   PORTDbits.RD9   // TEST input, Pushbutton S1


// Global variables and defines:
volatile unsigned char
                tenMsFlag,
                msTickFlag;

volatile unsigned long
                msCounter,          // A 32-bit global millisecond counter to be used for timing operations.
                lastMsCounter;

#define BUTTON_DEBOUNCE_COUNTS  5   // Number of main loop cycles to debounce push button inputs.
unsigned char S1Pressed;
unsigned int S1Debounce;
unsigned long lastTime;
signed long sensorMaxInMm,
            sensorMinInMm;

unsigned int sensorMaxInThou,
             sensorMinInThou;

unsigned char twoEdgeValues;

unsigned char sensorResetState;
#define SENSOR_RESET_START    162
#define SENSOR_RESET_DONE       0


#endif	/* MAIN_H */

