#include <xc.h>
#include <proc/p32mz1024eff100.h>
#include "analog.h"
#include "delay.h"


// ------------------------------------------------------------------------------------------------
//  Initialize the analog to digital converter for 10-bit single-ended operation.
//
void init_ADC(void)
{
    // Configure ADC1 for 10-bit unsigned integer output.
    AD1CON1 = 0x0000;

    // Voltage Reference Configuration: ADC+ = AVdd and ADC- = AVss.
    // Always use MUXA
    AD1CON2 = 0x0000;

    // ADC clock derived from Peripheral Bus Clock (PBCLK).
    // NOTE: A total of 12 TAD cycles are required to perform a complete conversion.
    //       TAD must be greater then 83.33 ns for the ADC to function correctly.
    //       PBCLK = 10MHz, or a period of 100ns
    //       With ADCS == 0, Tad = 2 * TPB = 200ns, which is greater then 83ns, so ADCS can be 0.
    //       Total conversion time = 12 * Tad = 2.4us
    AD1CON3 = 0x0000;
    AD1CON3bits.SAMC = 0x01;    // Auto-sample Time of 1 Tad.
    AD1CON3bits.ADCS = 0x00;    // ADC Conversion Clock Select bits (Tad = 2 * TPB when ADCS == 0)

    // Channel 0 positive input is AN0, Channel 0 negative input is ADC- (AVss).
    AD1CHS = 0x0000;

    // Turn on ADC module
    AD1CON1bits.ON = 1;

    // Start sampling.
    AD1CON1bits.SAMP = 1;
}


// ------------------------------------------------------------------------------------------------
//  Select the desired ADC channel.
//
void select_ADC_channel(unsigned char channel)
{
    // Select ADC1 MUXA positive input channel if it is not already selected.
    if (AD1CHSbits.CH0SA != channel)
    {
        AD1CHSbits.CH0SA = channel;

        // Wait for analog stage to stabilize
        delay_us(2);
    }
}


// ------------------------------------------------------------------------------------------------
//  Get the ADC result for the desired channel.
//
unsigned int get_ADC_result(unsigned char channel)
{
    unsigned int result;

    // Select ADC1 MUXA positive input channel if it is not already selected.
    if (AD1CHSbits.CH0SA != channel)
    {
        AD1CHSbits.CH0SA = channel;

        // Wait for analog stage to stabilize
        delay_us(2);
    }

    // Stop sampling and start conversion.
    AD1CON1bits.SAMP = 0;

    // Wait for conversion to complete
    while (!AD1CON1bits.DONE) {;;}

    // Get result
    result = ADC1BUF0;

    // Clear DONE bit and resume sampling.
    AD1CON1bits.DONE = 0;
    AD1CON1bits.SAMP = 1;

    // Return result (10-bit)
    return result;
}





