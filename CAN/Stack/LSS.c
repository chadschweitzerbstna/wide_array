


#include "../Header/CO_driver.h"
#include "CANopen.h"
#include "LSS.h"



// -------------------------------------------------------------------------------------------------
//
void CO_LSS_init(   unsigned char node_id, unsigned long vendor_id, unsigned long product_code,
                    unsigned long revision_number, unsigned long serial_number, unsigned char lss_lock)
{
    CO_LSS.DynamicNode = node_id;
    CO_LSS.vendor_id_default = vendor_id;
    CO_LSS.product_code_default = product_code;
    CO_LSS.revision_number_default = revision_number;
    CO_LSS.serial_number_default = serial_number;
    CO_LSS.lock = lss_lock;
    
    CO_LSS.NodeChangedDelayTimer = 0;
    CO_LSS.SwitchSelectiveVendorId = 0;
    CO_LSS.SwitchSelectiveProductCode = 0;
    CO_LSS.SwitchSelectiveRevisionNumber = 0;
    CO_LSS.SwitchSelectiveSerialNumber = 0;

    CO_LSS.state = 0;
    
}


// -------------------------------------------------------------------------------------------------
//
void CO_LSS_receive(CO_CANmsg_t RXmsg)
{
    CO_CANmsg_t LSS_reply;
    
    // Set COB_ID of reply message
    LSS_reply.COB_ID = LSS_SLAVE_TX_COB_ID;

    // Only perform LSS operations if we are not in the LSS_LOCK state
    CO_LSS.lock = OD_lssLock;   // Read the LSS LOCK variable
    if(CO_LSS.lock == 0)
    {
        // Switch based on received message data
        switch(RXmsg.data[0])
        {
            case LSS_SWITCH_GLOBAL:
            {
                switch(RXmsg.data[1])
                {
                    case LSS_SWITCH_TO_WAITING_STATE:
                        CO_LSS.state = LSS_STATE_WATING;
                        break;
                    case LSS_SWITCH_TO_CONFIGURATION_STATE:
                        CO_LSS.state = LSS_STATE_CONFIGURATION;
                        break;
                }
            }
            break;
            case LSS_SWITCH_SELECTIVE_VENDOR_ID:
            {
                CO_LSS.SwitchSelectiveVendorId = (unsigned long)RXmsg.data[1]
                                               | (unsigned long)(RXmsg.data[2] << 8)
                                               | (unsigned long)(RXmsg.data[3] << 16)
                                               | (unsigned long)(RXmsg.data[4] << 24);
                CO_LSS.SwitchSelectiveProductCode = 0;
                CO_LSS.SwitchSelectiveRevisionNumber = 0;
                CO_LSS.SwitchSelectiveSerialNumber = 0;
            }
            break;
            case LSS_SWITCH_SELECTIVE_PRODUCT_CODE:
            {
                CO_LSS.SwitchSelectiveProductCode = (unsigned long)RXmsg.data[1]
                                                  | (unsigned long)(RXmsg.data[2] << 8)
                                                  | (unsigned long)(RXmsg.data[3] << 16)
                                                  | (unsigned long)(RXmsg.data[4] << 24);
            }
            break;
            case LSS_SWITCH_SELECTIVE_REVISION_NUMBER:
            {
                CO_LSS.SwitchSelectiveRevisionNumber = (unsigned long)RXmsg.data[1]
                                                     | (unsigned long)(RXmsg.data[2] << 8)
                                                     | (unsigned long)(RXmsg.data[3] << 16)
                                                     | (unsigned long)(RXmsg.data[4] << 24);
            }
            break;
            case LSS_SWITCH_SELECTIVE_SERIAL_NUMBER:
            {
                CO_LSS.SwitchSelectiveSerialNumber = (unsigned long)RXmsg.data[1]
                                                   | (unsigned long)(RXmsg.data[2] << 8)
                                                   | (unsigned long)(RXmsg.data[3] << 16)
                                                   | (unsigned long)(RXmsg.data[4] << 24);
            }
            break;
        }

        if( (CO_LSS.SwitchSelectiveVendorId == CO_LSS.vendor_id_default)
            && (CO_LSS.SwitchSelectiveProductCode == CO_LSS.product_code_default)
            && (CO_LSS.SwitchSelectiveRevisionNumber == CO_LSS.revision_number_default)
            && (CO_LSS.SwitchSelectiveSerialNumber == CO_LSS.serial_number_default)   )
        {
            CO_LSS.state = LSS_STATE_CONFIGURATION;

            // Reset variables
            CO_LSS.SwitchSelectiveVendorId = 0;
            CO_LSS.SwitchSelectiveProductCode = 0;
            CO_LSS.SwitchSelectiveRevisionNumber = 0;
            CO_LSS.SwitchSelectiveSerialNumber = 0;

            // Response to the master
            LSS_reply.data[0] = LSS_SWITCH_SELECTIVE_SLAVE_REPLY;
            CO_CANsend(LSS_reply);
        }

        if(CO_LSS.state == LSS_STATE_CONFIGURATION)
        {
            switch(RXmsg.data[0])
            {
                case LSS_STORE_CONFIGURATION:
                {
                    LSS_reply.data[0] = LSS_STORE_CONFIGURATION;
                    LSS_reply.data[1] = LSS_ERROR_STORE_CONFIGUARTION_NOT_SUPPORTED;

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_CONFIGURE_NODE_ID:
                {
                    CO_LSS.DynamicNode = RXmsg.data[1];

                    // Set NodeChangedDelayTimer to 20 milliseconds.
                    // This timer will be decremented in the 1ms service routine.
                    // When the timer == 1, the node will be reset (with the new node ID).
                    CO_LSS.NodeChangedDelayTimer = 20;
                    LSS_reply.data[0] = LSS_CONFIGURE_NODE_ID;

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_CONFIGURE_BIT_TIMING:
                {
                    LSS_reply.data[0] = LSS_CONFIGURE_BIT_TIMING;
                    LSS_reply.data[1] = 0x01;	// Bit-timing not supported

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_INQUIRE_VENDOR_ID:
                {
                    LSS_reply.data[0] = LSS_INQUIRE_VENDOR_ID;
                    LSS_reply.data[1] = (unsigned char)CO_LSS.vendor_id_default;
                    LSS_reply.data[2] = (unsigned char)(CO_LSS.vendor_id_default >> 8);
                    LSS_reply.data[3] = (unsigned char)(CO_LSS.vendor_id_default >> 16);
                    LSS_reply.data[4] = (unsigned char)(CO_LSS.vendor_id_default >> 24);

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_INQUIRE_PRODUCT_CODE:
                {
                    LSS_reply.data[0] = LSS_INQUIRE_PRODUCT_CODE;
                    LSS_reply.data[1] = (unsigned char)CO_LSS.product_code_default;
                    LSS_reply.data[2] = (unsigned char)(CO_LSS.product_code_default >> 8);
                    LSS_reply.data[3] = (unsigned char)(CO_LSS.product_code_default >> 16);
                    LSS_reply.data[4] = (unsigned char)(CO_LSS.product_code_default >> 24);

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_INQUIRE_REVISION_NUMBER:
                {
                    LSS_reply.data[0] = LSS_INQUIRE_REVISION_NUMBER;
                    LSS_reply.data[1] = (unsigned char)CO_LSS.revision_number_default;
                    LSS_reply.data[2] = (unsigned char)(CO_LSS.revision_number_default >> 8);
                    LSS_reply.data[3] = (unsigned char)(CO_LSS.revision_number_default >> 16);
                    LSS_reply.data[4] = (unsigned char)(CO_LSS.revision_number_default >> 24);

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_INQUIRE_SERIAL_NUMBER:
                {
                    LSS_reply.data[0] = LSS_INQUIRE_SERIAL_NUMBER;
                    LSS_reply.data[1] = (unsigned char)CO_LSS.serial_number_default;
                    LSS_reply.data[2] = (unsigned char)(CO_LSS.serial_number_default >> 8);
                    LSS_reply.data[3] = (unsigned char)(CO_LSS.serial_number_default >> 16);
                    LSS_reply.data[4] = (unsigned char)(CO_LSS.serial_number_default >> 24);

                    CO_CANsend(LSS_reply);
                }
                break;
                case LSS_INQUIRE_NODE_ID:
                {
                    LSS_reply.data[0] = LSS_INQUIRE_NODE_ID;
                    LSS_reply.data[1] = (unsigned char)CO_LSS.DynamicNode;

                    CO_CANsend(LSS_reply);
                }
                break;
            }
        }
    }

}


