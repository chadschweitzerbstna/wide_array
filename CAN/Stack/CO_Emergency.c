/*******************************************************************************

   File - CO_Emergency.c
   CANopen Emergency object.

   Copyright (C) 2004-2008 Janez Paternoster

   License: GNU Lesser General Public License (LGPL).

   <http://canopennode.sourceforge.net>
*/
/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.


   Author: Janez Paternoster

*******************************************************************************/


#include "../Header/CO_driver.h"
#include "CO_SDO.h"
#include "CO_Emergency.h"



static unsigned char errMsgBuffer[20];



/*******************************************************************************
   Function: CO_ODF_1003

   Function for accessing _Pre-Defined Error Field_ (index 0x1003) from SDO server.

   For more information see topic <SDO server access function> in CO_SDO.h file.
*******************************************************************************
UNSIGNED32 CO_ODF_1003( void       *object,
                        UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16 *pLength,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData)
{
    CO_emergencyObject_t *EMpr;

    EMpr = (CO_emergencyObject_t*) object; // This is the correct pointer type of the first argument

    if(subIndex==0)
        pData = (const void*) &EMpr->preDefinedErrorFieldNumberOfErrors;

    if(dir == 0)
    {
        // Reading Object Dictionary variable
        if(subIndex > EMpr->preDefinedErrorFieldNumberOfErrors)
            return 0x08000024L;  // No data available.
    }
    else
    {
        // Writing Object Dictionary variable
        // only '0' may be written to subIndex 0
        if(subIndex==0)
        {
            if( *((UNSIGNED8*)dataBuff) != 0 )
                return 0x06090030L; // Invalid value for parameter
        }
        else
            return 0x06010002L;     // Attempt to write a read only object.
    }
    return CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);
}
*/

/*******************************************************************************
   Function: CO_ODF_1014

   Function for accessing _COB ID EMCY_ (index 0x1014) from SDO server.

   For more information see topic <SDO server access function> in CO_SDO.h file.
*******************************************************************************
UNSIGNED32 CO_ODF_1014( void       *object,
                        UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16 *pLength,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData)
{
    UNSIGNED8 *nodeId;
    UNSIGNED32 cobIdEmcy;
    UNSIGNED32 abortCode = 0;
    unsigned char *pDest;
    unsigned char *pSource;

    nodeId = (UNSIGNED8*) object; // this is the correct pointer type of the first argument

    cobIdEmcy = *((UNSIGNED32*) pData); // Read from object dictionary
    cobIdEmcy += *nodeId;

    //memcpySwap4((UNSIGNED8*)dataBuff, (UNSIGNED8*)&cobIdEmcy);
    pDest = (UNSIGNED8*)&dataBuff;
    pSource = (UNSIGNED8*)cobIdEmcy;
    *pDest = *pSource;
    *(pDest+1) = *(pSource+1);
    *(pDest+2) = *(pSource+2);
    *(pDest+3) = *(pSource+3);

    return abortCode;
}
*/

/******************************************************************************/
INTEGER16 CO_emergency_init(    UNSIGNED8   *errorStatusBits,
                                UNSIGNED8    errorStatusBitsSize,
                                UNSIGNED8   *errorRegister,
                                UNSIGNED32  *preDefinedErrorField,
                                UNSIGNED8    preDefinedErrorFieldSize,
                                UNSIGNED8    msgBufferSize,
                                UNSIGNED8    nodeId             )
{
    UNSIGNED8 i;

    if (msgBufferSize < 2)
        msgBufferSize = 2;

    // Create error message buffer
    EM.msgBuffer = &errMsgBuffer[0];

    EMpr.nodeId = nodeId;
    EMpr.reportBuffer = &EM;

    // *** Configure object variables ***
    EM.errorStatusBits = errorStatusBits;
    EM.errorStatusBitsSize = errorStatusBitsSize;
    if (errorStatusBitsSize < 6)
        return CO_ERROR_ILLEGAL_ARGUMENT;
    EM.msgBufferEnd = EM.msgBuffer + (msgBufferSize*8);
    EM.msgBufferWritePtr = EM.msgBuffer;
    EM.msgBufferReadPtr = EM.msgBuffer;
    EM.msgBufferFull = 0;
    EM.wrongErrorReport = 0;
    EM.errorReportBusy = 0;
    EM.errorReportBusyError = 0;
    EMpr.errorRegister = errorRegister;
    EMpr.preDefinedErrorField = preDefinedErrorField;
    EMpr.preDefinedErrorFieldSize = preDefinedErrorFieldSize;
    EMpr.preDefinedErrorFieldNumberOfErrors = 0;
    EMpr.inhibitEmergencyTimer = 0;

    // Clear error status bits
    for (i=0; i<errorStatusBitsSize; i++)
        EM.errorStatusBits[i] = 0;

    EMpr.nodeId = nodeId;

    return CO_ERROR_NO;
}


/******************************************************************************/
void CO_emergency_process( UNSIGNED8        NMTisPreOrOperational,
                           UNSIGNED16       timeDifference_100us,
                           UNSIGNED16       EMinhTime       )
{
    CO_emergencyReport_t *EM = EMpr.reportBuffer;
    UNSIGNED8 errorRegister;
    CO_CANmsg_t msg;

    // Check for errors from CAN driver (TX and RX errors).
    CO_CAN_check_for_errors();

    if(EM->errorReportBusyError)
    {
        CO_errorReport(EM, ERROR_ERROR_REPORT_BUSY, EM->errorReportBusyError);
        EM->errorReportBusyError = 0;
    }
    if(EM->wrongErrorReport)
    {
        CO_errorReport(EM, ERROR_WRONG_ERROR_REPORT, EM->wrongErrorReport);
        EM->wrongErrorReport = 0;
    }

    // Calculate Error register
    errorRegister = 0;
    // Generic error
    if( EM->errorStatusBits[5] )
        errorRegister |= 0x01;
    // Communication error (overrun, error state)
    if(EM->errorStatusBits[2] || EM->errorStatusBits[3])
        errorRegister |= 0x10;
    *EMpr.errorRegister = (*EMpr.errorRegister & 0xEE) | errorRegister;

    // Inhibit time
    if(EMpr.inhibitEmergencyTimer < EMinhTime)
        EMpr.inhibitEmergencyTimer += timeDifference_100us;

    // Send Emergency message.
    if( NMTisPreOrOperational && EMpr.inhibitEmergencyTimer >= EMinhTime
         && (EM->msgBufferReadPtr != EM->msgBufferWritePtr || EM->msgBufferFull) )
    {
        // Copy data from emergency buffer into CAN buffer and preDefinedErrorField buffer
        UNSIGNED8* EMdataPtr = EM->msgBufferReadPtr;
        UNSIGNED32 preDEF;
        UNSIGNED8* ppreDEF = (UNSIGNED8*) &preDEF;

        // Create the error message.
        // COB_ID = dynamic node Id + 0x80
        // data length is always 8
        // data byte 0 =
        // data byte 1 =
        // data byte 2 = Error Register, object 0x1001.
        // data byte 3 =
        // data byte 4 =
        // data byte 5 =
        // data byte 6 =
        // data byte 7 =

        msg.COB_ID = EMpr.nodeId;
        msg.dataLen = 8;
        msg.data[0] = *EMdataPtr;
        *(ppreDEF++) = *(EMdataPtr++);
        msg.data[1] = *EMdataPtr;
        *(ppreDEF++) = *(EMdataPtr++);
        msg.data[2] =  *(EMpr.errorRegister);  // Byte 2 is always the contents of the Error Register, object 0x1001.
        *(ppreDEF++) = *EMpr.errorRegister;
        EMdataPtr++;
        msg.data[3] = *EMdataPtr;
        *(ppreDEF++) = *(EMdataPtr++);
        msg.data[4] = *(EMdataPtr++);
        msg.data[5] = *(EMdataPtr++);
        msg.data[6] = *(EMdataPtr++);
        msg.data[7] = *(EMdataPtr++);

        // Update read buffer pointer and reset inhibit timer
        if (EMdataPtr == EM->msgBufferEnd)
            EM->msgBufferReadPtr = EM->msgBuffer;
        else
            EM->msgBufferReadPtr = EMdataPtr;
        EMpr.inhibitEmergencyTimer = 0;

        // Verify message buffer overflow, then clear full flag
        if (EM->msgBufferFull == 2)
        {
            EM->msgBufferFull = 0;
            CO_errorReport(EM, ERROR_EMERGENCY_BUFFER_FULL, 0);
        }
        else
            EM->msgBufferFull = 0;

        // Write to 'pre-defined error field' (object dictionary, index 0x1003)
        if (EMpr.preDefinedErrorField)
        {
            UNSIGNED8 i;

            if (EMpr.preDefinedErrorFieldNumberOfErrors < EMpr.preDefinedErrorFieldSize)
                EMpr.preDefinedErrorFieldNumberOfErrors++;
            // Shift existing error messeges down
            for (i=EMpr.preDefinedErrorFieldNumberOfErrors-1; i>0; i--)
                EMpr.preDefinedErrorField[i] = EMpr.preDefinedErrorField[i-1];
            // Add the new error message
            EMpr.preDefinedErrorField[0] = preDEF;
        }

        // Send CAN message
        CO_CANsend(msg);
    }
    return;
}


/******************************************************************************/
INTEGER8 CO_errorReport(CO_emergencyReport_t *EM, UNSIGNED8 errorBit, UNSIGNED16 errorCode, UNSIGNED32 infoCode)
{
    UNSIGNED8 index = errorBit >> 3;
    UNSIGNED8 bitmask = 1 << (errorBit & 0x7);
    UNSIGNED8 *errorStatusBits = &EM->errorStatusBits[index];
    UNSIGNED8 *msgBufferWritePtrCopy;

    // If error was allready reported, return
    if ( (*errorStatusBits & bitmask) != 0 )
        return 0;

    if(!EM)
        return -1;

    // If errorBit value not supported, send emergency 'ERROR_WRONG_ERROR_REPORT'
    if (index >= EM->errorStatusBitsSize)
    {
        EM->wrongErrorReport = errorBit;
        return -1;
    }

    // Set error bit
    if (errorBit)
        *errorStatusBits |= bitmask;    // Any error except NO_ERROR

    // Set busy flag.  If allready busy, prepare for emergency and return.
    __builtin_disable_interrupts();
    if (EM->errorReportBusy++)
    {
        EM->errorReportBusy--;
        __builtin_enable_interrupts();
        EM->errorReportBusyError = errorBit;
        return -3;
    }
    __builtin_enable_interrupts();

    // Verify buffer full
    if(EM->msgBufferFull)
    {
        EM->msgBufferFull = 2;
        EM->errorReportBusy--;
        return -2;
    }

    // Copy data for emergency message
    msgBufferWritePtrCopy = EM->msgBufferWritePtr;
    memcpySwap2(msgBufferWritePtrCopy, (UNSIGNED8*)&errorCode);
    msgBufferWritePtrCopy += 3;   //third bit is Error register - written later
    *(msgBufferWritePtrCopy++) = errorBit;
    memcpySwap4(msgBufferWritePtrCopy, (UNSIGNED8*)&infoCode);
    msgBufferWritePtrCopy += 4;

    // Update write buffer pointer
    if (msgBufferWritePtrCopy == EM->msgBufferEnd)
        EM->msgBufferWritePtr = EM->msgBuffer;
    else
        EM->msgBufferWritePtr = msgBufferWritePtrCopy;

    // Verify buffer full, clear busy flag and return
    if (EM->msgBufferWritePtr == EM->msgBufferReadPtr)
        EM->msgBufferFull = 1;
    EM->errorReportBusy--;
    
    return 1;
}


/******************************************************************************/
INTEGER8 CO_errorReset(CO_emergencyReport_t *EM, UNSIGNED8 errorBit, UNSIGNED16 errorCode, UNSIGNED32 infoCode)
{
   UNSIGNED8 index = errorBit >> 3;
   UNSIGNED8 bitmask = 1 << (errorBit & 0x7);
   UNSIGNED8 *errorStatusBits = &EM->errorStatusBits[index];
   UNSIGNED8 *msgBufferWritePtrCopy;

   // If error is allready cleared, return
   if ( (*errorStatusBits & bitmask) == 0 )
       return 0;

   if(!EM)
       return -1;

    // If errorBit value not supported, send emergency 'ERROR_WRONG_ERROR_REPORT'
    if (index >= EM->errorStatusBitsSize)
    {
        EM->wrongErrorReport = errorBit;
        return -1;
    }

    // Set busy flag.  If allready busy, return.
    __builtin_disable_interrupts();
    if (EM->errorReportBusy++)
    {
        EM->errorReportBusy--;
        __builtin_enable_interrupts();
        return -3;
    }
    __builtin_enable_interrupts();

    // Erase error bit
    *errorStatusBits &= ~bitmask;

    // Verify buffer full
    if(EM->msgBufferFull)
    {
        EM->msgBufferFull = 2;
        EM->errorReportBusy--;
        return -2;
    }

   // Copy data for emergency message
   msgBufferWritePtrCopy = EM->msgBufferWritePtr;
   *(msgBufferWritePtrCopy++) = 0;
   *(msgBufferWritePtrCopy++) = 0;
   *(msgBufferWritePtrCopy++) = 0;
   *(msgBufferWritePtrCopy++) = errorBit;
   memcpySwap4(msgBufferWritePtrCopy, (UNSIGNED8*)&infoCode);
   msgBufferWritePtrCopy += 4;

   // Update write buffer pointer
   if (msgBufferWritePtrCopy == EM->msgBufferEnd)
       EM->msgBufferWritePtr = EM->msgBuffer;
   else
       EM->msgBufferWritePtr = msgBufferWritePtrCopy;

   // Verify buffer full, clear busy flag and return
   if (EM->msgBufferWritePtr == EM->msgBufferReadPtr)
       EM->msgBufferFull = 1;
   EM->errorReportBusy--;

   return 1;
}


/******************************************************************************/
UNSIGNED8 CO_isError(CO_emergencyReport_t *EM, UNSIGNED8 errorBit, UNSIGNED16 dummy)
{
   UNSIGNED8 index = errorBit >> 3;
   UNSIGNED8 bitmask = 1 << (errorBit & 0x7);

   if( (EM->errorStatusBits[index] & bitmask) )
       return 1;

   return 0;
}
