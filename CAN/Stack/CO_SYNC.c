/*******************************************************************************

   File - CO_SYNC.c
   CANopen SYNC object.

*******************************************************************************/


#include "../Header/CO_driver.h"
#include "CO_NMT_Heartbeat.h"
#include "CO_SYNC.h"
#include "CANopen.h"
#include "CO_Emergency.h"





// -------------------------------------------------------------------------------------------------
//
int CO_SYNC_receive(CO_CANmsg_t msg)
{
    // SYNC message received.
   
    // Process the SYNC message if we are in the OPERATIONAL or PRE-OPERATIONAL operational state.
    if( (NMT.operatingState == CO_NMT_OPERATIONAL) || (NMT.operatingState == CO_NMT_PRE_OPERATIONAL) )
    {
        // Data length should be zero for a sync pulse.
        if(msg.dataLen != 0)
        {
            // Error if data lenght is non-zero.
            CO_errorReport(&EM, ERROR_SYNC_LENGTH, msg.dataLen);
            return CO_ERROR_NO;
        }

        // Valid Sync
        CO_SYNC.running = 1;
        CO_SYNC.timer = 0;
    }

    return CO_ERROR_NO;
}


// -------------------------------------------------------------------------------------------------
//
int CO_SYNC_init(  unsigned long ObjDict_COB_ID_SYNCMessage,
                            unsigned long ObjDict_communicationCyclePeriod,
                            unsigned char ObjDict_synchronousCounterOverflowValue  )
{
    //UNSIGNED8 len = 0;

    // Configure object variables
    CO_SYNC.isProducer = (ObjDict_COB_ID_SYNCMessage & 0x40000000L) ? 1 : 0;
    CO_SYNC.COB_ID = ObjDict_COB_ID_SYNCMessage & 0x7FF;

    CO_SYNC.periodTime = ObjDict_communicationCyclePeriod;
    CO_SYNC.periodTimeoutTime = (ObjDict_communicationCyclePeriod / 2) * 3;

    // overflow?
    if(CO_SYNC.periodTimeoutTime < ObjDict_communicationCyclePeriod)
        CO_SYNC.periodTimeoutTime = 0xFFFFFFFFL;

    //if(ObjDict_synchronousCounterOverflowValue)
    //    len = 1;

    CO_SYNC.curentSyncTimeIsInsideWindow = 1;
    
    CO_SYNC.running = 0;
    CO_SYNC.timer = 0;

    // Configure SDO server for first argument of CO_ODF_1005, CO_ODF_1006 and CO_ODF_1019.
    //CO_OD_configureArgumentForODF(0x1005, (void*)&CO_SYNC);
    //CO_OD_configureArgumentForODF(0x1006, (void*)&CO_SYNC);
 

    return CO_ERROR_NO;
}


// -------------------------------------------------------------------------------------------------
//   Function: CO_SYNC_process
//
//   Process SYNC communication.
//
//   Function must be called cyclically.
//
//   Parameters:
//      timeDifference_us        - Time difference from previous function call in [microseconds].
//      ObjDict_synchronousWindowLength - _Synchronous window length_ variable from
//                                 Object dictionary (index 0x1007).
//   Return:
//      0 - No special meaning.
//      1 - New SYNC message recently received or was just transmitted.
//      2 - SYNC time was just passed out of window.
//
unsigned char CO_SYNC_process(  unsigned long timeDifference_us,
                                unsigned long ObjDict_synchronousWindowLength )
{
    UNSIGNED8 ret = 0;
    UNSIGNED32 timerNew;

    // If we are in the OPERATIONAL or PRE-OPERATIONAL state...
    if( (NMT.operatingState == CO_NMT_OPERATIONAL) || (NMT.operatingState == CO_NMT_PRE_OPERATIONAL) )
    {
        // If new SYNC message was recently received or was just transmitted, return 1.
        if( CO_SYNC.running && CO_SYNC.timer == 0 )
            ret = 1;

        // Update the sync timer, no overflow
        timerNew = CO_SYNC.timer + timeDifference_us;
        if(timerNew > CO_SYNC.timer)
            CO_SYNC.timer = timerNew;

        // Synchronous PDOs are allowed only inside time window.
        // If synchronousWindowLength is non-zero...
        if(ObjDict_synchronousWindowLength)
        {
            if(CO_SYNC.timer > ObjDict_synchronousWindowLength)
            {
                if(CO_SYNC.curentSyncTimeIsInsideWindow == 1)
                    ret = 2; // SYNC time just passed out of window.

                CO_SYNC.curentSyncTimeIsInsideWindow = 0;
            }
            else
            {
                CO_SYNC.curentSyncTimeIsInsideWindow = 1;
            }
        }
        else  // SynchronousWindowLength is zero (disabled).
        {     // PDOs can be sent at anytime.
            CO_SYNC.curentSyncTimeIsInsideWindow = 1;
        }

        // Verify timeout of SYNC
        //if(SYNC.periodTime && SYNC.timer > SYNC.periodTimeoutTime && NMT.operatingState == CO_NMT_OPERATIONAL)
            //CO_errorReport(CO->SYNC->EM, ERROR_SYNC_TIME_OUT, SYNC.timer);
            // TODO: FIX ABOVE
    }
    else
    {
        // Our device is not in the OPERATIONAL or PRE-OPERATIONAL state:
        // Reset the SYNC RUNNING flag.
        CO_SYNC.running = 0;
    }
    return ret;
}





/*
// -------------------------------------------------------------------------------------------------
//
UNSIGNED32 CO_ODF_1005( UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16  length,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData    )
{
    UNSIGNED8 configureSyncProducer = 0;
    UNSIGNED32 abortCode;
    UNSIGNED32 COB_ID;

    //memcpySwap4((UNSIGNED8*)&COB_ID, (UNSIGNED8*)dataBuff);

    if(dir == 1)
    {   //Writing Object Dictionary variable
        //only 11-bit CAN identifier is supported
        if(COB_ID & 0x20000000L)
            return 0x06090030L; //Invalid value for parameter (download only).

        //is 'generate Sync messge' bit set?
        if(COB_ID&0x40000000L)
        {
            //if bit was set before, value can not be changed
            if(SYNC.isProducer)
                return 0x08000022L;   //Data cannot be transferred or stored to the application because of the present device state.
            configureSyncProducer = 1;
        }
    }

    //abortCode = CO_ODF(&SYNC, index, subIndex, pLength, attribute, dir, dataBuff, pData);

    if(abortCode == 0 && dir == 1)
    {
        SYNC.COB_ID = COB_ID & 0x7FF;

        if(configureSyncProducer)
        {
            UNSIGNED8 len = 0;
            if(SYNC->counterOverflowValue)
            {
                len = 1;
                SYNC.counter = 0;
                SYNC.running = 0;
                SYNC.timer = 0;
            }
            //SYNC->CANtxBuff = CO_CANtxBufferInit(
            //                        SYNC->CANdevTx,         //CAN device
            //                        SYNC->CANdevTxIdx,      //index of specific buffer inside CAN module
            //                        SYNC->COB_ID,           //CAN identifier
            //                        0,                      //rtr
            //                        len,                    //number of data bytes
            //                        0   );                     //synchronous message flag bit
            SYNC.isProducer = 1;
        }
        else
        {
            SYNC.isProducer = 0;
        }
    }
    return abortCode;
}


// -------------------------------------------------------------------------------------------------
//
UNSIGNED32 CO_ODF_1006( UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16  length,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData)
{
    UNSIGNED32 abortCode;
    UNSIGNED32 period;

    //memcpySwap4((UNSIGNED8*)&period, (UNSIGNED8*)dataBuff);

    //abortCode = CO_ODF(&SYNC, index, subIndex, length, attribute, dir, dataBuff, pData);

    if(abortCode == 0 && dir == 1)
    {
        //period transition from 0 to something
        if(SYNC->periodTime == 0 && period)
            SYNC->counter = 0;

        SYNC->periodTime = period;
        SYNC->periodTimeoutTime = period / 2 * 3;
        //overflow?
        if(SYNC->periodTimeoutTime < period)
            SYNC->periodTimeoutTime = 0xFFFFFFFFL;

        SYNC->running = 0;
        SYNC->timer = 0;
    }
    return abortCode;
}


// -------------------------------------------------------------------------------------------------
//  Object 0x1019 is the Synchronous counter object (Not currently used with our device).
//  The synchronous counter defines whether a counter is mapped into the SYNC message, as well as
//  the highest value the counter can reach. 0 disables the sync counter.
// ------------------------------------------------------------------------------------------------
UNSIGNED32 CO_ODF_1019( UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16  length,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData)
{
    UNSIGNED32 abortCode;
    UNSIGNED8 len = 0;

    if(dir == 1)
    {  //Writing Object Dictionary variable
        if(SYNC.periodTime)
            return 0x08000022L; // Data cannot be transferred or stored to the application because of the present device state.
    }

    //abortCode = CO_ODF(object, index, subIndex, pLength, attribute, dir, dataBuff, pData);

    if( (abortCode == 0) && (dir == 1) )
    {
        if(SYNC.counterOverflowValue)
            len = 1;
        SYNC.counterOverflowValue = *((UNSIGNED8*)dataBuff);

        //SYNC->CANtxBuff = CO_CANtxBufferInit(
        //                      SYNC->CANdevTx,         //CAN device
        //                      SYNC->CANdevTxIdx,      //index of specific buffer inside CAN module
        //                      SYNC->COB_ID,           //CAN identifier
        //                      0,                      //rtr
        //                      len,                    //number of data bytes
        //                      0);                     //synchronous message flag bit
    }
    return abortCode;
}
*/