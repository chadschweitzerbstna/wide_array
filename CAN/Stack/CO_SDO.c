/*******************************************************************************

   File - CO_SDO.c
   CANopen SDO server object (SDO slave).

   Copyright (C) 2004-2008 Janez Paternoster

   License: GNU Lesser General Public License (LGPL).

   <http://canopennode.sourceforge.net>
*/
/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.


   Author: Janez Paternoster

*******************************************************************************/

#include "../Header/CO_driver.h"
#include "CO_SDO.h"


// State machine, values of the SDO->state variable
#define STATE_IDLE                     0x00
#define STATE_DOWNLOAD_INITIATE        0x11
#define STATE_DOWNLOAD_SEGMENTED       0x12
#define STATE_DOWNLOAD_BLOCK_INITIATE  0x14
#define STATE_DOWNLOAD_BLOCK_SUBBLOCK  0x15
#define STATE_DOWNLOAD_BLOCK_END       0x16
#define STATE_UPLOAD_INITIATE          0x21
#define STATE_UPLOAD_SEGMENTED         0x22
#define STATE_UPLOAD_BLOCK_INITIATE    0x24
#define STATE_UPLOAD_BLOCK_INITIATE_2  0x25
#define STATE_UPLOAD_BLOCK_SUBBLOCK    0x26
#define STATE_UPLOAD_BLOCK_END         0x27

// Client command specifier, see DS301
#define CCS_DOWNLOAD_INITIATE             1
#define CCS_DOWNLOAD_SEGMENT              0
#define CCS_UPLOAD_INITIATE               2
#define CCS_UPLOAD_SEGMENT                3
#define CCS_DOWNLOAD_BLOCK                6
#define CCS_UPLOAD_BLOCK                  5
#define CCS_ABORT                      0x80



/******************************************************************************/
int CO_SDO_receive(CO_CANmsg_t msg)
{
    // verify message length
    if(msg.dataLen != 8)
        return CO_ERROR_RX_MSG_LENGTH;

    // verify message overflow (previous message was not processed yet)
    if(CO_SDO.CANrxNew)
        return CO_ERROR_RX_OVERFLOW;

    //if ( ((msg.COB_ID & 0xF00) != 0x600) || (msg.COB_ID > 0x700) )
    //    return 0;

    // copy data and set 'new message' flag
    CO_SDO.CANrxData[0] = msg.data[0];
    CO_SDO.CANrxData[1] = msg.data[1];
    CO_SDO.CANrxData[2] = msg.data[2];
    CO_SDO.CANrxData[3] = msg.data[3];
    CO_SDO.CANrxData[4] = msg.data[4];
    CO_SDO.CANrxData[5] = msg.data[5];
    CO_SDO.CANrxData[6] = msg.data[6];
    CO_SDO.CANrxData[7] = msg.data[7];

    CO_SDO.CANrxNew = 1;

    return CO_ERROR_NO;
}


/******************************************************************************/
UNSIGNED32 CO_ODF(  UNSIGNED16  index,
                    UNSIGNED8   subIndex,
                    UNSIGNED16  length,
                    UNSIGNED16  attribute,
                    UNSIGNED8   dir,
                    void       *pBuffer,
                    const void *pODValue       )
{
    UNSIGNED8 *pSource;
    UNSIGNED8 *pDest;


    if( dir == 0 )
    {
        // Reading Object Dictionary variable
#ifdef BIG_ENDIAN
        if(attribute & CO_ODA_MB_VALUE)
        {
            UNSIGNED8 *dataBuffCopy = (UNSIGNED8*)dataBuff;
            UNSIGNED8 *pDataCopy = (UNSIGNED8*)pData + length;
            DISABLE_INTERRUPTS
            while(length--) *(dataBuffCopy++) = *(--pDataCopy);
            ENABLE_INTERRUPTS
        }
        else
        {
#endif
        // Reading Object Dictionary variable
        pSource = (UNSIGNED8*)pODValue;
        pDest = (UNSIGNED8*)pBuffer;

        __builtin_disable_interrupts();

        while(length--)
            *(pDest++) = *(pSource++);

        __builtin_enable_interrupts();

#ifdef BIG_ENDIAN
        }
#endif
    }
    else    // Writing Object Dictionary variable
    {
#ifdef BIG_ENDIAN
        if(attribute & CO_ODA_MB_VALUE)
        {
            UNSIGNED8 *dataBuffCopy = (UNSIGNED8*)dataBuff;
            UNSIGNED8 *pDataCopy = (UNSIGNED8*)pData + length;
            DISABLE_INTERRUPTS
            while(length--) *(--pDataCopy) = *(dataBuffCopy++);
            ENABLE_INTERRUPTS
        }
        else
        {
#endif
        // Writing Object Dictionary variable
        pSource = (UNSIGNED8*)pBuffer;
        pDest = (UNSIGNED8*)pODValue;

        __builtin_disable_interrupts();

        while(length--)
            *(pDest++) = *(pSource++);

        __builtin_enable_interrupts();
#ifdef BIG_ENDIAN
        }
#endif
    }
    return 0L;
}


/******************************************************************************
void CO_OD_configureArgumentForODF(UNSIGNED16 index, void *object)
{
    UNSIGNED16 indexOfFoundObject;

    if( CO_OD_find(index, &indexOfFoundObject) )
    {
        CO_SDO.ObjectDictionaryPointers[indexOfFoundObject] = &object;
    }
}
*/

/******************************************************************************/
void CO_SDO_init(   UNSIGNED16      ObjDictIndex_SDOServerParameter,
              const sCO_OD_object  *ObjectDictionary,
                    UNSIGNED16      ObjectDictionarySize,
                    UNSIGNED8       nodeId          )
{
    unsigned int i;

    // Configure object variables
    CO_SDO.state = 0;
    CO_SDO.ObjectDictionary = ObjectDictionary;
    CO_SDO.ObjectDictionarySize = ObjectDictionarySize;
    CO_SDO.nodeId = nodeId;

    // Configure SDO server for first argument of CO_ODF_1200
    //CO_OD_configureArgumentForODF(ObjDictIndex_SDOServerParameter, (void*)&CO_SDO.nodeId);

    // Create the array of pointers to Object Dictionary values
    //for (i=0; i<ObjectDictionarySize; i++)
    //{
    //    CO_SDO.ObjectDictionaryPointers[i] = &ObjectDictionary[i].pData;
    //}


}


/******************************************************************************/
const sCO_OD_object* CO_OD_find( UNSIGNED16     index,
                                 UNSIGNED16    *indexOfFoundObject )
{
    // Fast search in ordered Object Dictionary. If indexes are mixed, this won't work.
    // If Object Dictionary has up to 2^N entries, then N is max number of loop passes.
    UNSIGNED16 cur, min, max;
    const sCO_OD_object* object;

    min = 0;
    max = CO_SDO.ObjectDictionarySize - 1;

    while(min < max)
    {
        cur = (min + max) / 2;
        object = &CO_SDO.ObjectDictionary[cur];
        // Is object matched
        if(index == object->index)
        {
            if(indexOfFoundObject)
                *indexOfFoundObject = cur;
            return object;
        }
        if(index < object->index)
        {
            max = cur;
            if(max) max--;
        }
        else
            min = cur + 1;
    }

    if(min == max)
    {
        object = &CO_SDO.ObjectDictionary[min];
        // Is object matched
        if(index == object->index)
        {
            if(indexOfFoundObject)
                *indexOfFoundObject = min;
            return object;
        }
    }

    return 0;  // object does not exist in OD
}


/******************************************************************************/
unsigned int CO_OD_getLength(sCO_OD_object object, unsigned char subIndex)
{
    if(object.length)
    {
        // object type is var or array
        if(subIndex == 0 && object.maxSubIndex > 0)
            return 1;  //object type is array
        return object.length;
    }
    else if(object.pData == 0)
    {
        // data type is domain
        return CO_OD_MAX_OBJECT_SIZE;
    }
    else
    {
        // object type is record
        return ((CO_ODrecord_t*)(object.pData))[subIndex].length;
    }
}


/******************************************************************************/
unsigned char CO_OD_getAttribute(sCO_OD_object object, unsigned char subIndex)
{
    unsigned char attr;
   
    if(object.length)
    {
        // object type is var or array
        attr = object.attribute;
        if(subIndex == 0 && object.maxSubIndex > 0)
            return (attr&0x03) | 0x04;  // object type is array
        return attr;
    }
    else if(object.pData == 0)
    {
        // data type is domain
        attr = object.attribute;
        return attr;
    }
    else
    {
        // object type is record
        attr = ((CO_ODrecord_t*)(object.pData))[subIndex].attribute;
        return attr;
    }
}


/******************************************************************************/
const void* CO_OD_getDataPointer(sCO_OD_object object, UNSIGNED8 subIndex)
{
    unsigned char *ptr;
    //unsigned long ptrAddr;

    if(object.pData == 0)
    {
        // Data type is domain
        ptr = 0;
        return ptr;
    }
    else if(object.maxSubIndex == 0)
    {
        // Object Type is Var
        ptr = object.pData;

        // Strip off upper characters (garbage) in pointer - this is a crude work around needed
        // for the PIC18's crappy compiler.
        //ptrAddr = ptr;
        //ptr = ptrAddr & 0x000FFFFF;    // 20-bit addressing
        return ptr;
    }
    else if(object.length)
    {
        // Object Type is Array
        if(subIndex==0)
        {
            // this is the data, for the subIndex 0 in the array
            return (const void*) &object.maxSubIndex;
            return ptr;
        }
        ptr = (const void*)(((const INTEGER8*)object.pData) + ((subIndex-1) * object.length));

        // Strip off upper characters (garbage) in pointer - this is a crude work around needed
        // for the PIC18's crappy compiler.
        //ptrAddr = ptr;
        //ptr = ptrAddr & 0x000FFFFF;    // 20-bit addressing
        return ptr;
    }
    else
    {   // Object Type is Record
        ptr = ((const CO_ODrecord_t*)(object.pData))[subIndex].pData;

        // Strip off upper characters (garbage) in pointer - this is a crude work around needed
        // for the PIC18's crappy compiler.
        //ptrAddr = ptr;
        //ptr = ptrAddr & 0x000FFFFF;    // 20-bit addressing
        return ptr;
    }
}


/******************************************************************************/
void CO_SDO_abort(unsigned long code)
{
    CO_CANmsg_t msg;
    unsigned char *pDataSource = (unsigned char*)&code;

    msg.COB_ID = (0x580 + CO_SDO.nodeId);
    msg.dataLen = 8;    // Always 8 bytes for SDO transmission
    msg.data[0] = 0x80;
    msg.data[1] = CO_SDO.index & 0xFF;
    msg.data[2] = (CO_SDO.index >> 8) & 0xFF;
    msg.data[3] = CO_SDO.subIndex;
    
    // memcpySwap4
    msg.data[4] = *pDataSource;
    msg.data[5] = *(pDataSource+1);
    msg.data[6] = *(pDataSource+2);
    msg.data[7] = *(pDataSource+3);

    CO_SDO.state = 0;

    CO_CANsend(msg);
}


/******************************************************************************/
void CO_SDO_process( UNSIGNED8  NMTisPreOrOperational,
                     UNSIGNED16 timeDifference_ms,
                     UNSIGNED16 SDOtimeoutTime          )
{
    UNSIGNED32 abortCode = 0;
    UNSIGNED8 attr;
    UNSIGNED16 len, i;
    UNSIGNED32 lenRx, len32;
    CO_CANmsg_t msg;
    unsigned char *pDataDest;
    unsigned char *pDataSource;


    if(NMTisPreOrOperational)
    {
        msg.COB_ID = (0x580 + CO_SDO.nodeId);
        msg.dataLen = 8;        // Always 8 bytes for SDO transmission

        if (CO_SDO.CANrxNew /* && !CO_SDO.CANtxBuff.bufferFull*/)
        {
            // New SDO object has to be processed and SDO CAN send buffer is free
            // clear response buffer
            msg.data[0] = msg.data[1] = msg.data[2] = msg.data[3] = 0;
            msg.data[4] = msg.data[5] = msg.data[6] = msg.data[7] = 0;

            // Switch Client Command Specifier
            switch(CO_SDO.CANrxData[0] >> 5)
            {   
                case 1:   // Initiate SDO Download request
                    CO_SDO.index = (UNSIGNED16)(CO_SDO.CANrxData[2] << 8) | CO_SDO.CANrxData[1];
                    CO_SDO.subIndex = CO_SDO.CANrxData[3];
                    // find pointer to object dictionary object
                    CO_SDO.pODE = CO_OD_find(CO_SDO.index, &CO_SDO.indexOfFoundObject);
                    if(!CO_SDO.pODE)
                    {
                        CO_SDO_abort(0x06020000L); //object does not exist in OD
                        break;
                    }
                    if(CO_SDO.subIndex > CO_SDO.pODE->maxSubIndex)
                    {
                        CO_SDO_abort(0x06090011L); //Sub-index does not exist.
                        break;
                    }
                    CO_SDO.dataLength = CO_OD_getLength(*CO_SDO.pODE, CO_SDO.subIndex);
                    attr = CO_OD_getAttribute(*CO_SDO.pODE, CO_SDO.subIndex);
                    // verify length
                    if(CO_SDO.dataLength > CO_OD_MAX_OBJECT_SIZE)
                    {
                        CO_SDO_abort(0x06040047L);  // general internal incompatibility in the device
                        break;
                    }
                    if( !(attr & CO_ODA_WRITEABLE) )
                    {
                        CO_SDO_abort(0x06010002L); // attempt to write a read-only object
                        break;
                    }
                    // Default response
                    msg.data[0] = 0x60;
                    msg.data[1] = CO_SDO.CANrxData[1];
                    msg.data[2] = CO_SDO.CANrxData[2];
                    msg.data[3] = CO_SDO.CANrxData[3];
                    if(CO_SDO.CANrxData[0] & 0x02)
                    {
                        // Expedited transfer
                        if(CO_SDO.CANrxData[0] & 0x01) // is size indicated
                            len = 4 - ((CO_SDO.CANrxData[0] >> 2) & 0x03);   //size
                        else
                            len = 4;
                        // is domain data type
                        if(CO_SDO.pODE->pData == 0)
                        {
                            CO_SDO.dataLength = len;
                        }
                        // verify length
                        else if(CO_SDO.dataLength != len)
                        {
                            CO_SDO_abort(0x06070010L);  // Length of service parameter does not match
                            break;
                        }

                        // Write to memory

                        //void *object = CO_SDO.ObjectDictionaryPointers[CO_SDO.indexOfFoundObject];
                        const void *pData = CO_OD_getDataPointer(*CO_SDO.pODE, CO_SDO.subIndex);
                        
                        abortCode = CO_ODF( CO_SDO.index,           // index,
                                            CO_SDO.subIndex,        // subIndex,
                                            CO_SDO.dataLength,      // length,
                                            attr,                   // attribute,
                                            1,                      // dir = WRITE
                                           &CO_SDO.CANrxData[4],    // dataBuff,
                                            pData   );//CO_OD_getDataPointer(*CO_SDO.pODE, CO_SDO.subIndex) ); // pData

                        // Send response and finish
                        if(abortCode)
                        {
                            CO_SDO_abort(abortCode);
                            break;
                        }
                        CO_SDO.state = 0;
                        CO_CANsend(msg);  
                    }
                    else
                    {
                        // segmented transfer
                        // verify length if size is indicated
                        if(CO_SDO.CANrxData[0] & 0x01)
                        {
                            //memcpySwap4((UNSIGNED8*)&lenRx, &CO_SDO.CANrxData[4]);
                            pDataDest = &lenRx;
                            pDataSource = &CO_SDO.CANrxData[4];
                            *pDataDest = *pDataSource;
                            *(pDataDest+1) = *(pDataSource+1);
                            *(pDataDest+2) = *(pDataSource+2);
                            *(pDataDest+3) = *(pDataSource+3);
                            
                            // is domain data type
                            if(CO_SDO.pODE->pData == 0)
                            {
                                CO_SDO.dataLength = lenRx;
                            }
                            // verify length
                            else if(lenRx != CO_SDO.dataLength)
                            {
                                CO_SDO_abort(0x06070010L);  // Length of service parameter does not match
                                break;
                            }
                        }
                        CO_SDO.bufferOffset = 0;
                        CO_SDO.timeoutTimer = 0;
                        CO_SDO.state = 0x02;
                        CO_CANsend(msg);
                    }
                    break;

                case 0: // Download SDO segment
                    if( !(CO_SDO.state & 0x02) )
                    {   //download SDO segment was not initiated
                        CO_SDO_abort(0x05040001L); //command specifier not valid
                        break;
                    }
                    // verify toggle bit
                    if( (CO_SDO.CANrxData[0] & 0x10) != (CO_SDO.state & 0x10) )
                    {
                        CO_SDO_abort(0x05030000L);//toggle bit not alternated
                        break;
                    }
                    // get size
                    len = 7 - ((CO_SDO.CANrxData[0] >> 1) & 0x07);   // size
                    // verify length
                    if((CO_SDO.bufferOffset + len) > CO_SDO.dataLength || (CO_SDO.bufferOffset + len) > CO_OD_MAX_OBJECT_SIZE)
                    {
                        CO_SDO_abort(0x06070012L);  //Length of service parameter too high
                        break;
                    }
                    // copy data to buffer
                    for(i=0; i<len; i++)
                        CO_SDO.databuffer[CO_SDO.bufferOffset + i] = CO_SDO.CANrxData[i+1];
                    CO_SDO.bufferOffset += len;
                    // write response data (partial)
                    msg.data[0] = 0x20 | (CO_SDO.state & 0x10);
                    // If no more segments to be downloaded, copy data to variable
                    if(CO_SDO.CANrxData[0] & 0x01)
                    {
                        //verify length
                        if(CO_SDO.dataLength != CO_SDO.bufferOffset)
                        {
                            CO_SDO_abort(0x06070010L);  // Length of service parameter does not match
                            break;
                        }
                        attr = CO_OD_getAttribute(*CO_SDO.pODE, CO_SDO.subIndex);
                        abortCode = CO_ODF( CO_SDO.index,           // index,
                                            CO_SDO.subIndex,        // subIndex,
                                            CO_SDO.dataLength,      // length,
                                            attr,//CO_OD_getAttribute(*CO_SDO.pODE, CO_SDO.subIndex), // attribute,
                                            1,                      // dir = WRITE,
                                            CO_SDO.databuffer,      // dataBuff,
                                            CO_OD_getDataPointer(*CO_SDO.pODE, CO_SDO.subIndex) ); // pData

                        if(abortCode)
                        {
                            CO_SDO_abort(abortCode);
                            break;
                        }
                        CO_SDO.state = 0;
                    }
                    else
                    {
                        // reset timeout timer, alternate toggle bit
                        CO_SDO.timeoutTimer = 0;
                        if(CO_SDO.state&0x10)
                            CO_SDO.state &= 0xEF;
                        else
                            CO_SDO.state |= 0x10;
                    }   
                    // download segment response
                    CO_CANsend(msg);
                    break;
 
               case 2:   // Initiate SDO Upload request
                    CO_SDO.index = (UNSIGNED16)CO_SDO.CANrxData[2]<<8 | CO_SDO.CANrxData[1];
                    CO_SDO.subIndex = CO_SDO.CANrxData[3];
                    // Find pointer to object dictionary object
                    CO_SDO.pODE = CO_OD_find(CO_SDO.index, &CO_SDO.indexOfFoundObject);
                    if(!CO_SDO.pODE)
                    {
                        CO_SDO_abort(0x06020000L); // object does not exist in OD
                        break;
                    }
                    if(CO_SDO.subIndex > CO_SDO.pODE->maxSubIndex)
                    {
                        CO_SDO_abort(0x06090011L); // Sub-index does not exist.
                        break;
                    }
                    CO_SDO.dataLength = CO_OD_getLength(*CO_SDO.pODE, CO_SDO.subIndex);
                    attr = CO_OD_getAttribute(*CO_SDO.pODE, CO_SDO.subIndex);
                    // verify length
                    if(CO_SDO.dataLength > CO_OD_MAX_OBJECT_SIZE)
                    {
                        CO_SDO_abort(0x06040047L);  //general internal incompatibility in the device
                        break;
                    }
                    if( !(attr & CO_ODA_READABLE) )
                    {
                        CO_SDO_abort(0x06010001L);  //attempt to read a write-only object
                        break;
                    }

                    abortCode = CO_ODF( CO_SDO.index,           // index,
                                        CO_SDO.subIndex,        // subIndex,
                                        CO_SDO.dataLength,      // length,
                                        attr,                   // attribute,
                                        0,                      // dir = READ,
                                        CO_SDO.databuffer,      // dataBuff,
                                        CO_OD_getDataPointer(*CO_SDO.pODE, CO_SDO.subIndex) ); // pData

                    if(abortCode)
                    {
                        CO_SDO_abort(abortCode);
                        break;
                    }
                    // default response
                    msg.data[1] = CO_SDO.CANrxData[1];
                    msg.data[2] = CO_SDO.CANrxData[2];
                    msg.data[3] = CO_SDO.CANrxData[3];
                    if(CO_SDO.dataLength == 0 || CO_SDO.dataLength > CO_OD_MAX_OBJECT_SIZE)
                    {
                        CO_SDO_abort(0x06040047L);  // general internal incompatibility in the device
                        break;
                    }
                    else if(CO_SDO.dataLength <= 4)
                    {
                        // expedited transfer
                        for(i=0; i<CO_SDO.dataLength; i++)
                            msg.data[4+i] = CO_SDO.databuffer[i];
                        msg.data[0] = 0x43 | ((4 - CO_SDO.dataLength) << 2);
                        CO_SDO.state = 0;
                        CO_CANsend(msg);
                    }
                    else
                    {
                        CO_SDO.bufferOffset = 0;  //indicates pointer to next data to be send
                        CO_SDO.timeoutTimer = 0;
                        CO_SDO.state = 0x04;
                        msg.data[0] = 0x41;
                        len32 = CO_SDO.dataLength;
                        //memcpySwap4(&CO_SDO.CANtxBuff.data[4], (UNSIGNED8*)&len32);
                        pDataSource = (UNSIGNED8*)&len32;
                        msg.data[4] = *pDataSource;
                        msg.data[5] = *(pDataSource+1);
                        msg.data[6] = *(pDataSource+2);
                        msg.data[7] = *(pDataSource+3);
                        CO_CANsend(msg);
                    }
                    break;

                case 3:   // Upload SDO segment
                    if( !(CO_SDO.state & 0x04) )
                    {   // upload SDO segment was not initiated
                        CO_SDO_abort(0x05040001L);//command specifier not valid
                        break;
                    }
                    // verify toggle bit
                    if( (CO_SDO.CANrxData[0] & 0x10) != (CO_SDO.state & 0x10) )
                    {
                        CO_SDO_abort(0x05030000L); // toggle bit not alternated
                        break;
                    }
                    // calculate length to be sent
                    len = CO_SDO.dataLength - CO_SDO.bufferOffset;
                    if(len > 7)
                        len = 7;
                    // fill data bytes
                    for(i=0; i<len; i++)
                        msg.data[i+1] = CO_SDO.databuffer[CO_SDO.bufferOffset+i];
                    CO_SDO.bufferOffset += len;
                    msg.data[0] = 0x00 | (CO_SDO.state & 0x10) | ((7 - len) << 1);
                    // is end of transfer?
                    if(CO_SDO.bufferOffset == CO_SDO.dataLength)
                    {
                        msg.data[0] |= 1;
                        CO_SDO.state = 0;
                    }
                    else
                    {
                        // reset timeout timer, alternate toggle bit
                        CO_SDO.timeoutTimer = 0;
                        if(CO_SDO.state & 0x10)
                            CO_SDO.state &= 0xEF;
                        else
                            CO_SDO.state |= 0x10;
                    }
                     CO_CANsend(msg);
                    break;
                    
                case 4:   // Abort SDO transfer by client
                    CO_SDO.state = 0;
                    break;

                default:
                    CO_SDO_abort(0x05040001L); // command specifier not valid
            } // end switch
            CO_SDO.CANrxNew = 0;
        } // end process new SDO object


        CO_SDO.CANrxNew = 0;


        // verify timeout of segmented transfer

        if(CO_SDO.state)
        {
            // Segmented SDO transfer in progress
            if(CO_SDO.timeoutTimer < SDOtimeoutTime)
                CO_SDO.timeoutTimer += timeDifference_ms;
            if(CO_SDO.timeoutTimer >= SDOtimeoutTime)
            {
                CO_SDO_abort(0x05040000L); //SDO protocol timed out
            }
        }
    } //end of (pre)operational state
    else
    {
        CO_SDO.state = 0;
        CO_SDO.CANrxNew = 0;
    }
}
