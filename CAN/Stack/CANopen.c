/*******************************************************************************

   File - CANopen.c
   Main CANopen stack file. It combines Object dictionary (CO_OD) and all other
   CANopen source files. Configuration information are read from CO_OD.h file.

   Copyright (C) 2010 Janez Paternoster

   License: GNU Lesser General Public License (LGPL).

   <http://canopennode.sourceforge.net>
*/
/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.


   Author: Janez Paternoster

*******************************************************************************/


#include "../Header/CO_driver.h"
#include "../Header/CO_OD.h"
#include "CANopen.h"
#include "CO_Emergency.h"
#include "../../ctl.h"
#include "../../eeprom.h"
#include "../../CRC.h"


// *** The Object Dictionary array ***
extern const sCO_OD_object CO_OD[CO_OD_NoOfElements];
//extern unsigned char dynamicNodeID;


// -------------------------------------------------------------------------------------------------
//  Initialize and configure all of the various CANopen specifics.
//
void CO_init(void)
{
    // Initialize the object directory.  Communication parameters (baud rate, node ID, etc)
    // will be used for configuring the hardware and communication objects later on.
    //CO_init_ObjectDirectory(&CO_OD_EEPROM, sizeof(CO_OD_EEPROM));

    // Initialize the dynamic node ID (now that the Object Directory is initialized).
    unsigned char dynamicNodeID = OD_CANNodeID;

    // Initialize and configure the CAN message buffers.
    CO_CANmodule_init();

    // Initialize and configure the NMT state machine
    CO_NMT_init(dynamicNodeID, 500 /*firstHBtime*/, CAN_ID_HEARTBEAT + dynamicNodeID);
   
    // Initialize and configure the SYNC module
    CO_SYNC_init(OD_COB_ID_SYNCMessage, OD_communicationCyclePeriod, 0 /*OD_synchronousCounterOverflowValue*/ );

    // Initialize the SDO module
    CO_SDO_init(  0x1200,               //  ObjDictIndex_SDOServerParameter
                 &CO_OD[0],             // *ObjectDictionary
                  CO_OD_NoOfElements,   //  ObjectDictionarySize
                  dynamicNodeID    );

    // Initialize the LSS module
    CO_LSS_init( dynamicNodeID, OD_identity.vendorID, OD_identity.productCode,
                    OD_identity.revisionNumber, OD_identity.serialNumber, OD_lssLock);

    // Configure the transmit PDO(s)
    //for(i=0; i<CO_NO_TPDO; i++)
    //{
    /*    CO_TPDO_init( TPDO[i],
                        nodeId,
                        ((i<=4) ? (CAN_ID_TPDO0+i*0x100) : 0),
                        0,
                       &OD_TPDOCommunicationParameter[i],
                       &OD_TPDOMappingParameter[i],
                        0x1800+i,
                        0x1A00+i,
                        CO->CANmodule, CO_TXCAN_TPDO+i);
  */  //}
    // Only one TPDO at this time
    CO_TPDO_init(dynamicNodeID, (CAN_ID_TPDO0 + dynamicNodeID) /*TPDO1 default COB_ID*/, 0 /*restrictionFlags*/,
                   &OD_TPDOCommunicationParameter, &OD_TPDOMappingParameter, 0x1800, 0x1A00);

    // Only one RPDO at this time
    CO_RPDO1_init(dynamicNodeID, (CAN_ID_RPDO0 + dynamicNodeID), 0,
                   &OD_RPDOCommunicationParameter[0], &OD_RPDOMappingParameter[0], 0x1400, 0x1600);

    // Initialize the Emergency module
    CO_emergency_init( &OD_errorStatusBits[0],
                        ODL_errorStatusBits_stringLength,
                       &OD_errorRegister,
                       &OD_preDefinedErrorField[0],
                        ODL_preDefinedErrorField_arrayLength,
                        20,             // msgBufferSize in chars
                        CAN_ID_EMERGENCY + dynamicNodeID );


/*
    for(i=0; i<CO_NO_RPDO; i++)
    {
        CO_RPDO_init( CO->RPDO[i],
                        CO->EM,
                        CO->SDO,
                       &CO->NMT->operatingState,
                        nodeId,
                        ((i<=4) ? (CAN_ID_RPDO0+i*0x100) : 0),
                        0,
                       &OD_RPDOCommunicationParameter[i],
                       &OD_RPDOMappingParameter[i],
                        0x1400+i,
                        0x1600+i,
                        CO->CANmodule, CO_RXCAN_RPDO+i);
    }


    for(i=0; i<CO_NO_TPDO; i++)
    {
        CO_TPDO_init( CO->TPDO[i],
                        CO->EM,
                        CO->SDO,
                       &CO->NMT->operatingState,
                        nodeId,
                        ((i<=4) ? (CAN_ID_TPDO0+i*0x100) : 0),
                        0,
                       &OD_TPDOCommunicationParameter[i],
                       &OD_TPDOMappingParameter[i],
                        0x1800+i,
                        0x1A00+i,
                        CO->CANmodule, CO_TXCAN_TPDO+i);
    }
   */
 





}


// -------------------------------------------------------------------------------------------------
//
//
unsigned char CO_process(unsigned int timeDifference_ms)
{
    unsigned char NMTisPreOrOperational = 0;
    unsigned char reset = 0;

    if(NMT.operatingState == CO_NMT_PRE_OPERATIONAL || NMT.operatingState == CO_NMT_OPERATIONAL)
        NMTisPreOrOperational = 1;
   
    CO_SDO_process( NMTisPreOrOperational, timeDifference_ms,
                    //0xFFFF );     // 65.5 seconds
                    //0x7530 );     // 30 seconds
                    0x1388 );     //  5 seconds
                    //0x03E8 );     //  1 second

    CO_emergency_process(   NMTisPreOrOperational,
                            (timeDifference_ms * 10),
                            OD_inhibitTimeEmergency );

    reset = CO_NMT_process( timeDifference_ms,
                            OD_producerHeartbeatTime,
                            OD_NMTStartup,
                            OD_errorRegister );

    return reset;
}



// -------------------------------------------------------------------------------------------------
//
//
void CO_process_RPDO(void)
{
    unsigned char SYNCret;
    //unsigned char i;

   // First do SYNC process
   //  (determines if PDOs can be sent IF synchronousWindowLength is non-zero).
   SYNCret = CO_SYNC_process(1000L, 0);

   // If SYNCret == 2 - SYNC time just passed out of window.
   //if(SYNCret == 2)
   //{
       // Clear pending PDOs.
   //    CO_CANclearPendingSyncPDOs(CO->CANmodule);
   //}

   // Process received PDOs (copy data to OD).
   //for(i=0; i<CO_NO_RPDO; i++)
      CO_RPDO_process(&RPDO);
   
}


// -------------------------------------------------------------------------------------------------
//
//
void CO_process_TPDO(void)
{
   //unsigned char i;

   // Verify PDO Change Of State and process PDOs
/*   for(i=0; i<CO_NO_TPDO; i++)
   {
      if(!TPDO[i]->sendRequest)
      {
          // Prepare TPDO data automatically from Object Dictionary variables
          TPDO[i]->sendRequest = CO_TPDOisCOS(CO->TPDO[i]);
      }
      // Process transmitting PDO messages.
      // Called after 1ms interrupt, so time values of 10 and 1 are assumed.
      CO_TPDO_process(&CO->TPDO[i], CO->SYNC, 10, 1);
   }*/
   
    //if(!TPDO.sendRequest)
    //{
    //    // Prepare TPDO data automatically from Object Dictionary variables
    //    TPDO.sendRequest = CO_TPDOisCOS(&TPDO);
    //}

    // Process transmitting PDO messages.
    // Called after 1ms interrupt, so time values of 10 and 1 are assumed.
    CO_TPDO_process(&TPDO, 10, 1);


    // Scannerlist MUX PDO transmit.
    CO_scannerlist_process(&MUXPDO, 10, 1);

}

/*
// -------------------------------------------------------------------------------------------------
//  Initialize the non-volatile object directory values from external memory (EEPROM or FRAM).
//
void CO_init_ObjectDirectory(struct sCO_OD_EEPROM *pLocalValues, unsigned int numBytes)
{
    struct sCO_OD_EEPROM storedValues; // Buffer for data from EEPROM
    unsigned char calcCRC;
    unsigned int i;
    unsigned char *pDest, *pSource;

    // Check the CRC stored in the extermal memory.
    // If the CRC does not match the calculated CRC, then the EEPROM is blank or data is corrupt.
    // In either case, re-write the EEPROM with the defaults.
    // Otherwise, if the CRC is good, copy the stored values into RAM.

    // Get the stored values from eeprom.
    read_eeprom((unsigned char *)&storedValues, CAN_DATA_ADDR, numBytes);

    // Calulate the CRC.
    // Do not include the last byte in the calculation, which is the stored CRC value.
    calcCRC = crc8_slow((unsigned char *)&storedValues, (numBytes-1));

    // Check the CRC against the stored value
    if (calcCRC == storedValues.crc)
    {
        // Data is good  - copy saved values to RAM.
        pSource = (unsigned char *)&storedValues;
        pDest = (unsigned char *)pLocalValues;
        for (i=0; i<numBytes; i++)
            *(pDest+i) = *(pSource+i);
    }
    else
    {
        // Data is invalid - write defaults from RAM to external memory.

        // Calc new CRC
        calcCRC = crc8_slow((unsigned char *)pLocalValues, (numBytes-1));
        pLocalValues->crc = calcCRC;                    // Update the local CRC value in RAM

        // Copy the data in RAM to external memory.
        write_eeprom((unsigned char *)pLocalValues, CAN_DATA_ADDR, numBytes);
        
        // TODO: Flag an error???
    }

}
*/
/*
// -------------------------------------------------------------------------------------------------
//
//
void CO_save_ObjectDirectory(struct sCO_OD_EEPROM *pLocalValues, unsigned int numBytes)
{
    unsigned char calcCRC;

    // Calulate the new CRC.
    // Do not include the last byte in the calculation - that is the old CRC value.
    calcCRC = crc8_slow((unsigned char *)pLocalValues, (numBytes-1));

    // Update the local CRC value in RAM.
    pLocalValues->crc = calcCRC;

    // Copy the data in RAM to external memory.
    write_eeprom((unsigned char *)pLocalValues, CAN_DATA_ADDR, numBytes);
}
*/
