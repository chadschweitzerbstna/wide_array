/*******************************************************************************

   File: CANopen.h
   Main CANopen stack file. It combines Object dictionary (CO_OD) and all other
   CANopen source files. Configuration information are read from CO_OD.h file.

   Copyright (C) 2010 Janez Paternoster

   License: GNU Lesser General Public License (LGPL).

   <http://canopennode.sourceforge.net>
*/
/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.


   Author: Janez Paternoster

*******************************************************************************/

#ifndef _CANopen_H
#define _CANopen_H


#include "../Header/CO_driver.h"
#include "../Header/CO_OD.h"
#include "CO_SDO.h"
#include "CO_NMT_Heartbeat.h"
#include "CO_SYNC.h"
#include "CO_PDO.h"
#include "LSS.h"
#if CO_NO_SDO_CLIENT == 1
#include "CO_SDOmaster.h"
#endif






/* Indexes for CANopenNode message objects ************************************/
   #ifdef ODL_consumerHeartbeatTime_arrayLength
      #define CO_NO_HB_CONS   ODL_consumerHeartbeatTime_arrayLength
   #else
      #define CO_NO_HB_CONS   0
   #endif

   #define CO_RXCAN_NMT       0                                      // index for NMT message
   #define CO_RXCAN_SYNC      1                                      // index for SYNC message
   #define CO_RXCAN_RPDO     (CO_RXCAN_SYNC+CO_NO_SYNC)              // start index for RPDO messages
   #define CO_RXCAN_SDO_SRV  (CO_RXCAN_RPDO+CO_NO_RPDO)              // start index for SDO server message (request)
   #define CO_RXCAN_SDO_CLI  (CO_RXCAN_SDO_SRV+CO_NO_SDO_SERVER)     // start index for SDO client message (response)
   #define CO_RXCAN_CONS_HB  (CO_RXCAN_SDO_CLI+CO_NO_SDO_CLIENT)     // start index for Heartbeat Consumer messages
   //total number of received CAN messages
   #define CO_RXCAN_NO_MSGS (1+CO_NO_SYNC+CO_NO_RPDO+CO_NO_SDO_SERVER+CO_NO_SDO_CLIENT+CO_NO_HB_CONS)

   #define CO_TXCAN_NMT       0                                      // index for NMT master message
   #define CO_TXCAN_SYNC      CO_TXCAN_NMT+CO_NO_NMT_MASTER          // index for SYNC message
   #define CO_TXCAN_EMERG    (CO_TXCAN_SYNC+CO_NO_SYNC)              // index for Emergency message
   #define CO_TXCAN_TPDO     (CO_TXCAN_EMERG+CO_NO_EMERGENCY)        // start index for TPDO messages
   #define CO_TXCAN_SDO_SRV  (CO_TXCAN_TPDO+CO_NO_TPDO)              // start index for SDO server message (response)
   #define CO_TXCAN_SDO_CLI  (CO_TXCAN_SDO_SRV+CO_NO_SDO_SERVER)     // start index for SDO client message (request)
   #define CO_TXCAN_HB       (CO_TXCAN_SDO_CLI+CO_NO_SDO_CLIENT)     // index for Heartbeat message
   //total number of transmitted CAN messages
   #define CO_TXCAN_NO_MSGS (CO_NO_NMT_MASTER+CO_NO_SYNC+CO_NO_EMERGENCY+CO_NO_TPDO+CO_NO_SDO_SERVER+CO_NO_SDO_CLIENT+1)


/* Default CANopen COB identifiers ********************************************/
   #define CAN_ID_NMT_SERVICE  0x000
   #define CAN_ID_SYNC         0x080
   #define CAN_ID_EMERGENCY    0x080
   #define CAN_ID_TIME_STAMP   0x100
   #define CAN_ID_TPDO0        0x180
   #define CAN_ID_RPDO0        0x200
   #define CAN_ID_TPDO1        0x280
   #define CAN_ID_RPDO1        0x300
   #define CAN_ID_TPDO2        0x380
   #define CAN_ID_RPDO2        0x400
   #define CAN_ID_TPDO3        0x480
   #define CAN_ID_RPDO3        0x500
   #define CAN_ID_TSDO         0x580
   #define CAN_ID_RSDO         0x600
   #define CAN_ID_HEARTBEAT    0x700



/* Default CANopen COB identifiers 
typedef enum {
    CO_CAN_ID_NMT_SERVICE       = 0x000,
    CO_CAN_ID_SYNC              = 0x080,
    CO_CAN_ID_EMERGENCY         = 0x080,
    CO_CAN_ID_TIME_STAMP        = 0x100,
    CO_CAN_ID_TPDO0             = 0x180,
    CO_CAN_ID_RPDO0             = 0x200,
    CO_CAN_ID_TPDO1             = 0x280,
    CO_CAN_ID_RPDO1             = 0x300,
    CO_CAN_ID_TPDO2             = 0x380,
    CO_CAN_ID_RPDO2             = 0x400,
    CO_CAN_ID_TPDO3             = 0x480,
    CO_CAN_ID_RPDO3             = 0x500,
    CO_CAN_ID_TSDO              = 0x580,
    CO_CAN_ID_RSDO              = 0x600,
    CO_CAN_ID_HEARTBEAT         = 0x700
} CO_Default_CAN_ID_t;  */



/*******************************************************************************
   Object: CO_t

   CANopen stack object combines pointers to all CANopen objects.

   Variables:
      CANmodule[]    - CAN module object(s) of type <CO_CANmodule_t>.
      SDO            - SDO object of type <CO_SDO_t>.
      EM             - Emergency report object of type <CO_emergencyReport_t>.
      EMpr           - Emergency process object of type <CO_emergencyProcess_t>.
      NMT            - NMT object of type <CO_NMT_t>.
      SYNC           - SYNC object of type <CO_SYNC_t>.
      RPDO[]         - RPDO object(s) of type <CO_RPDO_t>.
      TPDO[]         - TPDO object(s) of type <CO_TPDO_t>.
      HBcons         - Heartbeat consumer object of type <CO_HBconsumer_t>.
      SDOclient      - SDO client object of type <CO_SDOclient_t>.
*******************************************************************************/
typedef struct
{
   CO_CANmodule_t            *pCANmodule;
   CO_SDO_t                  *pSDO;
 //  CO_emergencyReport_t      *EM;
 //  CO_emergencyObject_t      *EMpr;
   CO_NMT_t                  *pNMT;
   CO_SYNC_t                 *pSYNC;
   CO_RPDO_t                 *pRPDO[CO_NO_RPDO];
   CO_TPDO_t                 *pTPDO[CO_NO_TPDO];
  // CO_HBconsumer_t           *HBcons;
#if CO_NO_SDO_CLIENT == 1
   CO_SDOclient_t            *SDOclient;
#endif
} CO_t;


/*******************************************************************************
    CANopen object
*******************************************************************************/
//extern CO_t *CO;


/*******************************************************************************
   Object: NMT master

   Function CO_sendNMTcommand() is simple function, which sends CANopen message.
   This part of code is an example of custom definition of simple CANopen
   object. Follow the code in CANopen.c file. If macro CO_NO_NMT_MASTER is 1,
   function CO_sendNMTcommand can be used to send NMT master message.

   Parameters:
      CO                          - Pointer to CANopen object <CO_t>.
      command                     - NMT command.
      noodeID                     - Node ID.

   Return:
      0                           - Operation completed successfully.
      other                       - same as <CO_CANsend>.
*******************************************************************************/
#if CO_NO_NMT_MASTER == 1
   UNSIGNED8 CO_sendNMTcommand(CO_t *CO, UNSIGNED8 command, UNSIGNED8 nodeID);
#endif


/*******************************************************************************
   Function: CO_init

   Initialize CANopen stack.

   Function must be called in the communication reset section.

*******************************************************************************/
void CO_init(void);


/*******************************************************************************
   Function: CO_process

   Process CANopen objects.

   Function must be called cyclically. It processes all "asynchronous" CANopen
   objects.

   Parameters:
      CO                - Pointer to CANopen object <CO_t>.
      timeDifference_ms - Time difference from previous function call in [milliseconds].

    Return (from <CO_NMT_process>):
      0  - Normal return, no action.
      1  - Application must provide communication reset.
      2  - Application must provide complete device reset.
*******************************************************************************/
unsigned char CO_process(unsigned int timeDifference_ms);


/*******************************************************************************
   Function: CO_process_RPDO

   Process CANopen SYNC and RPDO objects.

   Function must be called cyclically from synchronous 1ms task. It processes
   SYNC and receive PDO CANopen objects.

   Parameters:
      CO                - Pointer to CANopen object <CO_t>.
*******************************************************************************/
void CO_process_RPDO(void);


// -------------------------------------------------------------------------------------------------
//   Function: CO_process_TPDO
//   Process CANopen TPDO objects.
//
void CO_process_TPDO(void);





//void CO_init_ObjectDirectory(struct sCO_OD_EEPROM *pLocalValues, unsigned int numBytes);
//void CO_save_ObjectDirectory(struct sCO_OD_EEPROM *pLocalValues, unsigned int numBytes);







#endif
