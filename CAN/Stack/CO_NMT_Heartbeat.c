/***************************************************************************************************

   File - CO_NMT_Heartbeat.c

***************************************************************************************************/


#include "../Header/CO_driver.h"
#include "CO_Emergency.h"
#include "CO_NMT_Heartbeat.h"




// -------------------------------------------------------------------------------------------------
//
//
void CO_NMT_receive(CO_CANmsg_t msg)
{
    UNSIGNED8 command, nodeId;

   // Verify message length
   if(msg.dataLen != 2)
       return ;//CO_ERROR_RX_MSG_LENGTH;

   nodeId = msg.data[1];
   if(nodeId == NMT.nodeId || nodeId == 0)  // Command is for our node, or for all nodes.
   {
      command = msg.data[0];

      switch(command)
      {
         case CO_NMT_ENTER_OPERATIONAL:
             //if( !(EMpr.errorRegister) )
                NMT.operatingState = CO_NMT_OPERATIONAL;
             break;
         case CO_NMT_ENTER_STOPPED:
             NMT.operatingState = CO_NMT_STOPPED;
             break;
         case CO_NMT_ENTER_PRE_OPERATIONAL:
             NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
             break;
         case CO_NMT_RESET_NODE:
             NMT.resetCommand = 2;
             break;
         case CO_NMT_RESET_COMMUNICATION:
             NMT.resetCommand = 1;
             break;
         default: 
             CO_errorReport(&EM, ERROR_NMT_WRONG_COMMAND, command);
             break;
      }
   }
}


// -------------------------------------------------------------------------------------------------
//
//
void CO_NMT_init (unsigned char nodeId, unsigned int firstHBtime, unsigned int HBproducerID)
{
    // Configure NMT object variables
    NMT.operatingState  = CO_NMT_INITIALIZING;  // NMT state
    NMT.nodeId          = nodeId;               // NMT node ID
    NMT.firstHBTime     = firstHBtime;
    NMT.resetCommand    = 0;                    // Initialize to 0.  A non-zero command will trigger a reset.
    NMT.HBproducerTimer = 0;
    NMT.HBproducerID = HBproducerID;
}


// -------------------------------------------------------------------------------------------------
//
//
unsigned char CO_NMT_process(   unsigned int    timeDifference_ms,
                                unsigned int    HBtime,
                                unsigned long   NMTstartup,
                                unsigned char   errReg          )
{
    CO_CANmsg_t msg;
    unsigned char CANpassive;

    // Update the HBproducer timer if the elapsed time is less then the HBtime.
    if(NMT.HBproducerTimer < HBtime)
        NMT.HBproducerTimer += timeDifference_ms;

    // If Heartbeat time is non-zero (disabled) and the elapsed time is greater then the HB time,
    //  send HB message.  Or if NMT state == INITIALIZING, send heart beat (NMT status) message.
    if( (HBtime && (NMT.HBproducerTimer >= HBtime)) || (NMT.operatingState == CO_NMT_INITIALIZING) )
    {
        // Clear timer.
        NMT.HBproducerTimer = 0;

        // Send heart beat (NMT status message / operating state)
        msg.COB_ID = NMT.HBproducerID;      // COB_ID = 0x700 + nodeID
        msg.dataLen = 1;                    // Length
        msg.data[0] = NMT.operatingState;   // status byte
        CO_CANsend(msg);

        if(NMT.operatingState == CO_NMT_INITIALIZING)
        {
            // If first HB time is less then normal HB time, update the HB producer timer appropriately.
            if(HBtime > NMT.firstHBTime)
                NMT.HBproducerTimer = HBtime - NMT.firstHBTime;

            // Switch to operational state automatically if so desired
            //  (based on setting 1F80 in Objecty Dictionary).
            if( (NMTstartup & 0x04) == 0 )
                NMT.operatingState = CO_NMT_OPERATIONAL;
            else
                NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
        }
    }

    // CAN passive flag
    CANpassive = 0;
    if( CO_isError(EMpr.reportBuffer, ERROR_CAN_TX_BUS_PASSIVE) || CO_isError(EMpr.reportBuffer, ERROR_CAN_RX_BUS_PASSIVE) )
    {
        CANpassive = 1;
    }

/*
    // CANopen green RUN LED (DR 303-3)
    switch(NMT->operatingState)
    {
        case CO_NMT_STOPPED:
            NMT->LEDgreenRun = NMT->LEDsingleFlash;
            break;
        case CO_NMT_PRE_OPERATIONAL:
            NMT->LEDgreenRun = NMT->LEDblinking;
            break;
        case CO_NMT_OPERATIONAL:
            NMT->LEDgreenRun = 1;
            break;
    }
*/
/*
    // CANopen red ERROR LED (DR 303-3)
    if( CO_isError(NMT->EMpr->reportBuffer, ERROR_CAN_TX_BUS_OFF) )
        NMT->LEDredError = 1;
    else if(CO_isError(NMT->EMpr->reportBuffer, ERROR_SYNC_TIME_OUT))
        NMT->LEDredError = NMT->LEDtripleFlash;
    else if( CO_isError(NMT->EMpr->reportBuffer, ERROR_HEARTBEAT_CONSUMER)
              || CO_isError(NMT->EMpr->reportBuffer, ERROR_HEARTBEAT_CONSUMER_REMOTE_RESET) )
        NMT->LEDredError = NMT->LEDdoubleFlash;
    else if( CANpassive || CO_isError(NMT->EMpr->reportBuffer, ERROR_CAN_BUS_WARNING) )
        NMT->LEDredError = NMT->LEDsingleFlash;
    else if(errReg)
        NMT->LEDredError = (NMT->LEDblinking>=0)?-1:1;
    else
        NMT->LEDredError = -1;
*/

    // In case of error enter pre-operational state
    if (NMT.operatingState == CO_NMT_OPERATIONAL)
    {
        if(CANpassive)
            errReg |= 0x10;

        if(errReg)
        {
            // Communication error
            if(errReg & 0x10)
            {
                NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
                /*NMT.operatingState = CO_NMT_STOPPED;
                if ( CO_isError(ERROR_CAN_TX_BUS_OFF) || CO_isError(ERROR_HEARTBEAT_CONSUMER)
                            || CO_isError(ERROR_HEARTBEAT_CONSUMER_REMOTE_RESET) )
                {
                    NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
                    NMT.operatingState = CO_NMT_STOPPED;
                }*/
            }

            // Generic error
            if(errReg & 0x01)
            {
                NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
                //NMT.operatingState = CO_NMT_STOPPED;

            }

            // Device profile error
            if(errReg & 0x20)
            {
                NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
                //NMT.operatingState = CO_NMT_STOPPED;
            }

            // Manufacturer specific error
            if(errReg & 0x80)
            {
                NMT.operatingState = CO_NMT_PRE_OPERATIONAL;
                //NMT.operatingState = CO_NMT_STOPPED;
            }

            // If operational state is lost, send HB (status) immediately.
            if (NMT.operatingState != CO_NMT_OPERATIONAL)
                NMT.HBproducerTimer = HBtime;
        }
    }

    // Return the reset command.  If non-zero, communication reset (1) or device reset (2) should occur.
    return NMT.resetCommand;
}
