/*******************************************************************************

   File: CO_SYNC.h
   CANopen SYNC object.

*******************************************************************************/

#ifndef _CO_SYNC_H
#define _CO_SYNC_H



#include "../Header/CO_driver.h"


/*******************************************************************************
   Topic: SYNC message

   Description and contents of SYNC message.

   For CAN identifier see <Default COB identifiers>.

   SYNC message is used for synchronization of the nodes on network. There is
   one SYNC producer and zero or more SYNC consumers.

   Contents of SYNC message:
      By default SYNC message has no data. If _Synchronous counter overflow value_
      from Object dictionary (index 0x1019) is different than 0, SYNC message has
      one data byte: counter incremented by 1 with every SYNC transmission.
*******************************************************************************/


// -------------------------------------------------------------------------------------------------
//   Object: CO_SYNC_t
//
//   Object controls SYNC producer and consumer.
//
//   Variables:
//      isProducer           - True, if device is SYNC producer. Calculated from _COB ID SYNC Message_
//                             variable from Object dictionary (index 0x1005).
//      COB_ID               - COB_ID of SYNC message. Calculated from _COB ID SYNC Message_
//                             variable from Object dictionary (index 0x1005).
//      periodTime           - Sync period time in [microseconds]. Calculated from _Communication cycle period_
//                             variable from Object dictionary (index 0x1006).
//      periodTimeoutTime    - Sync period timeout time in [microseconds]
//                             (periodTimeoutTime = periodTime * 1,5).
//      counterOverflowValue - Value from _Synchronous counter overflow value_
//                             variable from Object dictionary (index 0x1019).
//
//      curentSyncTimeIsInsideWindow - True, if curent time is inside synchronous window.
//                             In this case synchronous PDO may be sent.
//
//      running              - True, after first SYNC was received or transmitted.
//      timer                - Timer for the SYNC message in [microseconds]. Set to
//                             zero after received or transmitted SYNC message.
//      counter              - Counter of the SYNC message if counterOverflowValue
//                             is different than zero.
//
typedef struct {
    unsigned char   isProducer;
    unsigned int    COB_ID;
    unsigned long   periodTime;
    unsigned long   periodTimeoutTime;
    //unsigned char   counterOverflowValue;
    unsigned char   curentSyncTimeIsInsideWindow;
    unsigned long   running;
    unsigned long   timer;
    //unsigned char   counter;
} CO_SYNC_t;


// -------------------------------------------------------------------------------------------------
// *** Global SYNC structure ***
CO_SYNC_t CO_SYNC;



// -------------------------------------------------------------------------------------------------
//   Function: CO_SYNC_receive
//
//   Read received message from CAN module.
//
int CO_SYNC_receive(CO_CANmsg_t msg);



// -------------------------------------------------------------------------------------------------
//   Function: CO_SYNC_init
//
//   Initialize SYNC object.
//
//   Parameters:
//      ObjDict_COB_ID_SYNCMessage - _COB ID SYNC Message_ variable from
//                                   Object dictionary (index 0x1005).
//      ObjDict_communicationCyclePeriod - _Communication cycle period_ variable from
//                                   Object dictionary (index 0x1006).
//      ObjDict_synchronousCounterOverflowValue - _Synchronous counter overflow value_
//                                   variable from Object dictionary (index 0x1019).
//
int CO_SYNC_init(  unsigned long ObjDict_COB_ID_SYNCMessage,
                            unsigned long ObjDict_communicationCyclePeriod,
                            unsigned char ObjDict_synchronousCounterOverflowValue   );


// -------------------------------------------------------------------------------------------------
//   Function: CO_SYNC_process
//
//   Process SYNC communication.
//
//   Parameters:
//      timeDifference_us        - Time difference from previous function call in [microseconds].
//      ObjDict_synchronousWindowLength - _Synchronous window length_ variable from
//                                 Object dictionary (index 0x1007).
//   Return:
//      0 - No special meaning.
//      1 - New SYNC message recently received or was just transmitted.
//      2 - SYNC time was just passed out of window.
//
unsigned char CO_SYNC_process(  unsigned long timeDifference_us,
                            unsigned long ObjDict_synchronousWindowLength  );





/*******************************************************************************
   Function: CO_ODF_1005

   Function for accessing _COB ID SYNC Message_ (index 0x1005) from SDO server.

   For more information see topic <SDO server access function> in CO_SDO.h file.
*******************************************************************************/
/*UNSIGNED32 CO_ODF_1005( void       *object,
                        UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16 *pLength,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData);
*/

/*******************************************************************************
   Function: CO_ODF_1006

   Function for accessing _Communication cycle period_ (index 0x1006) from SDO server.

   For more information see topic <SDO server access function> in CO_SDO.h file.
*******************************************************************************/
/*UNSIGNED32 CO_ODF_1006( void       *object,
                        UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16 *pLength,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData);
*/

/*******************************************************************************
   Function: CO_ODF_1019

   Function for accessing _Synchronous counter overflow value_ (index 0x1019) from SDO server.

   For more information see topic <SDO server access function> in CO_SDO.h file.
*******************************************************************************/
/*UNSIGNED32 CO_ODF_1019( void       *object,
                        UNSIGNED16  index,
                        UNSIGNED8   subIndex,
                        UNSIGNED16 *pLength,
                        UNSIGNED16  attribute,
                        UNSIGNED8   dir,
                        void       *dataBuff,
                        const void *pData);
*/


#endif
