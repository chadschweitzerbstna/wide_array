/* 
 * File:   LSS.h
 * Author: cls
 *
 * Created on December 5, 2014, 9:40 AM
 */

#ifndef LSS_H
#define	LSS_H




#define LSS_SLAVE_TX_COB_ID                 0x7E4
#define LSS_SLAVE_RX_COB_ID                 0x7E5
#define LSS_MASTER_TX_COB_ID                0x7E5
#define LSS_MASTER_RX_COB_ID                0x7E4

#define LSS_SWITCH_TO_WAITING_STATE 		0
#define LSS_STATE_WATING 			0
#define LSS_SWITCH_TO_CONFIGURATION_STATE 	1
#define LSS_STATE_CONFIGURATION 		1
		
#define LSS_SWITCH_GLOBAL			0x04
#define LSS_SWITCH_SELECTIVE_VENDOR_ID		0x40
#define LSS_SWITCH_SELECTIVE_PRODUCT_CODE	0x41
#define LSS_SWITCH_SELECTIVE_REVISION_NUMBER	0x42
#define LSS_SWITCH_SELECTIVE_SERIAL_NUMBER	0x43
#define LSS_SWITCH_SELECTIVE_SLAVE_REPLY	0x44

#define LSS_CONFIGURE_NODE_ID			0x11
#define LSS_CONFIGURE_BIT_TIMING		0x13
#define LSS_STORE_CONFIGURATION			0x17
#define LSS_INQUIRE_VENDOR_ID 			0x5a
#define LSS_INQUIRE_PRODUCT_CODE		0x5b
#define LSS_INQUIRE_REVISION_NUMBER		0x5c
#define LSS_INQUIRE_SERIAL_NUMBER		0x5d
#define LSS_INQUIRE_NODE_ID			0x5e

#define LSS_ERROR_STORE_CONFIGUARTION_NOT_SUPPORTED 1


// LSS object data structure
typedef struct {
    unsigned long   vendor_id_default;
    unsigned long   product_code_default;
    unsigned long   revision_number_default;
    unsigned long   serial_number_default;

    unsigned char   DynamicNode;
    unsigned char   NodeChangedDelayTimer;

    unsigned long   SwitchSelectiveVendorId;
    unsigned long   SwitchSelectiveProductCode;
    unsigned long   SwitchSelectiveRevisionNumber;
    unsigned long   SwitchSelectiveSerialNumber;

    unsigned char   state;
    unsigned char   lock;
} CO_LSS_t;


// Global LSS object
CO_LSS_t CO_LSS;


// Function prototypes
void CO_LSS_receive(CO_CANmsg_t LSSmsg);
void CO_LSS_init( unsigned char node_id,
                  unsigned long vendor_id,
                  unsigned long product_code,
                  unsigned long revision_number,
                  unsigned long serial_number,
                  unsigned char lss_lock    );




#endif	/* LSS_H */

