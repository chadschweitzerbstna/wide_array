/***************************************************************************************************

   File: CO_NMT_Heartbeat.h

 **************************************************************************************************/

#ifndef _CO_NMT_HEARTBEAT_H
#define _CO_NMT_HEARTBEAT_H

#include "../Header/CO_driver.h"


// -------------------------------------------------------------------------------------------------
//   Constants: NMT internal state
//      CO_NMT_INITIALIZING           - (0) - Device is initializing.
//      CO_NMT_PRE_OPERATIONAL        - (127 = 0x7F) - Device is in pre-operational state.
//      CO_NMT_OPERATIONAL            - (5) - Device is in operational state.
//      CO_NMT_STOPPED                - (4) - Device is stopped.
//
//   Constants: NMT command
//      CO_NMT_ENTER_OPERATIONAL      - (1) - Start device.
//      CO_NMT_ENTER_STOPPED          - (2) - Stop device.
//      CO_NMT_ENTER_PRE_OPERATIONAL  - (128 = 0x80) - Put device into pre-operational.
//      CO_NMT_RESET_NODE             - (129 = 0x81) - Reset device.
//      CO_NMT_RESET_COMMUNICATION    - (130 = 0x82) - Reset CANopen communication on device.
//
#define  CO_NMT_INITIALIZING           0
#define  CO_NMT_PRE_OPERATIONAL        127
#define  CO_NMT_OPERATIONAL            5
#define  CO_NMT_STOPPED                4

#define  CO_NMT_ENTER_OPERATIONAL      1
#define  CO_NMT_ENTER_STOPPED          2
#define  CO_NMT_ENTER_PRE_OPERATIONAL  128
#define  CO_NMT_RESET_NODE             129
#define  CO_NMT_RESET_COMMUNICATION    130



// -------------------------------------------------------------------------------------------------
//
//  NMT structure:
//
//      resetCommand      - If different than zero, device will reset.
//      nodeId            - CANopen Node ID of this device.
//      HBproducerTimer   - Internal timer for HB producer.
//     firstHBTime       - See parameters in <CO_NMT_init>.
//
typedef struct {
   unsigned char    operatingState;
   unsigned char    resetCommand;
   unsigned char    nodeId;
   unsigned int     HBproducerTimer;
   unsigned int     HBproducerID;
   unsigned int     firstHBTime;
} CO_NMT_t;



// -------------------------------------------------------------------------------------------------
// *** Global NMT structure ***
CO_NMT_t NMT;



// -------------------------------------------------------------------------------------------------
//  Function: CO_NMT_receive.
//  Read received NMT message from CAN module.
//
void CO_NMT_receive(CO_CANmsg_t msg);



// -------------------------------------------------------------------------------------------------
//   Function: CO_NMT_init
//
//   Initialize NMT and Heartbeat producer.
//
//      nodeId      - CANopen Node ID of this device.
//      firstHBtime - Time between bootup and first heartbeat message in milliseconds.
//                    If firstHBTime is greater than _Producer Heartbeat time_
//                    (object dictionary, index 0x1017), latter is used instead.
//      HB_Tx_CobId - CAN identifier for HB message.
//
void CO_NMT_init (unsigned char nodeId, unsigned int firstHBtime, unsigned int HBproducerID);



// -------------------------------------------------------------------------------------------------
//   Function: CO_NMT_process
//
//   Process received NMT and produce Heartbeat messages.
//
//   Parameters:
//    timeDifference_ms - Time difference from previous function call in [milliseconds].
//    HBtime            - _Producer Heartbeat time_ (object dictionary, index 0x1017).
//    NMTstartup        - _NMT startup behavior_ (object dictionary, index 0x1F80).
//
unsigned char CO_NMT_process(   unsigned int    timeDifference_ms,
                                unsigned int    HBtime,
                                unsigned long   NMTstartup,
                                unsigned char   errReg          );



#endif
