/*******************************************************************************

   File - CO_PDO.c
   CANopen Process Data Object.


*******************************************************************************/


#include "../Header/CO_driver.h"
#include "CO_SDO.h"
#include "CO_NMT_Heartbeat.h"
#include "CO_SYNC.h"
#include "CO_PDO.h"
#include "CANopen.h"




// -------------------------------------------------------------------------------------------------
//
void CO_MUX_PDO_receive(CO_CANmsg_t msg, unsigned int nodeId)
{
    unsigned int destNodeId;
    unsigned int index;
    unsigned char subIndex;
    unsigned long value;

    if( NMT.operatingState == CO_NMT_OPERATIONAL )
    {
        // Process the MUX PDO based on the object
        destNodeId = msg.data[0];
        index = msg.data[2] << 8;
        index = index + msg.data[1];
        subIndex = msg.data[3];

        if (destNodeId == (nodeId + 0x80))
        {
            switch(index)
            {
                case 0x3110:
                    value = msg.data[4];
                    OD_materialSetupControlWord = (unsigned char)value;
                    break;

                case 0x5A01:
                    value = msg.data[4];
                    OD_lssLock = (unsigned char)value;
                    break;

                default:
                    break;
            }
        }
    }
}



// -------------------------------------------------------------------------------------------------
//
unsigned int CO_PDO_receive(CO_RPDO_t *pRPDO, CO_CANmsg_t msg)
{
    if( pRPDO->valid && (NMT.operatingState == CO_NMT_OPERATIONAL) )
    {
        // verify message length
        if( pRPDO->dataLength && (msg.dataLen < pRPDO->dataLength) )
            return CO_ERROR_RX_PDO_LENGTH;

        // verify message overflow (previous message was not processed yet)
        if(pRPDO->CANrxNew)
            return CO_ERROR_RX_PDO_OWERFLOW;

        // copy data and set 'new message' flag
        pRPDO->CANrxData[0] = msg.data[0];
        pRPDO->CANrxData[1] = msg.data[1];
        pRPDO->CANrxData[2] = msg.data[2];
        pRPDO->CANrxData[3] = msg.data[3];
        pRPDO->CANrxData[4] = msg.data[4];
        pRPDO->CANrxData[5] = msg.data[5];
        pRPDO->CANrxData[6] = msg.data[6];
        pRPDO->CANrxData[7] = msg.data[7];

        pRPDO->CANrxNew = 1;

        // verify message length
        if( pRPDO->dataLength && (msg.dataLen > pRPDO->dataLength) )
            return CO_ERROR_RX_PDO_LENGTH;
    }
    else
        pRPDO->CANrxNew = 0;

    return CO_ERROR_NO;
}



// -------------------------------------------------------------------------------------------------
//
void CO_RPDOconfigCom(CO_RPDO_t* RPDO, UNSIGNED32 COB_IDUsedByRPDO)
{
    UNSIGNED16 ID;
    enum CO_ReturnError r;

    ID = (UNSIGNED16)COB_IDUsedByRPDO;

    // is RPDO used?
    if( (COB_IDUsedByRPDO & 0xBFFFF800L) == 0 && RPDO->dataLength && ID )
    {
        // is used default COB-ID?
        if(ID == RPDO->defaultCOB_ID)
            ID += RPDO->nodeId;
        RPDO->valid = 1;
    }
    else
    {
        ID = 0;
        RPDO->valid = 0;
        RPDO->CANrxNew = 0;
    }

}


// -------------------------------------------------------------------------------------------------
//
void CO_TPDOconfigCom(CO_TPDO_t* pTPDO, unsigned long COB_IDUsedByTPDO, unsigned char syncFlag)
{
    unsigned int ID;

    ID = (unsigned int)COB_IDUsedByTPDO;

    // is TPDO used?
    if((COB_IDUsedByTPDO & 0xBFFFF800L) == 0 && pTPDO->dataLength && ID)
    {
        //is used default COB-ID?
        if(ID == pTPDO->defaultCOB_ID)
            ID += pTPDO->nodeId;
        pTPDO->valid = 1;
    }
    else
    {
        ID = 0;
        pTPDO->valid = 0;
    }

    // TODO: Configure TPDO TX buffer...
    

}


/******************************************************************************/
UNSIGNED32 CO_PDOfindMap(  UNSIGNED32     map,
                           UNSIGNED8      R_T,
                           UNSIGNED8    **ppData,
                           UNSIGNED8     *pLength,
                           UNSIGNED8     *pSendIfCOSFlags,
                           UNSIGNED8     *pIsMultibyteVar   )
{
    UNSIGNED16 index;
    UNSIGNED8 subIndex;
    UNSIGNED8 dataLen;
    UNSIGNED8 objectLen;
    const sCO_OD_object* pODE;
    UNSIGNED8 attr;
    static UNSIGNED32 dummyTX = 0;
    static UNSIGNED32 dummyRX;
    UNSIGNED8 dummySize = 4;


    index = (UNSIGNED16)(map >> 16);    // OD Index
    subIndex = (UNSIGNED8)(map >> 8);   // OD subindex
    dataLen = (UNSIGNED8) map;          // OD data length in bits

    // Data length must be a multiple of 8 bits (must be byte aligned).
    if(dataLen & 0x07)
        return 0x06040041L;         // Object cannot be mapped to the PDO.

    dataLen = dataLen >> 3;         // Data length, now in bytes.
    *pLength += dataLen;

    // Total PDO length can not be more than 8 bytes
    if(*pLength > 8)
        return 0x06040042L;         // The number and length of the objects to be mapped would exceed PDO length.

    // Is there a reference to dummy entries
    if((index <= 7) && (subIndex == 0))
    {
        if(index < 2)
            dummySize = 0;
        else if( (index==2) || (index==5) )
            dummySize = 1;
        else if( (index==3) || (index==6) )
            dummySize = 2;

        // Is size of variable big enough for map
        if(dummySize < dataLen)
            return 0x06040041L;   // Object cannot be mapped to the PDO.

        // Data and ODE pointer
        if(R_T == 0)
            *ppData = (UNSIGNED8*) &dummyRX;
        else
            *ppData = (UNSIGNED8*) &dummyTX;

        return 0;
    }

    // Find object in Object Dictionary
    pODE = CO_OD_find(index, 0);

    // Does object exist in OD?
    if((!pODE) || (subIndex > pODE->maxSubIndex))
        return 0x06020000L;     // Object does not exist in the object dictionary.

    attr = CO_OD_getAttribute(*pODE, subIndex);
    // Is object Mappable for RPDO?
    if( (R_T == 0) && !(attr&CO_ODA_RPDO_MAPABLE && attr&CO_ODA_WRITEABLE && attr&CO_ODA_MEM_RAM) )
        return 0x06040041L;     // Object cannot be mapped to the PDO.
    // Is object Mappable for TPDO?
    if( (R_T != 0) && !((attr&CO_ODA_TPDO_MAPABLE) && (attr&CO_ODA_READABLE)) )
        return 0x06040041L;     // Object cannot be mapped to the PDO.

    // is size of variable big enough for map
    objectLen = CO_OD_getLength(*pODE, subIndex);
    if(objectLen < dataLen)
        return 0x06040041L;   // Object cannot be mapped to the PDO.

    // mark multibyte variable
    *pIsMultibyteVar = (attr&CO_ODA_MB_VALUE) ? 1 : 0;

    // pointer to data
    *ppData = (UNSIGNED8*) CO_OD_getDataPointer(*pODE, subIndex);


#ifdef BIG_ENDIAN
    // skip unused MSB bytes
    if(*pIsMultibyteVar)
    {
      *ppData += objectLen - dataLen;
    }
#endif

    // setup change of state flags
    if(attr & CO_ODA_TPDO_DETECT_COS)
    {
        UNSIGNED8 i;
        for(i=*pLength-dataLen; i<*pLength; i++)
            *pSendIfCOSFlags |= 1 << i;
    }

    return 0;
}


/******************************************************************************/
UNSIGNED8 CO_RPDOconfigMap(   CO_RPDO_t* RPDO,
                        const OD_RPDOMappingParameter_t *ObjDict_RPDOMappingParameter )
{
    UNSIGNED8 i;
    UNSIGNED8 length = 0;
    UNSIGNED32 ret = 0;
    const UNSIGNED32* pMap = &ObjDict_RPDOMappingParameter->mappedObject1;

    for(i=ObjDict_RPDOMappingParameter->numberOfMappedObjects; i>0; i--)
    {
        UNSIGNED8 j;
        UNSIGNED8* pData;
        UNSIGNED8 dummy = 0;
        UNSIGNED8 prevLength = length;
        UNSIGNED8 MBvar;

        // function do much checking of errors in map
        ret = CO_PDOfindMap(*(pMap++),
                             0,
                            &pData,
                            &length,
                            &dummy,
                            &MBvar);
        if(ret)
        {
            length = 0;
            //CO_errorReport(RPDO->EM, ERROR_PDO_WRONG_MAPPING, ret);
            break;
        }

        // write PDO data pointers
#ifdef BIG_ENDIAN
        if(MBvar)
        {
            for(j=length-1; j>=prevLength; j--)
                RPDO->mapPointer[j] = pData++;
        }
        else
        {
            for(j=prevLength; j<length; j++)
                RPDO->mapPointer[j] = pData++;
        }
#else
        for(j=prevLength; j<length; j++)
            RPDO->mapPointer[j] = pData++;
#endif
    }

    RPDO->dataLength = length;

    return ret;
}


/******************************************************************************/
UNSIGNED8 CO_TPDOconfigMap( CO_TPDO_t* pTPDO,
                          const OD_TPDOMappingParameter_t *ObjDict_TPDOMappingParameter )
{
    UNSIGNED8 i;
    UNSIGNED8 length = 0;
    UNSIGNED32 ret = 0;
    const UNSIGNED32* pMap = &ObjDict_TPDOMappingParameter->mappedObject1;
    UNSIGNED8 j;
    UNSIGNED8 *pData;
    //unsigned char *pTest;
    UNSIGNED8 prevLength;
    UNSIGNED8 MBvar;
    //const sCO_OD_object* pODE;
    //UNSIGNED16 index;
    //UNSIGNED8 subIndex;
    //UNSIGNED8 dataLen;

/*
    unsigned char * tmpPointer;

    tmpPointer = (unsigned char *)&OD_statusWord;
    pTPDO->mapPointer[0] = tmpPointer;
    pTPDO->mapPointer[1] = tmpPointer+1;

    tmpPointer = (unsigned char *)&OD_edgeValue.edgeValue1;
    pTPDO->mapPointer[2] = tmpPointer;
    pTPDO->mapPointer[3] = tmpPointer+1;
    pTPDO->mapPointer[4] = tmpPointer+2;

    tmpPointer = (unsigned char *)&OD_edgeValue.edgeValue2;
    pTPDO->mapPointer[5] = tmpPointer;
    pTPDO->mapPointer[6] = tmpPointer+1;
    pTPDO->mapPointer[7] = tmpPointer+2;

    pTPDO->dataLength = 8;
*/

    pTPDO->sendIfCOSFlags = 0;

    for(i=ObjDict_TPDOMappingParameter->numberOfMappedObjects; i>0; i--)
    {
        prevLength = length;

        // function do much checking of errors in map
        ret = CO_PDOfindMap( *(pMap++),
                           1,
                           &pData,
                           &length,
                           &pTPDO->sendIfCOSFlags,
                           &MBvar   );
        if(ret)
        {
            length = 0;
            // TODO: Fix error report
            //CO_errorReport(TPDO->EM, ERROR_PDO_WRONG_MAPPING, ret);
            break;
        }

 /*       index = (UNSIGNED16)(*pMap >> 16);    // OD Index
        subIndex = (UNSIGNED8)(*pMap >> 8);   // OD subindex
        dataLen = (UNSIGNED8) *pMap & 0xFF;   // OD data length in bits
        dataLen = dataLen >> 3;             // Data length, now in bytes.
        length += dataLen;

        // Find object in Object Dictionary
        pODE = CO_OD_find(index, 0);
        ppData = 0;
        if(pODE->maxSubIndex == 0)
        {   // Object Type is Var
            pData = (const void*) pODE->pData;
            unsigned int tmpInt = pData;
            pData = tmpInt;

            pData = 0x07F7;

            pTest = (const unsigned char *)&CO_OD_RAM.statusWord;
            pTest = (const unsigned char *)&CO_OD_RAM.edgeValue.edgeValue1;
        }
        else if(pODE->length)
        {   // Object Type is Array
            if(subIndex == 0)
            {   // this is the data, for the subIndex 0 in the array
                pData = (const void*) &pODE->maxSubIndex;
            }
            else pData = (const void*)(((const INTEGER8*)pODE->pData) + ((subIndex-1) * pODE->length));
        }
        else
        {   // Object Type is Record
            pData = ((const CO_ODrecord_t*)(pODE->pData))[subIndex].pData;
        }


        pMap++;
*/

        // Write PDO data pointers
        for(j=prevLength; j<length; j++)
            pTPDO->mapPointer[j] = pData++;
    }

    pTPDO->dataLength = length;

    return ret;
}


// ------------------------------------------------------------------------------------------------
//
void CO_MUXPDOconfigMap(void)
{
    UNSIGNED8 i;
    UNSIGNED32 *pMap;   // Pointer to map - all maps are 32 bits
    UNSIGNED16 index;
    UNSIGNED8 subIndex;
    UNSIGNED8 dataLen;
    const sCO_OD_object *pODE;
    UNSIGNED8 *pData;


    // Set the base address for the scanner mapping entries.
    pMap = &OD_scannerlistMapping.mappedObject1;   // Pointer to first scannerlist map.

    // Zero the number of scanner objects.
    MUXPDO.numberOfScannerObjects = 0;

    // Map all of the scannerlist data pointers into the MUXPDO object.
    for(i=0; i<OD_scannerlistMapping.numberOfMappedObjects; i++)
    {
        // Get the index, subindex, and length of this mapped object.
        index = (UNSIGNED16)(*(pMap+i) >> 16);   // OD Index
        subIndex = (UNSIGNED8)(*(pMap+i) >> 8);  // OD subindex
        dataLen = (UNSIGNED8)*(pMap+i);          // OD data length in bits

        // Convert data length from number of bits to number of bytes.
        dataLen = dataLen >> 3; // NOTE: the data cannot be more then 4 bytes (32 bits).

        // Find the object in the Object Dictionary.
        pODE = CO_OD_find(index, 0);
        
        // Does object exist in the Object Dictionary?
        if((pODE) && (subIndex <= pODE->maxSubIndex))
        {
            // The object exists.  Get a pointer to the object data
            pData = (UNSIGNED8*)CO_OD_getDataPointer(*pODE, subIndex);

            // Copy the pointer to the MUXPDO mapped object list.
            MUXPDO.mapPointer[MUXPDO.numberOfScannerObjects] = pData;

            // Store the object index
            MUXPDO.index[MUXPDO.numberOfScannerObjects] = index;

            // Store the object sub index
            MUXPDO.subIndex[MUXPDO.numberOfScannerObjects] = subIndex;

            // Store the object data length.
            MUXPDO.dataLength[MUXPDO.numberOfScannerObjects] = dataLen;

            // Increment the number of valid mapped objects in the scanner list.
            MUXPDO.numberOfScannerObjects++;
        }

    }

}


/******************************************************************************/
UNSIGNED32 CO_ODF_RPDOcom( void       *object,
                           UNSIGNED16  index,
                           UNSIGNED8   subIndex,
                           UNSIGNED16 *pLength,
                           UNSIGNED16  attribute,
                           UNSIGNED8   dir,
                           void       *dataBuff,
                           const void *pData)
{
    CO_RPDO_t *RPDO;
    UNSIGNED32 dataBuffCopy, abortCode;
    unsigned char *pDest;
    unsigned char *pSource;

    RPDO = (CO_RPDO_t*)object;   // this is the correct pointer type of the first argument

    // Reading Object Dictionary variable
    if(dir == 0)
    {
        abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);
        if(abortCode==0 && subIndex==1)
        {
            // value in dataBuff is little endian as CANopen, so invert it back
            //memcpySwap4((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
            pDest = (UNSIGNED8*)&dataBuffCopy;
            pSource = (UNSIGNED8*)dataBuff;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            *(pDest+2) = *(pSource+2);
            *(pDest+3) = *(pSource+3);

            // if default COB ID is used, write default value here
            if((UNSIGNED16)dataBuffCopy == RPDO->defaultCOB_ID && RPDO->defaultCOB_ID)
                dataBuffCopy += RPDO->nodeId;

            // If PDO is not valid, set bit 31
            if(!RPDO->valid)
                dataBuffCopy |= 0x80000000L;

            //memcpySwap4((UNSIGNED8*)dataBuff, (UNSIGNED8*)&dataBuffCopy);
            pDest = (UNSIGNED8*)dataBuff;
            pSource = (UNSIGNED8*)&dataBuffCopy;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            *(pDest+2) = *(pSource+2);
            *(pDest+3) = *(pSource+3);
        }
        return abortCode;
    }

    // Writing Object Dictionary variable
    if(RPDO->restrictionFlags & 0x04)
        return 0x06010002L;  // Attempt to write a read only object.
    if(NMT.operatingState == CO_NMT_OPERATIONAL && (RPDO->restrictionFlags & 0x01))
        return 0x08000022L;   // Data cannot be transferred or stored to the application because of the present device state.

    switch(subIndex)
    {
        UNSIGNED32 curentData;
        case 1: // COB_ID
            //memcpySwap4((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
            pDest = (UNSIGNED8*)&dataBuffCopy;
            pSource = (UNSIGNED8*)dataBuff;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            *(pDest+2) = *(pSource+2);
            *(pDest+3) = *(pSource+3);

            curentData = *((const UNSIGNED32*)pData);

            // bits 11...29 must be zero
            if(dataBuffCopy&0x3FFF8000L)
                return 0x06090030L;  // Invalid value for parameter (download only).

            // if default COB-ID is being written, write defaultCOB_ID without nodeId
            if((UNSIGNED16)dataBuffCopy == (RPDO->defaultCOB_ID + RPDO->nodeId))
            {
                dataBuffCopy &= 0xC0000000L;
                dataBuffCopy += RPDO->defaultCOB_ID;
            }
            // if PDO is valid, bits 0..29 can not be changed
            if(RPDO->valid && ((dataBuffCopy^curentData)&0x3FFFFFFFL))
                return 0x06090030L;  // Invalid value for parameter (download only).
            // Write dataBuff back to CANopens little endian format
            //memcpySwap4((UNSIGNED8*)dataBuff, (UNSIGNED8*)&dataBuffCopy);
            pDest = (UNSIGNED8*)dataBuff;
            pSource = (UNSIGNED8*)&dataBuffCopy;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            *(pDest+2) = *(pSource+2);
            *(pDest+3) = *(pSource+3);
            break;

        case 2: // Transmission_type
            dataBuffCopy = *((UNSIGNED8*)dataBuff);
            // values from 241...253 are not valid
            if(dataBuffCopy>=241 && dataBuffCopy<=253)
                return 0x06090030L;  //Invalid value for parameter (download only).
            break;
    }

    abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);

    // Configure RPDO
    if(abortCode == 0 && subIndex == 1)
        CO_RPDOconfigCom(RPDO, dataBuffCopy);

    return abortCode;
}


/******************************************************************************/
UNSIGNED32 CO_ODF_TPDOcom( void       *object,
                           UNSIGNED16  index,
                           UNSIGNED8   subIndex,
                           UNSIGNED16 *pLength,
                           UNSIGNED16  attribute,
                           UNSIGNED8   dir,
                           void       *dataBuff,
                           const void *pData)
{
    CO_TPDO_t *TPDO;
    UNSIGNED32 dataBuffCopy, abortCode;
    unsigned char *pDest;
    unsigned char *pSource;

    TPDO = (CO_TPDO_t*)object;   // this is the correct pointer type of the first argument

    if(subIndex==4)
        return 0x06090011L;  // Sub-index does not exist.

    // Reading Object Dictionary variable
    if(dir == 0)
    {
        abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);
        if(abortCode==0)
        {
            switch(subIndex)
            {
                case 1: // COB_ID
                    // value in dataBuff is little endian as CANopen, so invert it back
                    //memcpySwap4((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
                    pDest = (UNSIGNED8*)&dataBuffCopy;
                    pSource = (UNSIGNED8*)dataBuff;
                    *pDest = *pSource;
                    *(pDest+1) = *(pSource+1);
                    *(pDest+2) = *(pSource+2);
                    *(pDest+3) = *(pSource+3);

                    // If default COB ID is used, write default value here
                    if((UNSIGNED16)dataBuffCopy == TPDO->defaultCOB_ID && TPDO->defaultCOB_ID)
                        dataBuffCopy += TPDO->nodeId;
                    // If PDO is not valid, set bit 31
                    if(!TPDO->valid)
                        dataBuffCopy |= 0x80000000L;
                    //memcpySwap4((UNSIGNED8*)dataBuff, (UNSIGNED8*)&dataBuffCopy);
                    pDest = (UNSIGNED8*)dataBuff;
                    pSource = (UNSIGNED8*)&dataBuffCopy;
                    *pDest = *pSource;
                    *(pDest+1) = *(pSource+1);
                    *(pDest+2) = *(pSource+2);
                    *(pDest+3) = *(pSource+3);
                    break;
            }
        }
        return abortCode;
    }

    // Writing Object Dictionary variable
    if(TPDO->restrictionFlags & 0x04)
        return 0x06010002L;  // Attempt to write a read only object.
    if(NMT.operatingState == CO_NMT_OPERATIONAL && (TPDO->restrictionFlags & 0x01))
        return 0x08000022L;   // Data cannot be transferred or stored to the application because of the present device state.

    switch(subIndex)
    {
        UNSIGNED32 curentData;
        case 1: // COB_ID
            //memcpySwap4((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
            pDest = (UNSIGNED8*)&dataBuffCopy;
            pSource = (UNSIGNED8*)dataBuff;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            *(pDest+2) = *(pSource+2);
            *(pDest+3) = *(pSource+3);

            curentData = *((const UNSIGNED32*)pData);

            // bits 11...29 must be zero
            if(dataBuffCopy&0x3FFF8000L)
                return 0x06090030L;  //Invalid value for parameter (download only).

            // if default COB-ID is being written, write defaultCOB_ID without nodeId
            if((UNSIGNED16)dataBuffCopy == (TPDO->defaultCOB_ID + TPDO->nodeId))
            {
                dataBuffCopy &= 0xC0000000L;
                dataBuffCopy += TPDO->defaultCOB_ID;
            }
            // if PDO is valid, bits 0..29 can not be changed
            if(TPDO->valid && ((dataBuffCopy^curentData)&0x3FFFFFFFL))
                return 0x06090030L;  // Invalid value for parameter (download only).
            // write dataBuff back to CANopens little endian format
            //memcpySwap4((UNSIGNED8*)dataBuff, (UNSIGNED8*)&dataBuffCopy);
            pDest = (UNSIGNED8*)dataBuff;
            pSource = (UNSIGNED8*)&dataBuffCopy;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            *(pDest+2) = *(pSource+2);
            *(pDest+3) = *(pSource+3);
            break;

        case 2: // Transmission_type
            dataBuffCopy = *((UNSIGNED8*)dataBuff);
            // values from 241...253 are not valid
            if(dataBuffCopy>=241 && dataBuffCopy<=253)
                return 0x06090030L;  // Invalid value for parameter (download only).
            break;

        case 3: // Inhibit_Time
            //memcpySwap2((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
            pDest = (UNSIGNED8*)&dataBuffCopy;
            pSource = (UNSIGNED8*)dataBuff;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);

            // if PDO is valid, value can not be changed
            if(TPDO->valid)
                return 0x06090030L;  //Invalid value for parameter (download only).
            break;

         case 5: // Event_Timer
            //memcpySwap2((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
            pDest = (UNSIGNED8*)&dataBuffCopy;
            pSource = (UNSIGNED8*)dataBuff;
            *pDest = *pSource;
            *(pDest+1) = *(pSource+1);
            break;

         case 6: // SYNC start value
            dataBuffCopy = *((UNSIGNED8*)dataBuff);
            // if PDO is valid, value can not be changed
            if(TPDO->valid)
                return 0x06090030L;  // Invalid value for parameter (download only).
            // values from 240...255 are not valid
            if(dataBuffCopy>240)
                return 0x06090030L;  //Invalid value for parameter (download only).
            break;
    }

    abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);

    // Configure TPDO
    if(abortCode == 0)
    {
        switch(subIndex)
        {
            case 1: // COB_ID
                CO_TPDOconfigCom(TPDO, dataBuffCopy, TPDO->syncFlag);
                TPDO->syncCounter = 255;
                break;

            case 2: // Transmission_type
                TPDO->syncFlag = ((UNSIGNED8)dataBuffCopy<=240) ? 1 : 0;
                TPDO->syncCounter = 255;
                break;

            case 3: // Inhibit_Time
                TPDO->inhibitTimer = 0;
                break;

            case 5: // Event_Timer
                TPDO->eventTimer = (UNSIGNED16)dataBuffCopy;
                break;
        }
    }
    return abortCode;
}


/******************************************************************************/
UNSIGNED32 CO_ODF_RPDOmap( void       *object,
                           UNSIGNED16  index,
                           UNSIGNED8   subIndex,
                           UNSIGNED16 *pLength,
                           UNSIGNED16  attribute,
                           UNSIGNED8   dir,
                           void       *dataBuff,
                           const void *pData)
{
    CO_RPDO_t *RPDO;
    UNSIGNED32 abortCode;
    UNSIGNED32 dataBuffCopy;
    unsigned char *pDest;
    unsigned char *pSource;

    RPDO = (CO_RPDO_t*)object;   // this is the correct pointer type of the first argument

    //Reading Object Dictionary variable
    if(dir == 0)
    {
        abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);
        if(abortCode==0 && subIndex==0)
        {
            //If there is error in mapping, dataLength is 0, so numberOfMappedObjects is 0.
            if(!RPDO->dataLength) *((UNSIGNED8*)dataBuff) = 0;
        }
        return abortCode;
    }

    // Writing Object Dictionary variable
    if(RPDO->restrictionFlags & 0x08)
        return 0x06010002L;  // Attempt to write a read only object.
    if(NMT.operatingState == CO_NMT_OPERATIONAL && (RPDO->restrictionFlags & 0x02))
        return 0x08000022L;   // Data cannot be transferred or stored to the application because of the present device state.
    if(RPDO->valid)
        return 0x06090030L;  // Invalid value for parameter (download only).

    // numberOfMappedObjects
    if(subIndex == 0)
    {
        if(*((UNSIGNED8*)dataBuff) > 8)
            return 0x06090031L;  //Value of parameter written too high.
    }
    else // mappedObject
    {
        UNSIGNED8* pData;
        UNSIGNED8 length = 0;
        UNSIGNED8 dummy = 0;
        UNSIGNED8 MBvar;
        UNSIGNED32 ret;

        if(RPDO->dataLength)
            return 0x06090030L;  //Invalid value for parameter (download only).

        // verify if mapping is correct
        //memcpySwap4((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
        pDest = (UNSIGNED8*)&dataBuffCopy;
        pSource = (UNSIGNED8*)dataBuff;
        *pDest = *pSource;
        *(pDest+1) = *(pSource+1);
        *(pDest+2) = *(pSource+2);
        *(pDest+3) = *(pSource+3);

        ret = CO_PDOfindMap( dataBuffCopy,
                             0,
                            &pData,
                            &length,
                            &dummy,
                            &MBvar);
        if(ret)
            return ret;
    }

    abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);

    // Configure mapping if subindex 0 was changed
    if(abortCode)
        RPDO->dataLength = 0;
    else if(subIndex == 0)
        abortCode = CO_RPDOconfigMap(RPDO, (OD_RPDOMappingParameter_t*) pData);

    return abortCode;
}


/******************************************************************************/
UNSIGNED32 CO_ODF_TPDOmap( void       *object,
                           UNSIGNED16  index,
                           UNSIGNED8   subIndex,
                           UNSIGNED16 *pLength,
                           UNSIGNED16  attribute,
                           UNSIGNED8   dir,
                           void       *dataBuff,
                           const void *pData    )
{
    CO_TPDO_t *TPDO;
    UNSIGNED32 abortCode;
    UNSIGNED32 dataBuffCopy;
    unsigned char *pDest;
    unsigned char *pSource;

    TPDO = (CO_TPDO_t*)object;   // this is the correct pointer type of the first argument

    // Reading Object Dictionary variable
    if(dir == 0)
    {
        abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);
        if(abortCode==0 && subIndex==0)
        {
            // If there is error in mapping, dataLength is 0, so numberOfMappedObjects is 0.
            if(!TPDO->dataLength)
                *((UNSIGNED8*)dataBuff) = 0;
        }
        return abortCode;
    }

    // Writing Object Dictionary variable
    if(TPDO->restrictionFlags & 0x08)
        return 0x06010002L;  // Attempt to write a read only object.
    if( NMT.operatingState == CO_NMT_OPERATIONAL && (TPDO->restrictionFlags & 0x02) )
        return 0x08000022L;   // Data cannot be transferred or stored to the application because of the present device state.
    if(TPDO->valid)
        return 0x06090030L;  // Invalid value for parameter (download only).

    // numberOfMappedObjects
    if(subIndex == 0)
    {
        if(*((UNSIGNED8*)dataBuff) > 8)
            return 0x06090031L;  // Value of parameter written too high.
    }
    else // mappedObject
    {
        UNSIGNED8* pData;
        UNSIGNED8 length = 0;
        UNSIGNED8 dummy = 0;
        UNSIGNED8 MBvar;
        UNSIGNED32 ret;

        if(TPDO->dataLength)
            return 0x06090030L;  //Invalid value for parameter (download only).

        // verify if mapping is correct
        //memcpySwap4((UNSIGNED8*)&dataBuffCopy, (UNSIGNED8*)dataBuff);
        pDest = (UNSIGNED8*)&dataBuffCopy;
        pSource = (UNSIGNED8*)dataBuff;
        *pDest = *pSource;
        *(pDest+1) = *(pSource+1);
        *(pDest+2) = *(pSource+2);
        *(pDest+3) = *(pSource+3);

        ret = CO_PDOfindMap( dataBuffCopy,
                             1,
                            &pData,
                            &length,
                            &dummy,
                            &MBvar);
        if(ret)
            return ret;
    }

    abortCode = CO_ODF(index, subIndex, *pLength, attribute, dir, dataBuff, pData);

    // Configure mapping if subindex 0 was changed
    if(abortCode)
        TPDO->dataLength = 0;
    else if(subIndex == 0)
        abortCode = CO_TPDOconfigMap(TPDO, (OD_TPDOMappingParameter_t*) pData);

    return abortCode;
}


/******************************************************************************/
INTEGER16 CO_RPDO_init(
      CO_RPDO_t                       *RPDO,
      UNSIGNED8                        nodeId,
      UNSIGNED16                       defaultCOB_ID,
      UNSIGNED8                        restrictionFlags,
const OD_RPDOCommunicationParameter_t *ObjDict_RPDOCommunicationParameter,
const OD_RPDOMappingParameter_t       *ObjDict_RPDOMappingParameter,
      UNSIGNED16                       ObjDictIndex_RPDOCommunicationParameter,
      UNSIGNED16                       ObjDictIndex_RPDOMappingParameter        )
{
    // Configure object variables
    RPDO->nodeId = nodeId;
    RPDO->defaultCOB_ID = defaultCOB_ID;
    RPDO->restrictionFlags = restrictionFlags;

    // Configure SDO server for first argument of CO_ODF_RPDOcom and CO_ODF_RPDOmap
    // TODO: FIX BELOW
    //CO_OD_configureArgumentForODF(ObjDictIndex_RPDOCommunicationParameter, (void*)RPDO);
    //CO_OD_configureArgumentForODF(ObjDictIndex_RPDOMappingParameter, (void*)RPDO);

    // configure communication and mapping
    RPDO->CANrxNew = 0;

    CO_RPDOconfigMap(RPDO, ObjDict_RPDOMappingParameter);
    CO_RPDOconfigCom(RPDO, ObjDict_RPDOCommunicationParameter->COB_IDUsedByRPDO);

    return CO_ERROR_NO;
}


/******************************************************************************/
INTEGER16 CO_RPDO1_init(
      UNSIGNED8                        nodeId,
      UNSIGNED16                       defaultCOB_ID,
      UNSIGNED8                        restrictionFlags,
const OD_RPDOCommunicationParameter_t *ObjDict_RPDOCommunicationParameter,
const OD_RPDOMappingParameter_t       *ObjDict_RPDOMappingParameter,
      UNSIGNED16                       ObjDictIndex_RPDOCommunicationParameter,
      UNSIGNED16                       ObjDictIndex_RPDOMappingParameter        )
{
    // Configure object variables
    RPDO.nodeId = nodeId;
    RPDO.defaultCOB_ID = defaultCOB_ID;
    RPDO.restrictionFlags = restrictionFlags;

    // Configure SDO server for first argument of CO_ODF_RPDOcom and CO_ODF_RPDOmap
    // TODO: FIX BELOW
    //CO_OD_configureArgumentForODF(ObjDictIndex_RPDOCommunicationParameter, (void*)&RPDO);
    //CO_OD_configureArgumentForODF(ObjDictIndex_RPDOMappingParameter, (void*)&RPDO);

    // configure communication and mapping
    RPDO.CANrxNew = 0;

    CO_RPDOconfigMap(&RPDO, ObjDict_RPDOMappingParameter);
    CO_RPDOconfigCom(&RPDO, ObjDict_RPDOCommunicationParameter->COB_IDUsedByRPDO);

    return CO_ERROR_NO;
}


/*
// -------------------------------------------------------------------------------------------------
//
INTEGER16 CO_TPDO_init(
      CO_TPDO_t                       *TPDO,
      UNSIGNED8                        nodeId,
      UNSIGNED16                       defaultCOB_ID,
      UNSIGNED8                        restrictionFlags,
const OD_TPDOCommunicationParameter_t *ObjDict_TPDOCommunicationParameter,
const OD_TPDOMappingParameter_t       *ObjDict_TPDOMappingParameter,
      UNSIGNED16                       ObjDictIndex_TPDOCommunicationParameter,
      UNSIGNED16                       ObjDictIndex_TPDOMappingParameter        )
{
    // Configure object variables
    TPDO->nodeId = nodeId;
    TPDO->defaultCOB_ID = defaultCOB_ID;
    TPDO->restrictionFlags = restrictionFlags;
    TPDO->ObjDict_TPDOCommunicationParameter = ObjDict_TPDOCommunicationParameter;

    // Configure SDO server for first argument of CO_ODF_TPDOcom and CO_ODF_TPDOmap
    // TODO - FIX THIS
    CO_OD_configureArgumentForODF(ObjDictIndex_TPDOCommunicationParameter, (void*)TPDO);
    CO_OD_configureArgumentForODF(ObjDictIndex_TPDOMappingParameter, (void*)TPDO);

    // configure communication and mapping
    TPDO->syncCounter = 255;
    TPDO->inhibitTimer = 0;
    TPDO->eventTimer = ObjDict_TPDOCommunicationParameter->eventTimer;
    TPDO->SYNCtimerPrevious = 0;
    if(ObjDict_TPDOCommunicationParameter->transmissionType >= 254)
        TPDO->sendRequest = 1;

    CO_TPDOconfigMap(TPDO, ObjDict_TPDOMappingParameter);
    CO_TPDOconfigCom(TPDO, ObjDict_TPDOCommunicationParameter->COB_IDUsedByTPDO, ((ObjDict_TPDOCommunicationParameter->transmissionType<=240) ? 1 : 0));

    if( (ObjDict_TPDOCommunicationParameter->transmissionType > 240 && ObjDict_TPDOCommunicationParameter->transmissionType < 254)
            || ObjDict_TPDOCommunicationParameter->SYNCStartValue > 240 )
    {
        TPDO->valid = 0;
    }

    return CO_ERROR_NO;
}
*/

// -------------------------------------------------------------------------------------------------
//
unsigned int CO_TPDO_init(
      UNSIGNED8                        nodeId,
      UNSIGNED16                       defaultCOB_ID,
      UNSIGNED8                        restrictionFlags,
const OD_TPDOCommunicationParameter_t *ObjDict_TPDOCommunicationParameter,
const OD_TPDOMappingParameter_t       *ObjDict_TPDOMappingParameter,
      UNSIGNED16                       ObjDictIndex_TPDOCommunicationParameter,
      UNSIGNED16                       ObjDictIndex_TPDOMappingParameter        )
{
    // *** Configure TPDO1 object variables ***
    TPDO.nodeId = nodeId;
    TPDO.defaultCOB_ID = CAN_ID_TPDO0;
    TPDO.restrictionFlags = 0;
    TPDO.ObjDict_TPDOCommunicationParameter = &OD_TPDOCommunicationParameter;

    // Configure SDO server for first argument of CO_ODF_TPDOcom and CO_ODF_TPDOmap
    //CO_OD_configureArgumentForODF(0x1800, (void*)&TPDO);     // TPDO Communication Parameters
    //CO_OD_configureArgumentForODF(0x1A00, (void*)&TPDO);     // TPDO Mapping Parameter
    //CO_OD_configureArgumentForODF(0x3103, (void*)&TPDO);

    // Configure communication and mapping for TPDO 1
    TPDO.syncCounter = 255;
    TPDO.inhibitTimer = 0;
    TPDO.eventTimer = OD_TPDOCommunicationParameter.eventTimer;
    TPDO.SYNCtimerPrevious = 0;
    if(OD_TPDOCommunicationParameter.transmissionType >= 254)
        TPDO.sendRequest = 1;

    CO_TPDOconfigMap(&TPDO, OD_TPDOMappingParameter);
    //CO_TPDOconfigCom( &TPDO, OD_TPDOCommunicationParameter.COB_IDUsedByTPDO,
    //                 ( (OD_TPDOCommunicationParameter.transmissionType <= 240) ? 1 : 0) );
    if( ( OD_TPDOCommunicationParameter.transmissionType > 240
          && OD_TPDOCommunicationParameter.transmissionType < 254)
            || OD_TPDOCommunicationParameter.SYNCStartValue > 240 )
    {
        TPDO.valid = 0;
    }
    else
        TPDO.valid = 1;


    // *** Configure MUX PDO ("scannerlist") object variables ***
    MUXPDO.nodeId = nodeId;
    MUXPDO.defaultCOB_ID = CAN_ID_RPDO3;
    MUXPDO.ObjDict_TPDOCommunicationParameter = &OD_MUXPDOCommunicationParameter;
    MUXPDO.scannerIndex = 0;

    // Configure MUX PDO communication
    MUXPDO.syncCounter = 255;
    MUXPDO.inhibitTimer = 0;
    MUXPDO.eventTimer = OD_MUXPDOCommunicationParameter.eventTimer;
    MUXPDO.SYNCtimerPrevious = 0;
    if(OD_MUXPDOCommunicationParameter.transmissionType >= 254)
        MUXPDO.sendRequest = 1;
    //CO_TPDOconfigCom( &MUXPDO, OD_MUXPDOCommunicationParameter.COB_IDUsedByTPDO,
    //                  ( (OD_MUXPDOCommunicationParameter.transmissionType <= 240) ? 1 : 0) );
    if( ( OD_MUXPDOCommunicationParameter.transmissionType > 240
          && OD_MUXPDOCommunicationParameter.transmissionType < 254)
            || OD_MUXPDOCommunicationParameter.SYNCStartValue > 240 )
    {
        MUXPDO.valid = 0;
    }
    else
        MUXPDO.valid = 1;

    // Configure the MUX PDO mapping
    CO_MUXPDOconfigMap();



/*
    unsigned char * tmpPointer;

    tmpPointer = (unsigned char *)&OD_statusWord;
    TPDO.mapPointer[0] = tmpPointer;
    TPDO.mapPointer[1] = tmpPointer+1;

    tmpPointer = (unsigned char *)&OD_edgeValue.edgeValue1;
    TPDO.mapPointer[2] = tmpPointer;
    TPDO.mapPointer[3] = tmpPointer+1;
    TPDO.mapPointer[4] = tmpPointer+2;

    tmpPointer = (unsigned char *)&OD_edgeValue.edgeValue2;
    TPDO.mapPointer[5] = tmpPointer;
    TPDO.mapPointer[6] = tmpPointer+1;
    TPDO.mapPointer[7] = tmpPointer+2;

    TPDO.dataLength = 8;
*/
    //unsigned char * tmpPointer;
    //tmpPointer = (unsigned char *)&OD_statusWord;
    //TPDO.mapPointer[0] = tmpPointer;
    //TPDO.mapPointer[1] = tmpPointer+1;

    return CO_ERROR_NO;
}



/******************************************************************************/
UNSIGNED8 CO_TPDOisCOS(CO_TPDO_t *pTPDO)
{
    // Prepare TPDO data automatically from Object Dictionary variables
    UNSIGNED8* pPDOdataByte;
    UNSIGNED8** ppODdataByte;
    unsigned char value = 0;

    // TODO: FIX THIS
    //pPDOdataByte = TPDO.CANtxBuff->data[TPDO->dataLength];
    ppODdataByte = &pTPDO->mapPointer[pTPDO->dataLength];

    switch(pTPDO->dataLength)
    {
        case 8:
            if( *(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x80) )
                value = 1;
            break;
        case 7:
            if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x40))
                value = 1;
            break;
        case 6: 
           if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x20))
               value = 1;
           break;
        case 5: 
           if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x10))
               value = 1;
           break;
        case 4: 
           if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x08))
               value = 1;
           break;
        case 3: 
           if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x04))
               value = 1;
           break;
        case 2:
            if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x02))
                value = 1;
            break;
        case 1:
            if(*(--pPDOdataByte) != **(--ppODdataByte) && (pTPDO->sendIfCOSFlags&0x01))
                value = 1;
            break;
    }

    return value;
}


/******************************************************************************/
void CO_TPDOsend(CO_TPDO_t *pTPDO)
{
    unsigned char i, length;
    unsigned char **ppODdata;  // Pointer to OD data to send
    CO_CANmsg_t msg;

    ppODdata = &pTPDO->mapPointer[0];     // The array of pointers to TPDO data.
    length = pTPDO->dataLength;

    // Copy data from the OD to the message buffer
    msg.COB_ID = pTPDO->defaultCOB_ID + pTPDO->nodeId;
    msg.dataLen = length;


    //unsigned char * testPtr = 0;
    //testPtr = (unsigned char *)&OD_statusWord;
    //testPtr = (unsigned char *)&OD_edgeValue.edgeValue1;
//
    //if (testPtr != *ppODdata)
    //{
    //    msg.data[0] = OD_statusWord & 0x00FF;
    //    msg.data[1] = (OD_statusWord & 0xFF00)>>8;
     //   msg.data[2] = OD_edgeValue.edgeValue1 & 0x0000FF;
    //    msg.data[3] = (OD_edgeValue.edgeValue1 & 0x00FF00) >> 8;
    //    msg.data[4] = (OD_edgeValue.edgeValue1 & 0xFF0000) >> 16;
    //}


    for(i=0; i<length; i++)
        msg.data[i] = **(ppODdata + i);
    
    pTPDO->sendRequest = 0;

    // Send the data copied to the message buffer.
    CO_CANsend(msg);
}


/******************************************************************************/
void CO_RPDO_process(CO_RPDO_t *RPDO)
{
    unsigned char i, length;
    unsigned char *pPDOdata;        // Pointer to RPDO data to copy
    unsigned char **ppODdata;       // Pointer to OD data to overwrite

    if( RPDO->CANrxNew && RPDO->valid && (NMT.operatingState == CO_NMT_OPERATIONAL))
    {
        pPDOdata = RPDO->CANrxData;
        ppODdata = &RPDO->mapPointer[0];
        length = RPDO->dataLength;

        // TODO: Check this...
        // Copy data from the PDO buffer to the OD object.
        for(i=0; i<length; i++)
        {
            **(ppODdata + i) = *(pPDOdata + i);
        }
    }
    RPDO->CANrxNew = 0;
}


// -------------------------------------------------------------------------------------------------
//   Function: CO_TPDO_process
//
//   Process transmitting PDO messages.
//
//   Function must be called cyclically in any NMT state. It prepares and sends
//  TPDO if necessary. If Change of State needs to be detected, function
//   <CO_TPDOisCOS> must be called before.
//
//   Parameters:
//      TPDO                 - Pointer to TPDO object <CO_TPDO_t>.
//      timeDifference_100us - Time difference from previous function call in [100 * microseconds].
//      timeDifference_ms    - Time difference from previous function call in [milliseconds].
// -------------------------------------------------------------------------------------------------
void CO_TPDO_process(   CO_TPDO_t     *pTPDO,
                        UNSIGNED16     timeDifference_100us,
                        UNSIGNED16     timeDifference_ms    )
{
    long int i;

    if( pTPDO->valid && (NMT.operatingState == CO_NMT_OPERATIONAL) )
    {
        // Send PDO by application request or by Event timer
        if(pTPDO->ObjDict_TPDOCommunicationParameter->transmissionType >= 253)
        {
            if( (pTPDO->inhibitTimer == 0) && (pTPDO->sendRequest || (pTPDO->ObjDict_TPDOCommunicationParameter->eventTimer && (pTPDO->eventTimer == 0))) )
            {
                CO_TPDOsend(pTPDO);
                pTPDO->inhibitTimer = pTPDO->ObjDict_TPDOCommunicationParameter->inhibitTime;
                pTPDO->eventTimer = pTPDO->ObjDict_TPDOCommunicationParameter->eventTimer;
            }
        }
        else if(CO_SYNC.running) // Synchronous PDOs
        {
            // Detect SYNC message
            if( CO_SYNC.timer < pTPDO->SYNCtimerPrevious )
            {
                // Send synchronous acyclic PDO
                if(pTPDO->ObjDict_TPDOCommunicationParameter->transmissionType == 0)
                {
                    if(pTPDO->sendRequest)
                        CO_TPDOsend(pTPDO);
                }
                else // Send synchronous cyclic PDO
                {
                    // is the start of synchronous TPDO transmission
                    if(pTPDO->syncCounter == 255)
                        pTPDO->syncCounter = pTPDO->ObjDict_TPDOCommunicationParameter->transmissionType;
                   
                    // Send PDO after every N-th Sync
                    if(--pTPDO->syncCounter == 0)
                    {
                        pTPDO->syncCounter = pTPDO->ObjDict_TPDOCommunicationParameter->transmissionType;
                        CO_TPDOsend(pTPDO);
                    }
                }
            }
        }
    }
    else
    {
        // Not operational or valid. Force TPDO first send after operational or valid.
        pTPDO->sendRequest = 1;
    }

    // Update timers
    i = pTPDO->inhibitTimer;
    i -= timeDifference_100us;      // TODO: THIS LINE BREAKS STUFF
    pTPDO->inhibitTimer = (i<=0) ? 0 : (UNSIGNED16)i;

    i = pTPDO->eventTimer;
    i -= timeDifference_ms;
    pTPDO->eventTimer = (i<=0) ? 0 : (UNSIGNED16)i;

    pTPDO->SYNCtimerPrevious = CO_SYNC.timer;
}



// ------------------------------------------------------------------------------------------------
//
void CO_scannerlist_process( CO_MUXPDO_t  *pMUXPDO,
                             UNSIGNED16    timeDifference_100us,
                             UNSIGNED16    timeDifference_ms    )
{
    INTEGER32 i;
    CO_CANmsg_t msg;

    // Check if it is time to send the message
    if( pMUXPDO->valid && (NMT.operatingState == CO_NMT_OPERATIONAL) )
    {
        if(CO_SYNC.running) // Synchronous PDOs
        {
            // Detect SYNC message
            if( CO_SYNC.timer < pMUXPDO->SYNCtimerPrevious )
            {
                // Send synchronous acyclic PDO
                if(pMUXPDO->ObjDict_TPDOCommunicationParameter->transmissionType == 0)
                {
                    if(pMUXPDO->sendRequest)
                    {
                        // Build MUX PDO message
                        CO_scannerlist_msg_build(&msg, pMUXPDO);

                        // Send the MUX PDO message
                        CO_CANsend(msg);
                        
                        // Increment the scannerlist index
                        pMUXPDO->scannerIndex++;
                    }
                }
                else // Send synchronous cyclic PDO
                {
                    // is the start of synchronous TPDO transmission
                    if(pMUXPDO->syncCounter == 255)
                        pMUXPDO->syncCounter = pMUXPDO->ObjDict_TPDOCommunicationParameter->transmissionType;

                    // Send MUX PDO after every N-th Sync
                    if(--pMUXPDO->syncCounter == 0)
                    {
                        pMUXPDO->syncCounter = pMUXPDO->ObjDict_TPDOCommunicationParameter->transmissionType;

                        // Build MUX PDO message
                        CO_scannerlist_msg_build(&msg, pMUXPDO);

                        // Send the MUX PDO message
                        CO_CANsend(msg);

                        // Increment the scannerlist index
                        pMUXPDO->scannerIndex++;
                    }
                }
            }
        }
    }
    else
    {
        // Not operational or valid. Force TPDO first send after operational or valid.
        pMUXPDO->sendRequest = 1;
    }

    // Reset index when all scannerlist objects have been transmitted.
    if (pMUXPDO->scannerIndex >= pMUXPDO->numberOfScannerObjects)
        pMUXPDO->scannerIndex = 0;

    // Update timers
    i = pMUXPDO->inhibitTimer;
    i -= timeDifference_100us;      // TODO: THIS LINE BREAKS STUFF
    pMUXPDO->inhibitTimer = (i<=0) ? 0 : (UNSIGNED16)i;

    i = pMUXPDO->eventTimer;
    i -= timeDifference_ms;
    pMUXPDO->eventTimer = (i<=0) ? 0 : (UNSIGNED16)i;

    pMUXPDO->SYNCtimerPrevious = CO_SYNC.timer;
}


// ------------------------------------------------------------------------------------------------
//
void CO_scannerlist_msg_build(CO_CANmsg_t *msg, CO_MUXPDO_t *pMUXPDO)
{
    //UNSIGNED32 *pMap;   // Pointer to map - all maps are 32 bits
    UNSIGNED16 index;
    UNSIGNED8 subIndex;
    UNSIGNED8 dataLen;
    UNSIGNED8 i;
    //UNSIGNED8 scannerIndex;
    //const sCO_OD_object *pODE;
    UNSIGNED8 *pData;

/*
    // Get the current scannerlist mapping parameter for the object to be transmitted.
    scannerIndex = pMUXPDO->scannerIndex;
    pMap = &OD_scannerlistMapping.offset;   // Pointer to first mapped object in scannerlist.
    pMap += scannerIndex;                   // Pointer to current mapped object in scannerlist.

    // Get the index, subindex, and length of the associated object.
    index = (UNSIGNED16)(*pMap >> 16);    // OD Index
    subIndex = (UNSIGNED8)(*pMap >> 8);   // OD subindex
    dataLen = (UNSIGNED8) *pMap;          // OD data length in bits

    // Data length must be a multiple of 8 bits (must be byte aligned).
    //if(dataLen & 0x07)
    //    return; // Object cannot be mapped to the PDO.
    // Convert data length from number of bits to number of bytes.
    dataLen = dataLen >> 3;
    if (dataLen > 4)
        return -1;     // Data cannot be longer then 4 bytes.

    // Find the object in the Object Dictionary
    pODE = CO_OD_find(index, 0);
    // Does object exist in OD?
    if((!pODE) || (subIndex > pODE->maxSubIndex))
        return -1; // Object does not exist in the object dictionary.
    // Get pointer to object data
    pData = (UNSIGNED8*)CO_OD_getDataPointer(*pODE, subIndex);
 */


    // Get data pointer and object specifics.
    pData = pMUXPDO->mapPointer[pMUXPDO->scannerIndex];
    index = pMUXPDO->index[pMUXPDO->scannerIndex];          // OD Index
    subIndex = pMUXPDO->subIndex[pMUXPDO->scannerIndex];    // OD subindex
    dataLen = pMUXPDO->dataLength[pMUXPDO->scannerIndex];   // Number of bytes


    // Build the MUX PDO message to send
    msg->COB_ID = (pMUXPDO->defaultCOB_ID + pMUXPDO->nodeId);
    msg->dataLen = 8;                   // Always 8 bytes
    msg->data[0] = pMUXPDO->nodeId;     // Node ID
    msg->data[1] = index;               // Object index Low Byte
    msg->data[2] = index >> 8;          // Object index High Byte
    msg->data[3] = subIndex;            // Object sub-index
    for (i=0; i<dataLen; i++)           // Copy data bytes
        msg->data[4+i] = *(pData+i);    // Data
    while (i<4)                         // Copy 0x00 to any remaining bytes
    {
        msg->data[4+i] = 0x00;
        i++;
    }

}









