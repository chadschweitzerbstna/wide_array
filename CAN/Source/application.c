
#include <xc.h>
#include "../Header/application.h"
#include "../Stack/CANopen.h"
#include "../Stack/LSS.h"
#include "../../main.h"
#include "../../can.h"
#include "../../ctl.h"
#include "../../detector.h"



// *** Internal (file scope) variables ***
static volatile unsigned int CAN_1msTimer;      // Millisecond count (used for CANopen Async. processes)
static unsigned int CAN_1msTimerPrevious = 0;   // Previous millisecond count (used for CANopen Async. processes)
static long int edge1 = 0,
                edge2 = 0,
                edge1TOP = 0,
                edge1BOTTOM = 0,
                edge2TOP = 0,
                edge2BOTTOM = 0;

#define DEADBAND 0//250

static char countUp;
static enum {
    CO_INVALID_STATE = 3,
    CO_DEVICE_RESET = 2,
    CO_COMM_RESET = 1,
    CO_COMM_OPERATIONAL = 0
} nodeState, lastNodeState;


static unsigned char stateError = 0;


// *** Internal function prototypes ***
static void create_new_edge_values(unsigned int edgePosition1, unsigned int edgePosition2);


// -------------------------------------------------------------------------------------------------
//  The primary CANopen device process
//
unsigned char do_CANopen_tasks(unsigned char msTick, unsigned int edgeValue1, unsigned int edgeValue2)
{
    // The CANopen node communication state machine.
    // This determines whether the node is reset or active.
    if (nodeState != lastNodeState)
    {
        // A change of state has just occurred.
        // Update last state
        lastNodeState = nodeState;

        // Handle the node state change
        switch(nodeState)
        {
            // CANopen device reset
            case CO_DEVICE_RESET:
                // Disable all CAN Interrupts (and clear the flag).
                IEC1bits.CAN1IE = 0;
                
                // Flush the software CAN TX buffer
                CO_CANmodule.CANtxCount = 0;
                CO_CANmodule.CO_TX_buffer.head = 0;
                CO_CANmodule.CO_TX_buffer.tail = 0;
                
                // Update node state
                nodeState = CO_COMM_RESET;
                break;

            // CANopen communication reset
            case CO_COMM_RESET:
                // Disable timer and CAN interrupts, configure CANopen node,
                //  and re-enable timer and interrupts.
                init_CANopen();

                // TODO: Clear or reset any global OD entries??
                CAN_1msTimer = 0;
                CAN_1msTimerPrevious = 0;

                // Update node state
                nodeState = CO_COMM_OPERATIONAL;
                break;

            case CO_COMM_OPERATIONAL:
                IEC1bits.CAN1IE = 1;                // CAN Interrupt Enabled.

                // CAN communication active (handled in steady state section below)
                break;
        }
    }
    else
    {
        // Steady state operation
        switch(nodeState)
        {
            case CO_COMM_OPERATIONAL:
                // CAN communication active.  Update nodeState.
                nodeState = CANopen_main_process(msTick, edgeValue1, edgeValue2);
                break;

            default:
                stateError = 1;
                break;
        }
    }

    // Return the node state for informative purposes.
    return nodeState;
}



// -------------------------------------------------------------------------------------------------
//
// -------------------------------------------------------------------------------------------------
void init_CANopen()
{
    // Disable CAN interrupt
    IEC1bits.CAN1IE = 0;                // CAN Interrupt Disabled.
    IFS1bits.CAN1IF = 0;                // Clear Interrupt Flag.

    // Configure the PIC32 CAN hardware and filters.
    init_CAN1_hardware(OD_CANNodeID);

    // Initialize CANopen application.
    CO_init();
}


// -------------------------------------------------------------------------------------------------
//  Main CANopen process.  This should be called inside the main while loop.
// -------------------------------------------------------------------------------------------------
unsigned char CANopen_main_process(unsigned char msTickFlag, unsigned int edgePosition1, unsigned int edgePosition2)
{
    unsigned char reset = 0;
    CO_CANmsg_t msg;
    unsigned int elapsedTime = 0;
    //unsigned int whatIsThis;
    
    // <editor-fold defaultstate="collapsed" desc="Receive incomming CAN messages and dispatch to appropriet CO object">
    // *** CANopen asynchronous processes ***
    // Check for any incoming CAN messages and dispatch to appropriate CO object
    if ( CAN_RX_check() )
    {
        // If a message is ready, copy it to the RX buffer.
        CAN_getMsg( &msg.COB_ID, &msg.dataLen, msg.data);

        // Known, fixed COB ID's regardless of nodeID.
        switch (msg.COB_ID)
        {
            case 0x000:
                CO_NMT_receive(msg);    // Update device operation state, etc.
                break;

            case 0x080:
                CO_SYNC_receive(msg);   // SYNC object
                break;

            case LSS_SLAVE_RX_COB_ID:       // LSS
                CO_LSS_receive(msg);
                break;

            default:
                // All Mux PDOs (any node ID), plus RX SDOs with our node ID (after CAN module HW filtering).
                switch (msg.COB_ID & 0xF00)
                {
                    case 0x500:                 // ALL Mux PDOs (RX PDO4 - Receive all IDs from 0x500 to 0x57F).
                        CO_MUX_PDO_receive(msg, OD_CANNodeID);
                        break;

                    case 0x600:                 // RX SDOs that match our nodeID
                        CO_SDO_receive(msg);
                        break;

                    default:
                    //    whatIsThis = msg.COB_ID;
                        break;
                }
                break;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Run CANopen asynchronous processes">
    // *** Run the general CANopen asynchronous process ***
    // Update time since last process run.
    if (CAN_1msTimer >= CAN_1msTimerPrevious)   // Normal operation
        elapsedTime = CAN_1msTimer - CAN_1msTimerPrevious;
    else                                        // Timer overflow/rollover
        elapsedTime = (0xFFFF - CAN_1msTimerPrevious) + CAN_1msTimer;   

    // Run CANopen general processes (NMT, SDO, Emergency, HBconsumer)
    // This also produces a reset command if needed.
    reset = CO_process(elapsedTime);
    
    // Previous time now equals current time.
    CAN_1msTimerPrevious = CAN_1msTimer;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Run CANopen 1ms processes">
    // *** CANopen 1ms processes ***
    if (msTickFlag == 1)
    {
        // Increment CANopen system timer variable.
        CAN_1msTimer++;

        // Check for a node ID change and issue a device reset if necessary.
        if (CO_LSS.NodeChangedDelayTimer > 1)
        {
            // If the timer is greater then 1, decrement the timer.
            CO_LSS.NodeChangedDelayTimer--;

            // Change the node ID and reset the device if the timer has expired.
            if (CO_LSS.NodeChangedDelayTimer == 1)
            {
                // Set new global node ID
                OD_CANNodeID = CO_LSS.DynamicNode;

                // Return device reset command
                reset = CO_DEVICE_RESET;
                return reset;
            }
        }

        // Process Received PDOs and run sync process
        CO_process_RPDO();

        // Create new edge values.  This updates edge1 and edge2.
        create_new_edge_values(edgePosition1, edgePosition2);

        // Update sensor status (enabled or disabled).
/*
        tmpStatus = 0x0880;         // Both sensors disabled (locked)
        if (SENSOR_1_ENABLED == TRUE)
            tmpStatus &= ~0x0800;   // Clear bit to enable sensor 1
        if ((SENSOR_2_ENABLED == TRUE) || centerlineMode)
            tmpStatus &= ~0x0080;   // Clear bit to enable sensor 2
*/

        // Write new edge and status values to the Object Dictionary (OD)
        OD_statusWord = make_status_word();
        
        // Offsets are applied to the edges before being reported
        OD_edgeValue.edgeValue1 = edge1 - OD_processValueOFFSET[ODA_processValueOFFSET_1];  // Object 0x7130
        OD_edgeValue.edgeValue2 = edge2 - OD_processValueOFFSET[ODA_processValueOFFSET_2];
        OD_processValue[ODA_processValue_firstEdgeInUm] = OD_edgeValue.edgeValue1;          // Object 0x9130[1]
        OD_processValue[ODA_processValue_secondEdgeInUm] = OD_edgeValue.edgeValue2;         // Object 0x9130[2]

        // Process/Send Transmit PDOs (and MUX PDO)
        CO_process_TPDO();
    }
    // </editor-fold>

    return reset;
}


// -------------------------------------------------------------------------------------------------
// Edge values must be inverted and then converted to metric for the BST controller.
//
void create_new_edge_values(unsigned int edgePosition1, unsigned int edgePosition2)
{
    long int tmpEdge;

    // *** EDGE 1
    // Invert for BST controller
    tmpEdge = 0x8000 - edgePosition1;
    tmpEdge = tmpEdge - 0x4000;

    // Convert from thousandths of an inch (0.001") to micrometers (0.000001m)
    tmpEdge = tmpEdge * 127;
    tmpEdge = tmpEdge / 5;

    // Update Edge 1
    edge1 = tmpEdge;


    // *** EDGE 2
    // Invert for BST controller
    tmpEdge = 0x8000 - edgePosition2;
    tmpEdge = tmpEdge - 0x4000;

    // Convert from thousandths of an inch (0.001") to micrometers (0.000001m)
    tmpEdge = tmpEdge * 127;
    tmpEdge = tmpEdge / 5;

    // Update Edge 2
    edge2 = tmpEdge;
}



// ------------------------------------------------------------------------------------------------
//
void CO_look_for_cal_cmd(CO_CANmsg_t msg)
{
    unsigned int object;
    unsigned char subIndex;
    unsigned long value;

    if( NMT.operatingState == CO_NMT_OPERATIONAL )
    {
        // Process the MUX PDO based on the object
        object = msg.data[2] << 8;
        object = object + msg.data[1];

        switch (object)
        {
            case 0x3110:
                value = msg.data[4];
                OD_materialSetupControlWord = (unsigned char)value;

                break;

            default:
                break;
        }
    }
}


// ------------------------------------------------------------------------------------------------
//
//
void reset_CAN_node(void)
{
    nodeState = CO_DEVICE_RESET;
    lastNodeState = CO_INVALID_STATE;
}


// ------------------------------------------------------------------------------------------------
//
unsigned int make_status_word(void)
{
    unsigned int newStatus;

    // SENSOR-OPERATING-MODE Flags
    newStatus = ANALOG_MODE_AT_CANBUS | EDGE_1_INVALID | EDGE_1_LOCKED | EDGE_2_INVALID | EDGE_2_LOCKED;

    // Update status based on sensor and edge states (first edge).
    newStatus &= ~EDGE_1_INVALID;           // Clear bit6 to enable sensor 1
    if (FIRST_EDGE_FOUND == TRUE)
        newStatus &= ~EDGE_1_LOCKED;        // Clear bit4 "sensor 1 locked" if edge found
    else
        newStatus |= EDGE_1_LOCKED;
    
    if (twoEdgeValues)
    {
        newStatus &= ~EDGE_2_INVALID;       // Clear bit7 to enable "sensor 2" (second edge)
        if (SECOND_EDGE_FOUND == TRUE)
            newStatus &= ~EDGE_2_LOCKED;    // Clear bit5 "sensor 2 locked" if edge found
        else
            newStatus |= EDGE_2_LOCKED;
    }

    return newStatus;
}