/*******************************************************************************

   File - CO_OD.c
   CANopen Object Dictionary.

   Copyright (C) 2004-2008 Janez Paternoster

   License: GNU Lesser General Public License (LGPL).

   <http://canopennode.sourceforge.net>

   (For more information see <CO_SDO.h>.)
*/
/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.


   Author: Janez Paternoster


   This file was automatically generated with CANopenNode Object
   Dictionary Editor. DON'T EDIT THIS FILE MANUALLY !!!!

*******************************************************************************/


#include "../Header/CO_driver.h"
#include "../Header/CO_OD.h"
#include "../Stack/CO_SDO.h"
#include "../../main.h"


/**************************************************************************************************
   DEFINITION AND INITIALIZATION OF OBJECT DICTIONARY VARIABLES
**************************************************************************************************/

// ---- Defaults for RAM variables ----------------------------------------------------------------
struct sCO_OD_RAM CO_OD_RAM = {
          CO_OD_FIRST_LAST_WORD,

/*1001*/  0x0,
/*1002*/  0x0L,
/*1003*/ {0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L},
/*1010*/ {0x3, 0x3L, 0x0L, 0x0L},
/*2100*/ {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
/*2101*/  0x63,                   // Default Device ID = 99 (0x63)
/*3101*/  0x0000,
/*3103*/  0x0000,
/*3110*/  0x0,
/*3111*/  0x0L,
/*3210*/ {0},
/*3424*/ {0},
/*3425*/ {0},
/*3520*/ {0x7, 0x0, 0x0, 0xA9, 0x0, 0x0L, 0x0L, 0x0L},  // Default identification-number (0x3520,3)
                                                        // 0xA9 = Ultrasonic, 0xB9 = Infrared.
///*3730*/  0x0,
/*3731*/  0x1,  // CAN Terminator
///*4004*/ {'0'},
///*4500*/ {0x9, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L},
///*4510*/ {0x5, 0x0L, 0x0L, 0x0L, 0, 0x0},

/* 5500 */  0x00,
/* 5501 */  0x00,
/* 5502 */ {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
/* 5503 */ {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
/* 55FD*/   0x00,
/* 55FE*/   0xD4,
/* 55FF*/   0x00,
            
///*5600*/  0,
///*5700*/ {0, 0, 0, 0, 0},
///*5701*/ {0L, 0L, 0L, 0L, 0L},
///*5710*/  0x0,
///*571A*/ {0x0, 0x0, 0x0, 0x0},
///*5720*/  0x0L,
///*5721*/  0x0L,
///*5723*/  0x0L,
///*5724*/ {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
///*5729*/  0x0,
///*5730*/  0x0,
///*5739*/  0x0,
///*573C*/  0x0,
///*573D*/  0x0,
///*573E*/ {0x1D, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
///*573F*/  0x0,
///*5741*/  0x0,
///*5742*/ {0x7, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0},
///*5800*/  0x0L,
///*5810*/ {0x7, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
///*59A0*/ {0x4, 0x0L, 0x0L, 0x0L, 0x0L},
///*59A1*/  0x0,
///*59B0*/  0x0L,
///*59B1*/  0x0L,
///*59B2*/  0x0L,                   //                  Prefix: 0xFD = milli
///*5A00*/  0x0,                    //                  |  SI Numerator: 0x01 = metre
/*5A01*/  0x0,                    //                  |  |
/*6131*/ {0xFD010000, 0xFD010000},  // Physical Units: FD 01 00 00
/*6132*/ {0x01, 0x01},              // Decimal Digits: 1.  Ex: 123.0 millimeters = 1230 decimal
/*6401*/ {0x3, 0, 0, 0x0},
/*6424*/ {0x3, 0, 0, 0},
/*6425*/ {0x3, 0, 0, 0},
/*642F*/ {0x3, 0, 0, 0},
/*6431*/ {0x3, 0, 0, 0},
///*7100*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
///*7120*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
///*7122*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*7130*/ {0x2, 0, 0},
///*7140*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
///*7142*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/*8130*/ {0x2, 0, 0},
/*9100*/ {0},                       // Field Value
/*9120*/ {0x80000000},              // Field Value Min
/*9121*/ {0x80000000, 0x80000000},  // *** Process Value Min
/*9122*/ {0x7FFFFFFF},              // *** Field Value Max
/*9123*/ {0x7FFFFFFF, 0x7FFFFFFF},  // Process Value Max
/*9124*/ {0, 0},                    // *** Process Value Offset
/*9130*/ {0, 0},                    // Process Value (edge values)
/*9140*/ {0},                       // Field Value Limit Min
/*9142*/ {100},                     // Field Value Limit Max

          CO_OD_FIRST_LAST_WORD
};


// ---- Definition and Defaults for variables read from external memory (EEPROM or other) ---------
struct sCO_OD_EEPROM    CO_OD_EEPROM =
{
            CO_OD_FIRST_LAST_WORD,  // 0x55

/*1008*/    {DEFAULT_SENSOR_STRING},
/*2200*/    0x00,                   // Default Sensor Type = 0 (Ultrasonic)
/*2201*/    0x00,                   // Default Sensor Gap = 0  (1.5" gap)
/*2202*/    0x01,                   // Default Sensor Max Beam = 1 (point source)
/*5504*/                            // Detector Calibration Power Levels
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
/*5505*/                            // Detector Calibration Receive Values
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
/*5506*/    0x00,                   //  Default calibration opacity value

            CO_OD_FIRST_LAST_WORD,  // 0x55
};


// ---- Definition for ROM variables (constant variables, stored in flash) ------------------------
const struct sCO_OD_ROM CO_OD_ROM = {
          CO_OD_FIRST_LAST_WORD,

/*1000*/  0x20194L,
/*1005*/  0x80L,
/*1006*/  0x1388L,
/*100A*/ {FW_VERSION_STRING},   // NOTE: 0x100A to 0x1014 is exactly 5 characters
/*1014*/  0x80L,
/*1015*/  0x0,
/*1016*/ {0x0L},
/*1017*/  0x64,
/*1018*/ {0x4, 0x10BL, 0x2L, 0x0L, 0x0L},
/*1200*/{{0x2, 0x600L, 0x580L}},
/*1400*/{{0x2, 0x200L, 0xFF}},
/*1600*/{ { 0x1,                // RPDO Mapping Parameter, number of objects = 1
            0x31010010L,        // RPDO Mapping 1, Object 3101, Subindex 1, 16 bits (Control Word)
            0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L, 0x0L } },
/*1800*/{ {0x3, 0x180L, 0x1, 0x0},  // Transmit TPDO1 every 5ms
          {0xF, 0x500L, 0x2, 0x0}   // Transmit MUX PDO every 10ms
            },
/*1A00*/{ { 0x3,                // TPDO Mapping Parameter, number of objects = 3
            0x31030010L,        // TPDO Mapping 1, Object 3103, Subindex 0, 16 bits (Status Word)
            0x71300118L,        // TPDO Mapping 2, Object 7130, Subindex 1, 24 bits (Edge 1 value)
            0x71300218L,        // TPDO Mapping 3, Object 7130, Subindex 2, 24 bits (Edge 2 value)
            0x0L, 0x0L, 0x0L, 0x0L, 0x0L } },
/*1F80*/  0x0L,
/*1FA0*/ {  0x14,               // Scannerlist, number of objects = 20

    // NOTE: "Field value" is the non-scaled, raw reading from the sensor.
    //                   (which is converted to the)
    //       "Process value" is scaled and displayed value with units (kg, centigrade, mm, etc)

            0x61310120L,        // Object 6131, Subindex 1, 32 bits.    Physical units
            0x61310220L,        // Object 6131, Subindex 2, 32 bits.    Physical units
            0x61320108L,        // Object 6132, Subindex 1,  8 bits.    Decimal digits
            0x61320208L,        // Object 6132, Subindex 2,  8 bits.    Decimal digits

            0x91210120L,        // Object 9121, Subindex 1, 32 bits.    *** Process Value MIN, edge 1
            0x91210220L,        // Object 9121, Subindex 2, 32 bits.    *** Process Value MIN, edge 2
            0x91230120L,        // Object 9123, Subindex 1, 32 bits.    Process Value MAX, edge 1
            0x91230220L,        // Object 9123, Subindex 2, 32 bits.    Process Value MAX, edge 2

            0x91240120L,        // Object 9124, Subindex 1, 32 bits.    *** Process Value Offset, edge 1
            0x91240220L,        // Object 9124, Subindex 2, 32 bits.    *** Process Value Offset, edge 1
            0x91220120L,        // Object 9122, Subindex 1, 32 bits.    *** Field Value MAX
            0x91200120L,        // Object 9120, Subindex 1, 32 bits.    Field Value MIN

            0x35200310L,        // Object 3520, Subindex 3, 16 bits.    Sensor identification-number
            0x91000120L,        // Object 9100, Subindex 1, 32 bits.    Field Value[0].
            0x91000120L,        // Object 9100, Subindex 2, 32 bits.    Field Value[1].
            0x91400120L,        // Object 9140, Subindex 1, 32 bits.    Field value Limit Min[0]
            
            0x91420120L,        // Object 9142, Subindex 1, 32 bits.    Field value Limit Max[0]
            0x91400120L,        // Object 9140, Subindex 2, 32 bits.    Field value Limit Min[1]
            0x91420120L,        // Object 9142, Subindex 2, 32 bits.    Field value Limit Max[1]
            0x37310008L,        // Object 3731, Subindex 0,  8 bits.    CAN bus terminator status

                0x00000000L,//0x91300120L,        // Object 9130, Subindex 1, 32 bits.    Process Value, edge 1
                0x00000000L,//0x91300220L,        // Object 9130, Subindex 2, 32 bits.    Process Value, edge 2
                0x00000000L,//0x31030020L,        // Object 3103, Subindex 0, 32 bits.
                0x00000000L,

                0x00000000L,
                0x00000000L,
                0x00000000L,
                0x00000000L,

                0x00000000L,
                0x00000000L,
                0x00000000L,
                0x00000000L
       },

/*2102*/  0x1F4, // Baud rate - 500k

///*4520*/ {0x3, 0x0L, {'0'}, {'0'}},

          CO_OD_FIRST_LAST_WORD
};


/*******************************************************************************
   STRUCTURES FOR RECORD TYPE OBJECTS
*******************************************************************************/
/*0x1010*/ const CO_ODrecord_t ODrecord1010[4] = {
           {(const void*)&CO_OD_RAM.storeFlashParameters.numberOfEntries, 0x06,  1},
           {(const void*)&CO_OD_RAM.storeFlashParameters.saveAllParameters, 0x8A,  4},
           {(const void*)&CO_OD_RAM.storeFlashParameters.saveCommunicationParameters, 0x8A,  4},
           {(const void*)&CO_OD_RAM.storeFlashParameters.saveApplicationParameters, 0x8A,  4}};
/*0x1018*/ const CO_ODrecord_t ODrecord1018[5] = {
           {(const void*)&CO_OD_ROM.identity.maxSubIndex, 0x05,  1},
           {(const void*)&CO_OD_ROM.identity.vendorID, 0x85,  4},
           {(const void*)&CO_OD_ROM.identity.productCode, 0x85,  4},
           {(const void*)&CO_OD_ROM.identity.revisionNumber, 0x85,  4},
           {(const void*)&CO_OD_ROM.identity.serialNumber, 0x85,  4}};
/*0x1200*/ const CO_ODrecord_t ODrecord1200[3] = {
           {(const void*)&CO_OD_ROM.SDOServerParameter[0].maxSubIndex, 0x05,  1},
           {(const void*)&CO_OD_ROM.SDOServerParameter[0].COB_IDClientToServer, 0x85,  4},
           {(const void*)&CO_OD_ROM.SDOServerParameter[0].COB_IDServerToClient, 0x85,  4}};
/*0x1400*/ const CO_ODrecord_t ODrecord1400[3] = {
           {(const void*)&CO_OD_ROM.RPDOCommunicationParameter[0].maxSubIndex, 0x05,  1},
           {(const void*)&CO_OD_ROM.RPDOCommunicationParameter[0].COB_IDUsedByRPDO, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOCommunicationParameter[0].transmissionType, 0x0D,  1}};
/*0x1600*/ const CO_ODrecord_t ODrecord1600[9] = {
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].numberOfMappedObjects, 0x0D,  1},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject1, 0x9D,  4},   // controlWord
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject2, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject3, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject4, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject5, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject6, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject7, 0x8D,  4},
           {(const void*)&CO_OD_ROM.RPDOMappingParameter[0].mappedObject8, 0x8D,  4}};
/*0x1800*/ const CO_ODrecord_t ODrecord1800[4] = {
           {(const void*)&CO_OD_ROM.TPDOCommunicationParameter[0].maxSubIndex, 0x05,  1},
           {(const void*)&CO_OD_ROM.TPDOCommunicationParameter[0].COB_IDUsedByTPDO, 0x8D,  4},
           {(const void*)&CO_OD_ROM.TPDOCommunicationParameter[0].transmissionType, 0x0D,  1},
           {(const void*)&CO_OD_ROM.TPDOCommunicationParameter[0].inhibitTime, 0x8D,  2}};
/*0x1A00*/ const CO_ODrecord_t ODrecord1A00[9] = {
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].numberOfMappedObjects, 0x0D,  1},
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject1, 0xAD,  2},   // statusWord
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject2, 0xAD,  3},   // edge1
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject3, 0xAD,  3},   // edge2
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject4, 0x8D,  4},
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject5, 0x8D,  4},
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject6, 0x8D,  4},
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject7, 0x8D,  4},
           {(const void*)&CO_OD_ROM.TPDOMappingParameter[0].mappedObject8, 0x8D,  4}};
/*0x1FA0*/ const CO_ODrecord_t ODrecord1FA0[33] = {
           {(const void*)&CO_OD_ROM.scannerlistMapping.numberOfMappedObjects, 0x10,  1},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject1,  0x9D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject2,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject3,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject4,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject5,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject6,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject7,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject8,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject9,  0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject10, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject11, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject12, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject13, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject14, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject15, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject16, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject17, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject18, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject19, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject20, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject21, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject22, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject23, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject24, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject25, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject26, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject27, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject28, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject29, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject30, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject31, 0x8D,  4},
           {(const void*)&CO_OD_ROM.scannerlistMapping.mappedObject32, 0x8D,  4}
           };
/*0x3520*/ const CO_ODrecord_t ODrecord3520[8] = {
           {(const void*)&CO_OD_RAM.IDENT.maxSubIndex, 0x05,  1},
           {(const void*)&CO_OD_RAM.IDENT.identHw, 0x85,  2},
           {(const void*)&CO_OD_RAM.IDENT.identHwValue, 0x85,  2},
           {(const void*)&CO_OD_RAM.IDENT.identDevice, 0x85,  2},
           {(const void*)&CO_OD_RAM.IDENT.identDeviceValue, 0x85,  2},
           {(const void*)&CO_OD_RAM.IDENT.softwareVersionLow, 0x85,  4},
           {(const void*)&CO_OD_RAM.IDENT.softwareVersionHigh, 0x85,  4},
           {(const void*)&CO_OD_RAM.IDENT.blSoftwareVersion, 0x85,  4}};
/*0x4500*//* const CO_ODrecord_t ODrecord4500[10] = {
           {(const void*)&CO_OD_RAM.info.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.info.firstTestDate, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.serialNumberBoard, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.articleNumberBoard, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.distributorBoard, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.serialNumberDevice, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.articleNumberDevice, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.distributorDevice, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.testDate, 0x8E,  4},
           {(const void*)&CO_OD_RAM.info.testTime, 0x8E,  4}};*/
/*0x4510*//* const CO_ODrecord_t ODrecord4510[6] = {
           {(const void*)&CO_OD_RAM.mde.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.mde.minutes, 0x86,  4},
           {(const void*)&CO_OD_RAM.mde.init, 0x86,  4},
           {(const void*)&CO_OD_RAM.mde.powerUpDeviceCnt, 0x86,  4},
           {(const void*)&CO_OD_RAM.mde.processorMAXTemperature, 0x86,  2},
           {(const void*)&CO_OD_RAM.mde.maxOperatingVoltage, 0x86,  2}};*/
/*0x4520*//* const CO_ODrecord_t ODrecord4520[4] = {
           {(const void*)&CO_OD_ROM.dev.maxSubIndex, 0x05,  1},
           {(const void*)&CO_OD_ROM.dev.compilerVersion, 0x8D,  4},
           {(const void*)&CO_OD_ROM.dev.compilingDate[0], 0x0D,  1},
           {(const void*)&CO_OD_ROM.dev.compilingTime[0], 0x0D,  1}};*/
/*0x573E*//* const CO_ODrecord_t ODrecord573E[30] = {
           {(const void*)&CO_OD_RAM.variousTestResults.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.variousTestResults.biColorLedTestResult, 0x06,  1},
           {(const void*)&CO_OD_RAM.variousTestResults.framCheckTestResult, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.LEDDriverTestResult, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.hallSensorInput, 0x06,  1},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue1, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue2, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue3, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue4, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue5, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue6, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue7, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue8, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue9, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue10, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue11, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue12, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue13, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue14, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue15, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue16, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue17, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue18, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue19, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue20, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue21, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue22, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.testDACValue23, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.framCheckTest, 0x8E,  2},
           {(const void*)&CO_OD_RAM.variousTestResults.sensorAnalogOutSignal, 0x86,  2}};*/
/*0x5742*//* const CO_ODrecord_t ODrecord5742[8] = {
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.lowerLimitPhotodiodeCurveThreshold, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.upperLimitPhotodiodeCurveThreshold, 0x86,  2},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.lowerLimitPhotodiodeCurveRefMinThreshold, 0x8E,  2},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.upperLimitPhotodiodeCurveRefMaxThreshold, 0x8E,  2},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.ledDriverDivider, 0x8E,  2},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.ledCurrentHighLowSwitch, 0x06,  1},
           {(const void*)&CO_OD_RAM.variousPhotodiodeAdjustments.scalingCorrectionFactor, 0x8E,  4}};*/
/*0x5810*//* const CO_ODrecord_t ODrecord5810[8] = {
           {(const void*)&CO_OD_RAM.diag.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.diag.watchdogOccurredCounter, 0x86,  2},
           {(const void*)&CO_OD_RAM.diag.powerOnOccurredCounter, 0x86,  2},
           {(const void*)&CO_OD_RAM.diag.checksumErrors, 0x86,  2},
           {(const void*)&CO_OD_RAM.diag.busErrors, 0x86,  2},
           {(const void*)&CO_OD_RAM.diag.busErrors1, 0x86,  2},
           {(const void*)&CO_OD_RAM.diag.busErrors2, 0x86,  2},
           {(const void*)&CO_OD_RAM.diag.nmiCounter, 0x86,  2}};*/
/*0x59A0*//* const CO_ODrecord_t ODrecord59A0[5] = {
           {(const void*)&CO_OD_RAM.reset.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.reset.resetAll, 0x8E,  4},
           {(const void*)&CO_OD_RAM.reset.resetMde, 0x8E,  4},
           {(const void*)&CO_OD_RAM.reset.resetDiag, 0x8E,  4},
           {(const void*)&CO_OD_RAM.reset.resetInfo, 0x8E,  4}};*/
/*0x6401*/ const CO_ODrecord_t ODrecord6401[4] = {
           {(const void*)&CO_OD_RAM.readAnalogInput.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.readAnalogInput.read_Analogue_Input_1, 0xB6,  2},
           {(const void*)&CO_OD_RAM.readAnalogInput.processorTemperatureResult, 0xB6,  2},
           {(const void*)&CO_OD_RAM.readAnalogInput.actOperatingVoltage, 0xB6,  2}};
/*0x6424*/ const CO_ODrecord_t ODrecord6424[4] = {
           {(const void*)&CO_OD_RAM.analogInputUpperLimit.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.analogInputUpperLimit.upperLimitSensor, 0xBE,  2},
           {(const void*)&CO_OD_RAM.analogInputUpperLimit.upperLimitTemperature, 0xBE,  2},
           {(const void*)&CO_OD_RAM.analogInputUpperLimit.upperLimitVoltage, 0xBE,  2}};
/*0x6425*/ const CO_ODrecord_t ODrecord6425[4] = {
           {(const void*)&CO_OD_RAM.analogInputLowerLimit.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.analogInputLowerLimit.lowerLimitSensor, 0xBE,  2},
           {(const void*)&CO_OD_RAM.analogInputLowerLimit.lowerLimitTemperature, 0xBE,  2},
           {(const void*)&CO_OD_RAM.analogInputLowerLimit.lowerLimitVoltage, 0xBE,  2}};
/*0x642F*/ const CO_ODrecord_t ODrecord642F[4] = {
           {(const void*)&CO_OD_RAM.analogInputScaling.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.analogInputScaling.inputScalingSensor, 0xBE,  4},
           {(const void*)&CO_OD_RAM.analogInputScaling.inputScalingTemperature, 0xBE,  4},
           {(const void*)&CO_OD_RAM.analogInputScaling.inputScalingVoltage, 0xBE,  4}};
/*0x6431*/ const CO_ODrecord_t ODrecord6431[4] = {
           {(const void*)&CO_OD_RAM.analogInputOffset.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.analogInputOffset.inputOffsetSensor, 0xBE,  2},
           {(const void*)&CO_OD_RAM.analogInputOffset.inputOffsetTemperature, 0xBE,  2},
           {(const void*)&CO_OD_RAM.analogInputOffset.inputOffsetVoltage, 0xBE,  2}};
/*0x7130*/ const CO_ODrecord_t ODrecord7130[3] = {
           {(const void*)&CO_OD_RAM.edgeValue.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.edgeValue.edgeValue1, 0xB6,  3},
           {(const void*)&CO_OD_RAM.edgeValue.edgeValue2, 0xB6,  3}};
/*0x8130*/ const CO_ODrecord_t ODrecord8130[3] = {
           {(const void*)&CO_OD_RAM.edgeInMicrons.maxSubIndex, 0x06,  1},
           {(const void*)&CO_OD_RAM.edgeInMicrons.firstEdgeInMicrons, 0xBE,  3},
           {(const void*)&CO_OD_RAM.edgeInMicrons.secondEdgeInMicrons, 0xBE,  3}};



/*******************************************************************************
   OBJECT DICTIONARY
 
 Index, MaxSubIndex, Attribute, Length, pData

 
 Attribute flags for sCO_OD_object
      CO_ODA_MEM_ROM          - (0x01) - Variable is located in ROM memory.
      CO_ODA_MEM_RAM          - (0x02) - Variable is located in RAM memory.
      CO_ODA_MEM_EEPROM       - (0x03) - Variable is located in EEPROM memory.
      CO_ODA_READABLE         - (0x04) - SDO server may read from the variable.
      CO_ODA_WRITEABLE        - (0x08) - SDO server may write to the variable.
      CO_ODA_RPDO_MAPABLE     - (0x10) - Variable is mappable for RPDO.
      CO_ODA_TPDO_MAPABLE     - (0x20) - Variable is mappable for TPDO.
      CO_ODA_TPDO_DETECT_COS  - (0x40) - If variable is mapped to any PDO, then
                                         PDO is automatically send, if variable
                                         changes its value.
      CO_ODA_MB_VALUE         - (0x80) - True when variable is a multibyte value.

*******************************************************************************/
const sCO_OD_object CO_OD[CO_OD_NoOfElements] = {
{0x1000, 0x00, 0x85,  4, (const void*)&CO_OD_ROM.deviceType                       },
{0x1001, 0x00, 0x36,  1, (const void*)&CO_OD_RAM.errorRegister                    },
{0x1002, 0x00, 0xB6,  4, (const void*)&CO_OD_RAM.manufacturerStatusRegister       },
{0x1003, 0x08, 0x8E,  4, (const void*)&CO_OD_RAM.preDefinedErrorField[0]          },
{0x1005, 0x00, 0x8D,  4, (const void*)&CO_OD_ROM.COB_ID_SYNCMessage               },
{0x1006, 0x00, 0x8D,  4, (const void*)&CO_OD_ROM.communicationCyclePeriod         },
{0x1008, 0x00, 0x8F, 32, (const void*)&CO_OD_EEPROM.manufacturerDeviceName[0]     },
{0x100A, 0x00, 0x85,  8, (const void*)&CO_OD_ROM.manufacturerSoftwareVersion[0]   },
{0x1010, 0x03, 0x00,  0, (const void*)&ODrecord1010                               },
{0x1014, 0x00, 0x85,  4, (const void*)&CO_OD_ROM.emergencyCOB_ID                  },
{0x1015, 0x00, 0x8D,  2, (const void*)&CO_OD_ROM.inhibitTimeEmergency             },
{0x1016, 0x01, 0x8D,  4, (const void*)&CO_OD_ROM.consumerHeartbeatTime[0]         },
{0x1017, 0x00, 0x8D,  2, (const void*)&CO_OD_ROM.producerHeartbeatTime            },
{0x1018, 0x04, 0x00,  0, (const void*)&ODrecord1018                               },
{0x1200, 0x02, 0x00,  0, (const void*)&ODrecord1200                               },
{0x1400, 0x02, 0x00,  0, (const void*)&ODrecord1400                               },
{0x1600, 0x01, 0x00,  0, (const void*)&ODrecord1600                               },
{0x1800, 0x03, 0x00,  0, (const void*)&ODrecord1800                               },
{0x1A00, 0x03, 0x00,  0, (const void*)&ODrecord1A00                               },
{0x1F80, 0x00, 0x8D,  4, (const void*)&CO_OD_ROM.NMTStartup                       },
{0x1FA0, 0x20, 0x00,  0, (const void*)&ODrecord1FA0                               },
{0x2100, 0x00, 0x36, 10, (const void*)&CO_OD_RAM.errorStatusBits[0]               },
{0x2101, 0x00, 0x0D,  1, (const void*)&CO_OD_RAM.CANNodeID                        },
{0x2102, 0x00, 0x8D,  2, (const void*)&CO_OD_ROM.CANBitRate                       },

{0x2200, 0x00, 0x0F,  1, (const void*)&CO_OD_EEPROM.sensorType                    },
{0x2201, 0x00, 0x0F,  1, (const void*)&CO_OD_EEPROM.sensorGap                     },
{0x2202, 0x00, 0x0F,  1, (const void*)&CO_OD_EEPROM.sensorMaxBeam                 },

{0x3101, 0x00, 0x9A,  2, (const void*)&CO_OD_RAM.controlWord                      },
{0x3103, 0x00, 0xA6,  2, (const void*)&CO_OD_RAM.statusWord                       },
{0x3110, 0x00, 0x9A,  2, (const void*)&CO_OD_RAM.materialSetupControlWord         },
{0x3111, 0x00, 0x86,  4, (const void*)&CO_OD_RAM.materialSetupStatusWord          },
{0x3210, 0x01, 0xB6,  2, (const void*)&CO_OD_RAM.readAnalogInputAbs[0]            },
{0x3424, 0x01, 0xBE,  2, (const void*)&CO_OD_RAM.analogInputUpperLimitMax[0]      },
{0x3425, 0x01, 0xBE,  2, (const void*)&CO_OD_RAM.analogInputLowerLimitMax[0]      },
{0x3520, 0x07, 0x00,  0, (const void*)&ODrecord3520                               },
//{0x3730, 0x00, 0x86,  2, (const void*)&CO_OD_RAM.processorTemperature             },
{0x3731, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.setCanBusTerminator              },
//{0x4004, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.BSTSoftDownload[0]               },
//{0x4500, 0x09, 0x00,  0, (const void*)&ODrecord4500                               },
//{0x4510, 0x05, 0x00,  0, (const void*)&ODrecord4510                               },
//{0x4520, 0x03, 0x00,  0, (const void*)&ODrecord4520                               },


{0x5500, 0x00, 0x06, 1, (const void*)&CO_OD_RAM.currentBeam                     },
{0x5501, 0x00, 0x06, 1, (const void*)&CO_OD_RAM.currentSignal                   },
{0x5502, 0x7F, 0x06, 1, (const void*)&CO_OD_RAM.correctedSignals[0]             },
{0x5503, 0x7F, 0x06, 1, (const void*)&CO_OD_RAM.rawSignals[0]                   },
{0x5504, 0x7F, 0x06, 1, (const void*)&CO_OD_EEPROM.calibrationPowerLevels[0]    },
{0x5505, 0x7F, 0x06, 1, (const void*)&CO_OD_EEPROM.calibrationReceiveValues[0]  },
{0x5506, 0x7F, 0x06, 1, (const void*)&CO_OD_EEPROM.calibrationOpacity           },
{0x55FD, 0x00, 0x0D, 1, (const void*)&CO_OD_RAM.testPowerLevel                  },
{0x55FE, 0x00, 0x0D, 1, (const void*)&CO_OD_RAM.testFrequency                   },
{0x55FF, 0x00, 0x0D, 1, (const void*)&CO_OD_RAM.testMode                        },


//{0x5600, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.intLoad                          },
//{0x5700, 0x05, 0x8E,  2, (const void*)&CO_OD_RAM.debugVarSw[0]                    },
//{0x5701, 0x05, 0x8E,  4, (const void*)&CO_OD_RAM.debugVarSdw[0]                   },
//{0x5710, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.debugLeds                        },
//{0x571A, 0x04, 0x8E,  2, (const void*)&CO_OD_RAM.individualDigOrAnaModeValues[0]  },
//{0x5720, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.watchdogTest                     },
//{0x5721, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.bootloaderReset                  },
//{0x5723, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.resetFramCheck                   },
//{0x5724, 0x10, 0x86,  2, (const void*)&CO_OD_RAM.individualFilteredValues[0]      },
//{0x5729, 0x00, 0x8E,  2, (const void*)&CO_OD_RAM.dacTestActivate                  },
//{0x5730, 0x00, 0x8E,  2, (const void*)&CO_OD_RAM.manufacturerMasterSetupControlWord},
//{0x5739, 0x00, 0x8E,  2, (const void*)&CO_OD_RAM.materialSetup                    },
//{0x573C, 0x00, 0x8E,  2, (const void*)&CO_OD_RAM.enableMaunfacturerMasterSetup    },
//{0x573D, 0x00, 0x8E,  2, (const void*)&CO_OD_RAM.enableReferenceTableSetup        },
//{0x573E, 0x1D, 0x00,  0, (const void*)&ODrecord573E                               },
//{0x573F, 0x00, 0x8E,  2, (const void*)&CO_OD_RAM.referenceTableSetup              },
//{0x5741, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.setMaterialSetupValsToMasterSetupVals},
//{0x5742, 0x07, 0x00,  0, (const void*)&ODrecord5742                               },
//{0x5800, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.uploadObject                     },
//{0x5810, 0x07, 0x00,  0, (const void*)&ODrecord5810                               },
//{0x59A0, 0x04, 0x00,  0, (const void*)&ODrecord59A0                               },
//{0x59A1, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.readOutProtect                   },
//{0x59B0, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.uniqueId0Cpu                     },
//{0x59B1, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.uniqueId1Cpu                     },
//{0x59B2, 0x00, 0x8E,  4, (const void*)&CO_OD_RAM.uniqueId2Cpu                     },
//{0x5A00, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.dynamicNode                      },
{0x5A01, 0x00, 0x0E,  1, (const void*)&CO_OD_RAM.lssLock                          },
{0x6131, 0x02, 0xFE,  4, (const void*)&CO_OD_RAM.physicalUnits[0]                 },
{0x6132, 0x02, 0x7E,  1, (const void*)&CO_OD_RAM.decimalDigits[0]                 },
{0x6401, 0x03, 0x00,  0, (const void*)&ODrecord6401                               },
{0x6424, 0x03, 0x00,  0, (const void*)&ODrecord6424                               },
{0x6425, 0x03, 0x00,  0, (const void*)&ODrecord6425                               },
{0x642F, 0x03, 0x00,  0, (const void*)&ODrecord642F                               },
{0x6431, 0x03, 0x00,  0, (const void*)&ODrecord6431                               },
//{0x7100, 0x10, 0xB6,  2, (const void*)&CO_OD_RAM.normedADC1MeanValue[0]           },
//{0x7120, 0x10, 0xBE,  2, (const void*)&CO_OD_RAM.photodiodeFilterMaterialMinValue[0]},
//{0x7122, 0x10, 0xB6,  2, (const void*)&CO_OD_RAM.photodiodeFilterMaterialMaxValue[0]},
{0x7130, 0x02, 0x00,  0, (const void*)&ODrecord7130                               },
//{0x7140, 0x10, 0xBE,  2, (const void*)&CO_OD_RAM.photodiodeFilterMasterMinValue[0]},
//{0x7142, 0x10, 0xBE,  2, (const void*)&CO_OD_RAM.photodiodeFilterMasterMaxValue[0]},
{0x8130, 0x02, 0x00,  0, (const void*)&ODrecord8130                               },
{0x9100, 0x01, 0xB6,  4, (const void*)&CO_OD_RAM.fieldValue[0]                    },
{0x9120, 0x01, 0xB6,  4, (const void*)&CO_OD_RAM.fieldValueMIN[0]                 },
{0x9121, 0x02, 0xBE,  4, (const void*)&CO_OD_RAM.processValueMIN[0]               },
{0x9122, 0x01, 0xBE,  4, (const void*)&CO_OD_RAM.fieldValueMAX[0]                 },
{0x9123, 0x02, 0xBE,  4, (const void*)&CO_OD_RAM.processValueMAX[0]               },
{0x9124, 0x02, 0xBE,  4, (const void*)&CO_OD_RAM.processValueOFFSET[0]            },
{0x9130, 0x02, 0xBE,  4, (const void*)&CO_OD_RAM.processValue[0]                  },
{0x9140, 0x01, 0xB6,  4, (const void*)&CO_OD_RAM.fieldValueLimitMin[0]            },
{0x9142, 0x01, 0xB6,  4, (const void*)&CO_OD_RAM.fieldValueLimitMax[0]            },
};
