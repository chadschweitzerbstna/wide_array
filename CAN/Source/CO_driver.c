/***************************************************************************************************

   File - CO_driver.c

***************************************************************************************************/

#include "../../can.h"
#include "../Header/CO_driver.h"
#include "../Stack/CO_Emergency.h"

#include <GenericTypeDefs.h>
#include <xc.h>
#include <proc/p32mz1024eff100.h>
#include <peripheral/CAN.h>
#include <../../../v1.33/pic32-libs/peripheral/can/source/CANTypes.h>





// -------------------------------------------------------------------------------------------------
//  Initialize all of the CANmodule variables and buffers
//  for transmitting and receiving CAN messages.
//
void CO_CANmodule_init(void)
{
    unsigned char i, j;

    // Initialize RX buffer
    for (i=0; i<CAN_MSG_BUFFER_SIZE; i++)
    {
        CO_CANmodule.CO_RX_buffer.msg[i].COB_ID = 0;
        CO_CANmodule.CO_RX_buffer.msg[i].dataLen = 0;
        for (j=0; j<8; j++)
        {
            CO_CANmodule.CO_RX_buffer.msg[i].data[j] = 0;
        }
    }
    CO_CANmodule.CO_RX_buffer.head = 0;
    CO_CANmodule.CO_RX_buffer.tail = 0;
    CO_CANmodule.CO_RX_buffer.overflow = 0;
    CO_CANmodule.CANrxCount = 0;
    CO_CANmodule.RXbusy = 0;

    
    // Initialize TX buffer
    for (i=0; i<CAN_MSG_BUFFER_SIZE; i++)
    {
        CO_CANmodule.CO_TX_buffer.msg[i].COB_ID = 0;
        CO_CANmodule.CO_TX_buffer.msg[i].dataLen = 0;
        for (j=0; j<8; j++)
        {
            CO_CANmodule.CO_TX_buffer.msg[i].data[j] = 0;
        }
    }
    CO_CANmodule.CO_TX_buffer.head = 0;
    CO_CANmodule.CO_TX_buffer.tail = 0;
    CO_CANmodule.CO_TX_buffer.overflow = 0;
    CO_CANmodule.CANtxCount = 0;
    CO_CANmodule.TXbusy = 0;
    CO_CANmodule.firstMessage = 1;
}


// -------------------------------------------------------------------------------------------------
//  Send a message to the CAN hardware
//
signed short int CO_CANsend(CO_CANmsg_t msg)
{
    unsigned int msgId = msg.COB_ID;
    unsigned char msgDataLen = msg.dataLen;
    unsigned char *msgData = msg.data;
    CO_msg_buf_t *txBuffer = &CO_CANmodule.CO_TX_buffer;
    CANTxMessageBuffer *txMessage;      // pointer to CAN TX message buffer
    unsigned char i;

    // Is the TX buffer full?
    if (txBuffer->overflow)
    {
        //  Don't set an error if the bootup message has not been sent yet.
        if(!CO_CANmodule.firstMessage)
            CO_errorReport(&EM, ERROR_CAN_TX_OVERFLOW, 0);
        
        return CO_ERROR_TX_OVERFLOW;
    }
   
    // Disable interrupts
    __builtin_disable_interrupts();

    // If the CAN TX buffer is empty...
    //if (C1FIFOINT0bits.TXNFULLIF)   // Is the TX FIFO not full?
    
    //if ( (C1FIFOCON0bits.TXERR == 0) && (C1FIFOINT0bits.TXNFULLIF == 1) )
    //if ( ((C1FIFO & 0x08) == 0) && (CO_CANmodule.CANtxCount == 0) )
    if ( ((C1FIFOCON0 & 0x08) == 0) && (CO_CANmodule.CANtxCount == 0) )
    {
        // Get the address of the next avaialible spot in the CAN FIFO message buffer.
        txMessage = (CANTxMessageBuffer *) (PA_TO_KVA1(C1FIFOUA0));

        // Clear the FIFO at the location returned to us
        txMessage->messageWord[0] = 0;
        txMessage->messageWord[1] = 0;
        txMessage->messageWord[2] = 0;
        txMessage->messageWord[3] = 0;

        // Copy the message to be sent.
        // First validate the message length.
        if (msgDataLen > 8)
            msgDataLen = 8;

        // Copy the message data
        for (i=0; i<msgDataLen; i++)
            txMessage->data[i] = *(msgData+i);

        // Copy the message ID
        txMessage->msgSID.SID = msgId;
        txMessage->msgEID.IDE = 0;

        // Copy the data length
        txMessage->msgEID.DLC = msgDataLen;

        // Send the message
        C1FIFOCON0SET = 0x2000;     // same as C1FIFOCON0bits.UINC = 1;
        C1FIFOCON0SET = 0x0008;     // same as C1FIFOCON0bits.TXREQ = 1;
    }
    else    // Message still in FIFO.  Buffer the new message and send later by interrupt.
    {
        // Copy message to software buffer.
        unsigned char head = txBuffer->head;
        txBuffer->msg[head].COB_ID = msgId;
        txBuffer->msg[head].dataLen = msgDataLen;
        for (i=0; i<msgDataLen; i++)
            txBuffer->msg[head].data[i] = *(msgData+i);
        // Clear any remaining bytes
        while (i<8)
        {
            txBuffer->msg[head].data[i] = *(msgData+i);
            i++;
        }
        
        // Advance the TX buffer head and check for wrap-around.
        txBuffer->head++;
        if (txBuffer->head >= CAN_MSG_BUFFER_SIZE)
            txBuffer->head = 0;

        // Check for TX buffer overflow.
        if (txBuffer->head == txBuffer->tail)
            txBuffer->overflow = 1;
        
        // Update TX message count
        CO_CANmodule.CANtxCount++;
        
        // Enable CAN TX interrupt.
        C1INTbits.TBIE = 1;             // Enable Second Layer CAN1 TX Buffer Interrupt.
        C1FIFOINT0bits.TXEMPTYIE = 1;   // Enable Third layer FIFO_0 TX Buffer Empty interrupt.
    }

    // Enable interrupts
    __builtin_enable_interrupts();
}



// ------------------------------------------------------------------------------------------------
//
void __attribute__((vector(_CAN_1_VECTOR), interrupt(ipl5AUTO), nomips16)) CAN1_Interrupt(void)
{
    // Determine the nature of the CAN interrupt by reading the ICODE.
    unsigned char ICODE = C1VECbits.ICODE;

    if (ICODE == 0) //  FIFO0 Interrupt.
    {
        // First CAN message (boot-up) was sent successfully
        CO_CANmodule.firstMessage = 0;

        // Are there any new messages waiting to be sent?
        if (CO_CANmodule.CANtxCount > 0)
        {
            // Fix the formatting of the buffered tx message.
            CANTxMessageBuffer message;
            unsigned char i;
            for (i=0; i<4; i++)
                message.messageWord[1] = 0;
            message.msgEID.DLC = CO_CANmodule.CO_TX_buffer.msg[CO_CANmodule.CO_TX_buffer.tail].dataLen;
            message.msgSID.SID = CO_CANmodule.CO_TX_buffer.msg[CO_CANmodule.CO_TX_buffer.tail].COB_ID;
            for (i=0; i<8; i++)
                message.data[i] = CO_CANmodule.CO_TX_buffer.msg[CO_CANmodule.CO_TX_buffer.tail].data[i];

            // Get the address of the next avaialible spot in the CAN FIFO message buffer.
            UNSIGNED32 *TXmsgBuffer = PA_TO_KVA1(C1FIFOUA0);

            // Copy the message to the FIFO
            for (i=0; i<4; i++)
                *(TXmsgBuffer+i) = message.messageWord[i];

            // Send the message
            C1FIFOCON0SET = 0x2000;     // same as C1FIFOCON0bits.UINC = 1;
            C1FIFOCON0SET = 0x0008;     // same as C1FIFOCON0bits.TXREQ = 1;

            // Decrement TX message count.
            CO_CANmodule.CANtxCount--;
            // Advance TX buffer tail and check for wrap-around.
            CO_CANmodule.CO_TX_buffer.tail++;
            if (CO_CANmodule.CO_TX_buffer.tail >= CAN_MSG_BUFFER_SIZE)
                CO_CANmodule.CO_TX_buffer.tail = 0;
            // Clear overflow flag.
            CO_CANmodule.CO_TX_buffer.overflow = 0;
        }
        else
        {
            // No more messages to send - disable the TX Buffer Empty (TXEMPTYIE) interrupt.
            C1FIFOINT0bits.TXEMPTYIE = 0;
        }
        
        // Clear CAN1 third layer TX Buffer Empty Interrupt flag.
        C1FIFOINT0bits.TXEMPTYIF = 0;

        // Clear CAN1 second layer TX Buffer Interrupt flag.
        C1INTbits.TBIF = 0;
    }
    else if (ICODE == 0x43)   // Receive FIFO overflow interrupt (RBOVIF)
    {
        // TODO: This is for debug only right now.

        // Clear CAN1 second layer RX Overflow Interrupt flag.
        C1INTbits.RBOVIE = 0;
        C1INTbits.RBOVIF = 0;           // Clear the flag.
        
        // Disable the thrid layer receive FIFO overflow interrupt (RBOVIF)
        C1FIFOINT1bits.RXOVFLIE = 0;
        C1FIFOINT1bits.RXOVFLIF = 0;    // Clear the flag.
    }

    // Clear CAN1 first layer interrupt flag
    IFS1bits.CAN1IF = 0;
}


// ------------------------------------------------------------------------------------------------
//
void CO_CAN_check_for_errors(void)
{
    unsigned short int rxErrors, txErrors ,rxOverflow;
    unsigned long int errorData;

    // Get the number of RX errors (lower 8 bits in C1TREC)
    rxErrors = C1TRECbits.RERRCNT;

    // Get the number of TX errors (bits[15:8] in C1TREC)
    txErrors = C1TRECbits.TERRCNT;

    // Check if Transmitter is in Error State.
    if (C1TRECbits.TXBO)
       txErrors = 256;  // Transmitter in Error State, Bus OFF (TERRCNT ? 256)

    // Check for RX overflow.
    if (C1INTbits.RBOVIF)
        rxOverflow = 1;
    else
        rxOverflow = 0;

    // Create an error field to store specifics about whatever error occured.
    // This is manufactuter specific and not part of the standard pre-defined error codes.
    errorData = (UNSIGNED32)txErrors << 16 | rxErrors << 8 | rxOverflow;

    if(CO_CANmodule.errOld != errorData)
    {
        // This is a new error (or new errorData are available).
        CO_CANmodule.errOld = errorData;

        if(txErrors >= 256)                         // bus off
            CO_errorReport(&EM, ERROR_CAN_TX_BUS_OFF, errorData);
        else
        {                                           // not bus off
            CO_errorReset(&EM, ERROR_CAN_TX_BUS_OFF, errorData);

            if(rxErrors >= 96 || txErrors >= 96)    // bus warning
                CO_errorReport(&EM, ERROR_CAN_BUS_WARNING, errorData);

            if(rxErrors >= 128)                     // RX bus passive
                CO_errorReport(&EM, ERROR_CAN_RX_BUS_PASSIVE, errorData);
            else
                CO_errorReset(&EM, ERROR_CAN_RX_BUS_PASSIVE, errorData);

            if(txErrors >= 128)
            {                                       // TX bus passive
                if(!CO_CANmodule.firstMessage)
                    CO_errorReport(&EM, ERROR_CAN_TX_BUS_PASSIVE, errorData);
            }
            else
            {
                signed short int wasCleared;
                wasCleared = CO_errorReset(&EM, ERROR_CAN_TX_BUS_PASSIVE, errorData);
                if(wasCleared == 1)
                    CO_errorReset(&EM, ERROR_CAN_TX_OVERFLOW, errorData);
            }

            if(rxErrors < 96 && txErrors < 96)      // no error
                CO_errorReset(&EM, ERROR_CAN_BUS_WARNING, errorData);
        }

        if(rxOverflow)
            CO_errorReport(&EM, ERROR_CAN_RXB_OVERFLOW, errorData);
      
    }
}


// ------------------------------------------------------------------------------------------------
//
inline void CO_mem_copy(unsigned char *pDest, unsigned char *pSource, unsigned int numBytes)
{
    while (numBytes--)
        *(pDest++) = *(pSource++);
}


// ------------------------------------------------------------------------------------------------
//
inline void memcpySwap2(unsigned char* pDest, unsigned char* pSource)
{
    unsigned char numBytes = 2;
    while (numBytes--)
        *(pDest++) = *(pSource++);
}


// ------------------------------------------------------------------------------------------------
//
inline void memcpySwap4(unsigned char* pDest, unsigned char* pSource)
{
   unsigned char numBytes = 4;
    while (numBytes--)
        *(pDest++) = *(pSource++);
}