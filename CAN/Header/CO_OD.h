/*******************************************************************************

   File: CO_OD.h
   CANopen Object Dictionary.

   Copyright (C) 2004-2008 Janez Paternoster

   License: GNU Lesser General Public License (LGPL).

   <http://canopennode.sourceforge.net>

   (For more information see <CO_SDO.h>.)
*/
/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.


   Author: Janez Paternoster


   This file was automatically generated with CANopenNode Object
   Dictionary Editor. DON'T EDIT THIS FILE MANUALLY !!!!

*******************************************************************************/

#ifndef _CO_OD_H
#define _CO_OD_H


#include "CO_driver.h"


/*******************************************************************************
   FILE INFO:
      FileName:     Edge
      FileVersion:  3.0
      CreationTime: 16:48:11
      CreationDate: 2014-10-22
      CreatedBy:    cls
*******************************************************************************/


/*******************************************************************************
   DEVICE INFO:
      VendorName:     AccuWeb
      VendorNumber:   0
      ProductName:    Edge Detector with CAN
      ProductNumber:  0
*******************************************************************************/


/*******************************************************************************
   FEATURES
*******************************************************************************/
   #define CO_NO_SYNC               1   // Associated objects: 1005, 1006, 1007
   #define CO_NO_EMERGENCY          1   // Associated objects: 1014, 1015
   #define CO_NO_SDO_SERVER         1   // Associated objects: 1200
   #define CO_NO_SDO_CLIENT         0   
   #define CO_NO_RPDO               1   // Associated objects: 1400, 1600
   #define CO_NO_TPDO               1   // Associated objects: 1800, 1A00 (TPDO), and 1FA0 (MUX PDO)
   #define CO_NO_NMT_MASTER         0   


/*******************************************************************************
   OBJECT DICTIONARY
*******************************************************************************/
   #define CO_OD_NoOfElements       65


/*******************************************************************************
   TYPE DEFINITIONS FOR RECORDS
*******************************************************************************/
/*1010      */ typedef struct{
               UNSIGNED8      numberOfEntries;
               UNSIGNED32     saveAllParameters;
               UNSIGNED32     saveCommunicationParameters;
               UNSIGNED32     saveApplicationParameters;
               }              OD_storeFlashParameters_t;

/*1018      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     vendorID;
               UNSIGNED32     productCode;
               UNSIGNED32     revisionNumber;
               UNSIGNED32     serialNumber;
               }              OD_identity_t;
//#ifndef _CO_SDO_H
/*1200[1]   */ typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     COB_IDClientToServer;
               UNSIGNED32     COB_IDServerToClient;
               }              OD_SDOServerParameter_t;
//#endif
/*1400[4]   */ typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     COB_IDUsedByRPDO;
               UNSIGNED8      transmissionType;
               }              OD_RPDOCommunicationParameter_t;

/*1600[4]   */ typedef struct{
               UNSIGNED8      numberOfMappedObjects;
               UNSIGNED32     mappedObject1;
               UNSIGNED32     mappedObject2;
               UNSIGNED32     mappedObject3;
               UNSIGNED32     mappedObject4;
               UNSIGNED32     mappedObject5;
               UNSIGNED32     mappedObject6;
               UNSIGNED32     mappedObject7;
               UNSIGNED32     mappedObject8;
               }              OD_RPDOMappingParameter_t;

/*1800[2]   */ typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     COB_IDUsedByTPDO;
               UNSIGNED8      transmissionType;
               UNSIGNED16     inhibitTime;
               UNSIGNED8      compatibilityEntry;
               UNSIGNED16     eventTimer;
               UNSIGNED8      SYNCStartValue;
               }              OD_TPDOCommunicationParameter_t;

/*1A00[4]   */ typedef struct{
               UNSIGNED8      numberOfMappedObjects;
               UNSIGNED32     mappedObject1;
               UNSIGNED32     mappedObject2;
               UNSIGNED32     mappedObject3;
               UNSIGNED32     mappedObject4;
               UNSIGNED32     mappedObject5;
               UNSIGNED32     mappedObject6;
               UNSIGNED32     mappedObject7;
               UNSIGNED32     mappedObject8;
               }              OD_TPDOMappingParameter_t;

/*1FA0      */ typedef struct{
               UNSIGNED8      numberOfMappedObjects;
               UNSIGNED32     mappedObject1;
               UNSIGNED32     mappedObject2;
               UNSIGNED32     mappedObject3;
               UNSIGNED32     mappedObject4;
               UNSIGNED32     mappedObject5;
               UNSIGNED32     mappedObject6;
               UNSIGNED32     mappedObject7;
               UNSIGNED32     mappedObject8;
               UNSIGNED32     mappedObject9;
               UNSIGNED32     mappedObject10;
               UNSIGNED32     mappedObject11;
               UNSIGNED32     mappedObject12;
               UNSIGNED32     mappedObject13;
               UNSIGNED32     mappedObject14;
               UNSIGNED32     mappedObject15;
               UNSIGNED32     mappedObject16;
               UNSIGNED32     mappedObject17;
               UNSIGNED32     mappedObject18;
               UNSIGNED32     mappedObject19;
               UNSIGNED32     mappedObject20;
               UNSIGNED32     mappedObject21;
               UNSIGNED32     mappedObject22;
               UNSIGNED32     mappedObject23;
               UNSIGNED32     mappedObject24;
               UNSIGNED32     mappedObject25;
               UNSIGNED32     mappedObject26;
               UNSIGNED32     mappedObject27;
               UNSIGNED32     mappedObject28;
               UNSIGNED32     mappedObject29;
               UNSIGNED32     mappedObject30;
               UNSIGNED32     mappedObject31;
               UNSIGNED32     mappedObject32;
               }              OD_scannerlistMapping_t;

/*3520      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED16     identHw;
               UNSIGNED16     identHwValue;
               UNSIGNED16     identDevice;
               UNSIGNED16     identDeviceValue;
               UNSIGNED32     softwareVersionLow;
               UNSIGNED32     softwareVersionHigh;
               UNSIGNED32     blSoftwareVersion;
               }              OD_IDENT_t;

/*4500      *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     firstTestDate;
               UNSIGNED32     serialNumberBoard;
               UNSIGNED32     articleNumberBoard;
               UNSIGNED32     distributorBoard;
               UNSIGNED32     serialNumberDevice;
               UNSIGNED32     articleNumberDevice;
               UNSIGNED32     distributorDevice;
               UNSIGNED32     testDate;
               UNSIGNED32     testTime;
               }              OD_info_t;*/

/*4510      *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     minutes;
               UNSIGNED32     init;
               UNSIGNED32     powerUpDeviceCnt;
               INTEGER16      processorMAXTemperature;
               UNSIGNED16     maxOperatingVoltage;
               }              OD_mde_t;*/

/*4520      *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     compilerVersion;
               VISIBLE_STRING compilingDate[1];
               VISIBLE_STRING compilingTime[1];
               }              OD_dev_t;*/

/*573E  *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED8      biColorLedTestResult;
               UNSIGNED16     framCheckTestResult;
               UNSIGNED16     LEDDriverTestResult;
               UNSIGNED8      hallSensorInput;
               UNSIGNED16     testDACValue1;
               UNSIGNED16     testDACValue2;
               UNSIGNED16     testDACValue3;
               UNSIGNED16     testDACValue4;
               UNSIGNED16     testDACValue5;
               UNSIGNED16     testDACValue6;
               UNSIGNED16     testDACValue7;
               UNSIGNED16     testDACValue8;
               UNSIGNED16     testDACValue9;
               UNSIGNED16     testDACValue10;
               UNSIGNED16     testDACValue11;
               UNSIGNED16     testDACValue12;
               UNSIGNED16     testDACValue13;
               UNSIGNED16     testDACValue14;
               UNSIGNED16     testDACValue15;
               UNSIGNED16     testDACValue16;
               UNSIGNED16     testDACValue17;
               UNSIGNED16     testDACValue18;
               UNSIGNED16     testDACValue19;
               UNSIGNED16     testDACValue20;
               UNSIGNED16     testDACValue21;
               UNSIGNED16     testDACValue22;
               UNSIGNED16     testDACValue23;
               UNSIGNED16     framCheckTest;
               UNSIGNED16     sensorAnalogOutSignal;
               }              OD_variousTestResults_t;
*/
/*5742 *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED16     lowerLimitPhotodiodeCurveThreshold;
               UNSIGNED16     upperLimitPhotodiodeCurveThreshold;
               UNSIGNED16     lowerLimitPhotodiodeCurveRefMinThreshold;
               UNSIGNED16     upperLimitPhotodiodeCurveRefMaxThreshold;
               UNSIGNED16     ledDriverDivider;
               UNSIGNED8      ledCurrentHighLowSwitch;
               REAL32         scalingCorrectionFactor;
               }              OD_variousPhotodiodeAdjustments_t;
*/
/*5810      *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED16     watchdogOccurredCounter;
               UNSIGNED16     powerOnOccurredCounter;
               UNSIGNED16     checksumErrors;
               UNSIGNED16     busErrors;
               UNSIGNED16     busErrors1;
               UNSIGNED16     busErrors2;
               UNSIGNED16     nmiCounter;
               }              OD_diag_t;*/

/*59A0      *//* typedef struct{
               UNSIGNED8      maxSubIndex;
               UNSIGNED32     resetAll;
               UNSIGNED32     resetMde;
               UNSIGNED32     resetDiag;
               UNSIGNED32     resetInfo;
               }              OD_reset_t;*/

/*6401      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               INTEGER16      read_Analogue_Input_1;
               INTEGER16      processorTemperatureResult;
               UNSIGNED16     actOperatingVoltage;
               }              OD_readAnalogInput_t;

/*6424      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               INTEGER16      upperLimitSensor;
               INTEGER16      upperLimitTemperature;
               INTEGER16      upperLimitVoltage;
               }              OD_analogInputUpperLimit_t;

/*6425      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               INTEGER16      lowerLimitSensor;
               INTEGER16      lowerLimitTemperature;
               INTEGER16      lowerLimitVoltage;
               }              OD_analogInputLowerLimit_t;

/*642F      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               REAL32         inputScalingSensor;
               REAL32         inputScalingTemperature;
               REAL32         inputScalingVoltage;
               }              OD_analogInputScaling_t;

/*6431      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               INTEGER16      inputOffsetSensor;
               INTEGER16      inputOffsetTemperature;
               INTEGER16      inputOffsetVoltage;
               }              OD_analogInputOffset_t;

/*7130      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               INTEGER24      edgeValue1;
               INTEGER24      edgeValue2;
               }              OD_edgeValue_t;

/*8130      */ typedef struct{
               UNSIGNED8      maxSubIndex;
               INTEGER24      firstEdgeInMicrons;
               INTEGER24      secondEdgeInMicrons;
               }              OD_edgeInMicrons_t;


/*******************************************************************************
   STRUCTURES FOR VARIABLES IN DIFFERENT MEMORY LOCATIONS
*******************************************************************************/
#define CO_OD_FIRST_LAST_WORD   0x55 //Any value from 0x01 to 0xFE. If changed, EEPROM will be reinitialized.
#define CO_DUMMY_BYTE           0x00
#define CO_DUMMY_WORD           0x0000
#define CO_DUMMY_LONG           0x00000000

/***** Structure for RAM variables ********************************************/
struct sCO_OD_RAM {
               UNSIGNED32     FirstWord;

/*1001      */ UNSIGNED8      errorRegister;
/*1002      */ UNSIGNED32     manufacturerStatusRegister;
/*1003      */ UNSIGNED32     preDefinedErrorField[8];
/*1010      */ OD_storeFlashParameters_t storeFlashParameters;
/*2100      */ OCTET_STRING   errorStatusBits[10];
/*2101      */ UNSIGNED8      CANNodeID;
/*3101      */ UNSIGNED16     controlWord;
/*3103      */ UNSIGNED16     statusWord;
/*3110      */ UNSIGNED16     materialSetupControlWord;
/*3111      */ UNSIGNED32     materialSetupStatusWord;
/*3210      */ INTEGER16      readAnalogInputAbs[1];
/*3424      */ INTEGER16      analogInputUpperLimitMax[1];
/*3425      */ INTEGER16      analogInputLowerLimitMax[1];
/*3520      */ OD_IDENT_t     IDENT;  
///*3730      */ UNSIGNED16     processorTemperature;
/*3731      */ UNSIGNED8      setCanBusTerminator;
///*4004      */ VISIBLE_STRING BSTSoftDownload[1];
///*4500      */ OD_info_t      info;
///*4510      */ OD_mde_t       mde;

/* 5500 */  UNSIGNED8   currentBeam;
/* 5501 */  UNSIGNED8   currentSignal;
/* 5502 */  UNSIGNED8   correctedSignals[128];
/* 5503 */  UNSIGNED8   rawSignals[128];
/* 55FD */  UNSIGNED8   testPowerLevel;
/* 55FE */  UNSIGNED8   testFrequency;
/* 55FF */  UNSIGNED8   testMode;

///*5600      */ INTEGER8       intLoad;
///*5700      */ INTEGER16      debugVarSw[5];
///*5701      */ INTEGER32      debugVarSdw[5];
///*5710      */ UNSIGNED8      debugLeds;
///*571A      */ UNSIGNED16     individualDigOrAnaModeValues[4];
///*5720      */ UNSIGNED32     watchdogTest;
///*5721      */ UNSIGNED32     bootloaderReset;
///*5723      */ UNSIGNED32     resetFramCheck;
///*5724      */ UNSIGNED16     individualFilteredValues[16];
///*5729      */ UNSIGNED16     dacTestActivate;
///*5730      */ UNSIGNED16     manufacturerMasterSetupControlWord;
///*5739      */ UNSIGNED16     materialSetup;
///*573C      */ UNSIGNED16     enableMaunfacturerMasterSetup;
///*573D      */ UNSIGNED16     enableReferenceTableSetup;
///*573E      */ OD_variousTestResults_t variousTestResults;
///*573F      */ UNSIGNED16     referenceTableSetup;
///*5741      */ UNSIGNED8      setMaterialSetupValsToMasterSetupVals;
///*5742      */ OD_variousPhotodiodeAdjustments_t variousPhotodiodeAdjustments;
///*5800      */ UNSIGNED32     uploadObject;
///*5810      */ OD_diag_t      diag;
///*59A0      */ OD_reset_t     reset;
///*59A1      */ UNSIGNED8      readOutProtect;
///*59B0      */ UNSIGNED32     uniqueId0Cpu;
///*59B1      */ UNSIGNED32     uniqueId1Cpu;
///*59B2      */ UNSIGNED32     uniqueId2Cpu;
///*5A00      */ UNSIGNED8      dynamicNode;
/*5A01      */ UNSIGNED8      lssLock;
/*6131      */ UNSIGNED32     physicalUnits[2];
/*6132      */ UNSIGNED8      decimalDigits[2];
/*6401      */ OD_readAnalogInput_t readAnalogInput;
/*6424      */ OD_analogInputUpperLimit_t analogInputUpperLimit;
/*6425      */ OD_analogInputLowerLimit_t analogInputLowerLimit;
/*642F      */ OD_analogInputScaling_t analogInputScaling;
/*6431      */ OD_analogInputOffset_t analogInputOffset;
///*7100      */ INTEGER16      normedADC1MeanValue[16];
///*7120      */ INTEGER16      photodiodeFilterMaterialMinValue[16];
///*7122      */ INTEGER16      photodiodeFilterMaterialMaxValue[16];
/*7130      */ OD_edgeValue_t edgeValue;
///*7140      */ INTEGER16      photodiodeFilterMasterMinValue[16];
///*7142      */ INTEGER16      photodiodeFilterMasterMaxValue[16];
/*8130      */ OD_edgeInMicrons_t edgeInMicrons;
/*9100      */ INTEGER32      fieldValue[1];
/*9120      */ INTEGER32      fieldValueMIN[1];
/*9121      */ INTEGER32      processValueMIN[2];
/*9122      */ INTEGER32      fieldValueMAX[1];
/*9123      */ INTEGER32      processValueMAX[2];
/*9124      */ INTEGER32      processValueOFFSET[2];
/*9130      */ INTEGER32      processValue[2];
/*9140      */ INTEGER32      fieldValueLimitMin[1];
/*9142      */ INTEGER32      fieldValueLimitMax[1];

               UNSIGNED32     LastWord;
};



/***** Structure for EEPROM variables *****************************************/
struct sCO_OD_EEPROM {
            UNSIGNED8       FirstWord;

/*1008*/    VISIBLE_STRING  manufacturerDeviceName[32];
/*2200*/    UNSIGNED8       sensorType;
/*2201*/    UNSIGNED8       sensorGap;
/*2202*/    UNSIGNED8       sensorMaxBeam;
/*5504*/    UNSIGNED8       calibrationPowerLevels[128];
/*5505*/    UNSIGNED8       calibrationReceiveValues[128];
/*5506*/    UNSIGNED8       calibrationOpacity;
            UNSIGNED8       LastWord;
};


/***** Structure for ROM variables ********************************************/
struct sCO_OD_ROM {
               UNSIGNED32                   FirstWord;

/*1000      */ UNSIGNED32                   deviceType;
/*1005      */ UNSIGNED32                   COB_ID_SYNCMessage;
/*1006      */ UNSIGNED32                   communicationCyclePeriod;
/*100A      */ VISIBLE_STRING               manufacturerSoftwareVersion[8];
/*1014      */ UNSIGNED32                   emergencyCOB_ID;
/*1015      */ UNSIGNED16                   inhibitTimeEmergency;
/*1016      */ UNSIGNED32                   consumerHeartbeatTime[1];
/*1017      */ UNSIGNED16                   producerHeartbeatTime;
/*1018      */ OD_identity_t                identity;
/*1200[1]   */ OD_SDOServerParameter_t      SDOServerParameter[1];
/*1400[1]   */ OD_RPDOCommunicationParameter_t RPDOCommunicationParameter[1];
/*1600[1]   */ OD_RPDOMappingParameter_t     RPDOMappingParameter[1];
/*1800[2]   */ OD_TPDOCommunicationParameter_t TPDOCommunicationParameter[2];
/*1A00[1]   */ OD_TPDOMappingParameter_t    TPDOMappingParameter[1];
/*1F80      */ UNSIGNED32                   NMTStartup;
/*1FA0      */ OD_scannerlistMapping_t      scannerlistMapping;

/*2102      */ UNSIGNED16                   CANBitRate;

///*4520      */ OD_dev_t                     dev;

               UNSIGNED32                   LastWord;
};


/***** Declaration of Object Dictionary variables *****************************/
extern struct       sCO_OD_RAM      CO_OD_RAM;

extern struct       sCO_OD_EEPROM   CO_OD_EEPROM;

extern const struct sCO_OD_ROM      CO_OD_ROM;


/*******************************************************************************
   ALIASES FOR OBJECT DICTIONARY VARIABLES
*******************************************************************************/
/*1000, Data Type: UNSIGNED32 */
      #define OD_deviceType                              CO_OD_ROM.deviceType

/*1001, Data Type: UNSIGNED8 */
      #define OD_errorRegister                           CO_OD_RAM.errorRegister

/*1002, Data Type: UNSIGNED32 */
      #define OD_manufacturerStatusRegister              CO_OD_RAM.manufacturerStatusRegister

/*1003, Data Type: UNSIGNED32, Array[8] */
      #define OD_preDefinedErrorField                    CO_OD_RAM.preDefinedErrorField
      #define ODL_preDefinedErrorField_arrayLength       8

/*1005, Data Type: UNSIGNED32 */
      #define OD_COB_ID_SYNCMessage                      CO_OD_ROM.COB_ID_SYNCMessage

/*1006, Data Type: UNSIGNED32 */
      #define OD_communicationCyclePeriod                CO_OD_ROM.communicationCyclePeriod

/*1008, Data Type: VISIBLE_STRING, Array[32] */
      #define OD_manufacturerDeviceName                  CO_OD_EEPROM.manufacturerDeviceName
      #define ODL_manufacturerDeviceName_stringLength    32

/*100A, Data Type: VISIBLE_STRING, Array[8] */
      #define OD_manufacturerSoftwareVersion             CO_OD_ROM.manufacturerSoftwareVersion
      #define ODL_manufacturerSoftwareVersion_stringLength 8

/*1010, Data Type: OD_storeFlashParameters_t */
      #define OD_storeFlashParameters                    CO_OD_RAM.storeFlashParameters

/*1014, Data Type: UNSIGNED32 */
      #define OD_emergencyCOB_ID                         CO_OD_ROM.emergencyCOB_ID

/*1015, Data Type: UNSIGNED16 */
      #define OD_inhibitTimeEmergency                    CO_OD_ROM.inhibitTimeEmergency

/*1016, Data Type: UNSIGNED32, Array[1] */
      #define OD_consumerHeartbeatTime                   CO_OD_ROM.consumerHeartbeatTime
      #define ODL_consumerHeartbeatTime_arrayLength      1
      #define ODA_consumerHeartbeatTime_1                0

/*1017, Data Type: UNSIGNED16 */
      #define OD_producerHeartbeatTime                   CO_OD_ROM.producerHeartbeatTime

/*1018, Data Type: OD_identity_t */
      #define OD_identity                                CO_OD_ROM.identity

/*1400[1], Data Type: OD_receivePDO1Parameter_t, Array[1] */
      #define OD_RPDOCommunicationParameter              CO_OD_ROM.RPDOCommunicationParameter

/*1600[1], Data Type: OD_receivePDO1Mapping_t, Array[1] */
      #define OD_RPDOMappingParameter                    CO_OD_ROM.RPDOMappingParameter

/*1800[2], Data Type: OD_transmitPDO1Parameter_t, Array[1] */
      #define OD_TPDOCommunicationParameter              CO_OD_ROM.TPDOCommunicationParameter[0]
      #define OD_MUXPDOCommunicationParameter            CO_OD_ROM.TPDOCommunicationParameter[1]

/*1A00[1], Data Type: OD_transmitPDO1Mapping_t, Array[1] */
      #define OD_TPDOMappingParameter                    CO_OD_ROM.TPDOMappingParameter

/*1F80, Data Type: UNSIGNED32 */
      #define OD_NMTStartup                              CO_OD_ROM.NMTStartup

/*1FA0, Data Type: OD_scannerlistMapping_t */
      #define OD_scannerlistMapping                      CO_OD_ROM.scannerlistMapping

/*2100, Data Type: OCTET_STRING, Array[10] */
      #define OD_errorStatusBits                         CO_OD_RAM.errorStatusBits
      #define ODL_errorStatusBits_stringLength           10

/*2101, Data Type: UNSIGNED8 */
      #define OD_CANNodeID                               CO_OD_RAM.CANNodeID

/*2102, Data Type: UNSIGNED16 */
      #define OD_CANBitRate                              CO_OD_ROM.CANBitRate

/*2200, Data Type: UNSIGNED8 */
    #define OD_sensorType                               CO_OD_EEPROM.sensorType

/*2201, Data Type: UNSIGNED8 */
     #define OD_sensorGap                               CO_OD_EEPROM.sensorGap

/*2202, Data Type: UNSIGNED8 */
    #define OD_sensorMaxBeam                            CO_OD_EEPROM.sensorMaxBeam

/*3101, Data Type: UNSIGNED16 */
      #define OD_controlWord                             CO_OD_RAM.controlWord

/*3103, Data Type: UNSIGNED16 */
      #define OD_statusWord                              CO_OD_RAM.statusWord

/*3110, Data Type: UNSIGNED16 */
      #define OD_materialSetupControlWord                CO_OD_RAM.materialSetupControlWord

/*3111, Data Type: UNSIGNED32 */
      #define OD_materialSetupStatusWord                 CO_OD_RAM.materialSetupStatusWord

/*3210, Data Type: INTEGER16, Array[1] */
      #define OD_readAnalogInputAbs                      CO_OD_RAM.readAnalogInputAbs
      #define ODL_readAnalogInputAbs_arrayLength         1
      #define ODA_readAnalogInputAbs_readAnalogInputAbs  0

/*3424, Data Type: INTEGER16, Array[1] */
      #define OD_analogInputUpperLimitMax                CO_OD_RAM.analogInputUpperLimitMax
      #define ODL_analogInputUpperLimitMax_arrayLength   1
      #define ODA_analogInputUpperLimitMax_1             0

/*3425, Data Type: INTEGER16, Array[1] */
      #define OD_analogInputLowerLimitMax                CO_OD_RAM.analogInputLowerLimitMax
      #define ODL_analogInputLowerLimitMax_arrayLength   1
      #define ODA_analogInputLowerLimitMax_1             0

/*3520, Data Type: OD_IDENT_t */
      #define OD_IDENT                                   CO_OD_RAM.IDENT

/*3730, Data Type: UNSIGNED16 */
//      #define OD_processorTemperature                    CO_OD_RAM.processorTemperature

/*3731, Data Type: UNSIGNED8 */
      #define OD_setCanBusTerminator                     CO_OD_RAM.setCanBusTerminator

/*4004, Data Type: VISIBLE_STRING, Array[1] */
//      #define OD_BSTSoftDownload                         CO_OD_RAM.BSTSoftDownload
//      #define ODL_BSTSoftDownload_stringLength           1

/*4500, Data Type: OD_info_t */
//      #define OD_info                                    CO_OD_RAM.info

/*4510, Data Type: OD_mde_t */
//      #define OD_mde                                     CO_OD_RAM.mde

/*4520, Data Type: OD_dev_t */
//      #define OD_dev                                     CO_OD_ROM.dev


/* 5500, Data Type: UNSIGNED8 */
    #define OD_currentBeam                          CO_OD_RAM.currentBeam
/* 5501, Data Type: UNSIGNED8 */
    #define OD_currentSignal                        CO_OD_RAM.currentSignal
/* 5502, Data Type: UNSIGNED8, Array[128] */
    #define OD_correctedSignals                     CO_OD_RAM.correctedSignals
    #define OD_correctedSignals_arrayLength         128
/* 5503, Data Type: UNSIGNED8, Array[128] */
    #define OD_rawSignals                           CO_OD_RAM.rawSignals
    #define OD_rawSignals_arrayLength               128
/* 5504, Data Type: UNSIGNED8, Array[128] */
    #define OD_calibrationPowerLevels               CO_OD_EEPROM.calibrationPowerLevels
    #define OD_calibrationPowerLevels_arrayLength   128
/* 5505, Data Type: UNSIGNED8, Array[128] */
    #define OD_calibrationReceiveValues             CO_OD_EEPROM.calibrationReceiveValues
    #define OD_calibrationReceiveValues_arrayLength 128
/* 5506, Data Type: UNSIGNED8 */
    #define OD_calibrationOpacity                   CO_OD_EEPROM.calibrationOpacity
/* 55FD, Data Type: UNSIGNED8 */
    #define OD_testPowerLevel                       CO_OD_RAM.testPowerLevel
/* 55FE, Data Type: UNSIGNED8 */
    #define OD_testFrequency                        CO_OD_RAM.testFrequency
/* 55FF, Data Type: UNSIGNED8 */
    #define OD_testMode                             CO_OD_RAM.testMode

/*5600, Data Type: INTEGER8 */
//      #define OD_intLoad                                 CO_OD_RAM.intLoad

/*5700, Data Type: INTEGER16, Array[5] *//*
      #define OD_debugVarSw                              CO_OD_RAM.debugVarSw
      #define ODL_debugVarSw_arrayLength                 5
      #define ODA_debugVarSw_0                           0
      #define ODA_debugVarSw_1                           1
      #define ODA_debugVarSw_2                           2
      #define ODA_debugVarSw_3                           3
      #define ODA_debugVarSw_4                           4*/

/*5701, Data Type: INTEGER32, Array[5] *//*
      #define OD_debugVarSdw                             CO_OD_RAM.debugVarSdw
      #define ODL_debugVarSdw_arrayLength                5
      #define ODA_debugVarSdw_0                          0
      #define ODA_debugVarSdw_1                          1
      #define ODA_debugVarSdw_2                          2
      #define ODA_debugVarSdw_3                          3
      #define ODA_debugVarSdw_4                          4*/

/*5710, Data Type: UNSIGNED8 *//*
      #define OD_debugLeds                               CO_OD_RAM.debugLeds*/

/*571A, Data Type: UNSIGNED16, Array[4] *//*
      #define OD_individualDigOrAnaModeValues            CO_OD_RAM.individualDigOrAnaModeValues
      #define ODL_individualDigOrAnaMode_arrayLength     4
      #define ODA_individualDigOrAnaMode_0               0
      #define ODA_individualDigOrAnaMode_1               1
      #define ODA_individualDigOrAnaMode_2               2
      #define ODA_individualDigOrAnaMode_3               3*/

/*5720, Data Type: UNSIGNED32 *//*
      #define OD_watchdogTest                            CO_OD_RAM.watchdogTest*/

/*5721, Data Type: UNSIGNED32 *//*
      #define OD_bootloaderReset                         CO_OD_RAM.bootloaderReset*/

/*5723, Data Type: UNSIGNED32 *//*
      #define OD_resetFramCheck                          CO_OD_RAM.resetFramCheck*/

/*5724, Data Type: UNSIGNED16, Array[16] *//*
      #define OD_individualFilteredValues                CO_OD_RAM.individualFilteredValues
      #define ODL_individualFilteredValues_arrayLength   16
      #define ODA_individualFilteredValues_0             0
      #define ODA_individualFilteredValues_1             1
      #define ODA_individualFilteredValues_2             2
      #define ODA_individualFilteredValues_3             3
      #define ODA_individualFilteredValues_4             4
      #define ODA_individualFilteredValues_5             5
      #define ODA_individualFilteredValues_6             6
      #define ODA_individualFilteredValues_7             7
      #define ODA_individualFilteredValues_8             8
      #define ODA_individualFilteredValues_9             9
      #define ODA_individualFilteredValues_10            10
      #define ODA_individualFilteredValues_11            11
      #define ODA_individualFilteredValues_12            12
      #define ODA_individualFilteredValues_13            13
      #define ODA_individualFilteredValues_14            14
      #define ODA_individualFilteredValues_15            15*/

/*5729, Data Type: UNSIGNED16 */
//      #define OD_dacTestActivate                         CO_OD_RAM.dacTestActivate

/*5730, Data Type: UNSIGNED16 */
//      #define OD_manufacturerMasterSetupControlWord      CO_OD_RAM.manufacturerMasterSetupControlWord

/*5739, Data Type: UNSIGNED16 */
//      #define OD_materialSetup                           CO_OD_RAM.materialSetup

/*573C, Data Type: UNSIGNED16 */
//      #define OD_enableMaunfacturerMasterSetup           CO_OD_RAM.enableMaunfacturerMasterSetup

/*573D, Data Type: UNSIGNED16 */
//      #define OD_enableReferenceTableSetup               CO_OD_RAM.enableReferenceTableSetup

/*573E, Data Type: OD_variousTestResults_t */
//      #define OD_variousTestResults                      CO_OD_RAM.variousTestResults

/*573F, Data Type: UNSIGNED16 */
//      #define OD_referenceTableSetup                     CO_OD_RAM.referenceTableSetup

/*5741, Data Type: UNSIGNED8 */
//      #define OD_setMaterialSetupValsToMasterSetupVals   CO_OD_RAM.setMaterialSetupValsToMasterSetupVals

/*5742, Data Type: OD_variousPhotodiodeAdjustments_t */
//      #define OD_variousPhotodiodeAdjustments            CO_OD_RAM.variousPhotodiodeAdjustments

/*5800, Data Type: UNSIGNED32 */
//      #define OD_uploadObject                            CO_OD_RAM.uploadObject

/*5810, Data Type: OD_diag_t */
//      #define OD_diag                                    CO_OD_RAM.diag

/*59A0, Data Type: OD_reset_t */
//      #define OD_reset                                   CO_OD_RAM.reset

/*59A1, Data Type: UNSIGNED8 */
//      #define OD_readOutProtect                          CO_OD_RAM.readOutProtect

/*59B0, Data Type: UNSIGNED32 */
//      #define OD_uniqueId0Cpu                            CO_OD_RAM.uniqueId0Cpu

/*59B1, Data Type: UNSIGNED32 */
//      #define OD_uniqueId1Cpu                            CO_OD_RAM.uniqueId1Cpu

/*59B2, Data Type: UNSIGNED32 */
//      #define OD_uniqueId2Cpu                            CO_OD_RAM.uniqueId2Cpu

/*5A00, Data Type: UNSIGNED8 */
//      #define OD_dynamicNode                             CO_OD_RAM.dynamicNode

/*5A01, Data Type: UNSIGNED8 */
      #define OD_lssLock                                 CO_OD_RAM.lssLock

/*6131, Data Type: UNSIGNED32, Array[2] */
      #define OD_physicalUnits                           CO_OD_RAM.physicalUnits
      #define ODL_physicalUnits_arrayLength              2
      #define ODA_physicalUnits_1                        0
      #define ODA_physicalUnits_2                        1

/*6132, Data Type: UNSIGNED8, Array[2] */
      #define OD_decimalDigits                           CO_OD_RAM.decimalDigits
      #define ODL_decimalDigits_arrayLength              2
      #define ODA_decimalDigits_1                        0
      #define ODA_decimalDigits_2                        1

/*6401, Data Type: OD_readAnalogInput_t */
      #define OD_readAnalogInput                         CO_OD_RAM.readAnalogInput

/*6424, Data Type: OD_analogInputUpperLimit_t */
      #define OD_analogInputUpperLimit                   CO_OD_RAM.analogInputUpperLimit

/*6425, Data Type: OD_analogInputLowerLimit_t */
      #define OD_analogInputLowerLimit                   CO_OD_RAM.analogInputLowerLimit

/*642F, Data Type: OD_analogInputScaling_t */
      #define OD_analogInputScaling                      CO_OD_RAM.analogInputScaling

/*6431, Data Type: OD_analogInputOffset_t */
      #define OD_analogInputOffset                       CO_OD_RAM.analogInputOffset

/*7100, Data Type: INTEGER16, Array[16] *//*
      #define OD_normedADC1MeanValue                     CO_OD_RAM.normedADC1MeanValue
      #define ODL_normedADC1MeanValue_arrayLength        16
      #define ODA_normedADC1MeanValue_1                  0
      #define ODA_normedADC1MeanValue_2                  1
      #define ODA_normedADC1MeanValue_3                  2
      #define ODA_normedADC1MeanValue_4                  3
      #define ODA_normedADC1MeanValue_5                  4
      #define ODA_normedADC1MeanValue_6                  5
      #define ODA_normedADC1MeanValue_7                  6
      #define ODA_normedADC1MeanValue_8                  7
      #define ODA_normedADC1MeanValue_9                  8
      #define ODA_normedADC1MeanValue_10                 9
      #define ODA_normedADC1MeanValue_11                 10
      #define ODA_normedADC1MeanValue_12                 11
      #define ODA_normedADC1MeanValue_13                 12
      #define ODA_normedADC1MeanValue_14                 13
      #define ODA_normedADC1MeanValue_15                 14
      #define ODA_normedADC1MeanValue_16                 15*/

/*7120, Data Type: INTEGER16, Array[16] *//*
      #define OD_photodiodeFilterMaterialMinValue        CO_OD_RAM.photodiodeFilterMaterialMinValue
      #define ODL_photodiodeFilterMaterialMinValue_arrayLength 16
      #define ODA_photodiodeFilterMaterialMinValue_1     0
      #define ODA_photodiodeFilterMaterialMinValue_2     1
      #define ODA_photodiodeFilterMaterialMinValue_3     2
      #define ODA_photodiodeFilterMaterialMinValue_4     3
      #define ODA_photodiodeFilterMaterialMinValue_5     4
      #define ODA_photodiodeFilterMaterialMinValue_6     5
      #define ODA_photodiodeFilterMaterialMinValue_7     6
      #define ODA_photodiodeFilterMaterialMinValue_8     7
      #define ODA_photodiodeFilterMaterialMinValue_9     8
      #define ODA_photodiodeFilterMaterialMinValue_10    9
      #define ODA_photodiodeFilterMaterialMinValue_11    10
      #define ODA_photodiodeFilterMaterialMinValue_12    11
      #define ODA_photodiodeFilterMaterialMinValue_13    12
      #define ODA_photodiodeFilterMaterialMinValue_14    13
      #define ODA_photodiodeFilterMaterialMinValue_15    14
      #define ODA_photodiodeFilterMaterialMinValue_16    15*/

/*7122, Data Type: INTEGER16, Array[16] *//*
      #define OD_photodiodeFilterMaterialMaxValue        CO_OD_RAM.photodiodeFilterMaterialMaxValue
      #define ODL_photodiodeFilterMaterialMaxValue_arrayLength 16
      #define ODA_photodiodeFilterMaterialMaxValue_1     0
      #define ODA_photodiodeFilterMaterialMaxValue_2     1
      #define ODA_photodiodeFilterMaterialMaxValue_3     2
      #define ODA_photodiodeFilterMaterialMaxValue_4     3
      #define ODA_photodiodeFilterMaterialMaxValue_5     4
      #define ODA_photodiodeFilterMaterialMaxValue_6     5
      #define ODA_photodiodeFilterMaterialMaxValue_7     6
      #define ODA_photodiodeFilterMaterialMaxValue_8     7
      #define ODA_photodiodeFilterMaterialMaxValue_9     8
      #define ODA_photodiodeFilterMaterialMaxValue_10    9
      #define ODA_photodiodeFilterMaterialMaxValue_11    10
      #define ODA_photodiodeFilterMaterialMaxValue_12    11
      #define ODA_photodiodeFilterMaterialMaxValue_13    12
      #define ODA_photodiodeFilterMaterialMaxValue_14    13
      #define ODA_photodiodeFilterMaterialMaxValue_15    14
      #define ODA_photodiodeFilterMaterialMaxValue_16    15*/

/*7130, Data Type: OD_edgeValue_t */
      #define OD_edgeValue                               CO_OD_RAM.edgeValue

/*7140, Data Type: INTEGER16, Array[16] *//*
      #define OD_photodiodeFilterMasterMinValue          CO_OD_RAM.photodiodeFilterMasterMinValue
      #define ODL_photodiodeFilterMasterMinValue_arrayLength 16
      #define ODA_photodiodeFilterMasterMinValue_1       0
      #define ODA_photodiodeFilterMasterMinValue_2       1
      #define ODA_photodiodeFilterMasterMinValue_3       2
      #define ODA_photodiodeFilterMasterMinValue_4       3
      #define ODA_photodiodeFilterMasterMinValue_5       4
      #define ODA_photodiodeFilterMasterMinValue_6       5
      #define ODA_photodiodeFilterMasterMinValue_7       6
      #define ODA_photodiodeFilterMasterMinValue_8       7
      #define ODA_photodiodeFilterMasterMinValue_9       8
      #define ODA_photodiodeFilterMasterMinValue_10      9
      #define ODA_photodiodeFilterMasterMinValue_11      10
      #define ODA_photodiodeFilterMasterMinValue_12      11
      #define ODA_photodiodeFilterMasterMinValue_13      12
      #define ODA_photodiodeFilterMasterMinValue_14      13
      #define ODA_photodiodeFilterMasterMinValue_15      14
      #define ODA_photodiodeFilterMasterMinValue_16      15*/

/*7142, Data Type: INTEGER16, Array[16] *//*
      #define OD_photodiodeFilterMasterMaxValue          CO_OD_RAM.photodiodeFilterMasterMaxValue
      #define ODL_photodiodeFilterMasterMaxValue_arrayLength 16
      #define ODA_photodiodeFilterMasterMaxValue_1       0
      #define ODA_photodiodeFilterMasterMaxValue_2       1
      #define ODA_photodiodeFilterMasterMaxValue_3       2
      #define ODA_photodiodeFilterMasterMaxValue_4       3
      #define ODA_photodiodeFilterMasterMaxValue_5       4
      #define ODA_photodiodeFilterMasterMaxValue_6       5
      #define ODA_photodiodeFilterMasterMaxValue_7       6
      #define ODA_photodiodeFilterMasterMaxValue_8       7
      #define ODA_photodiodeFilterMasterMaxValue_9       8
      #define ODA_photodiodeFilterMasterMaxValue_10      9
      #define ODA_photodiodeFilterMasterMaxValue_11      10
      #define ODA_photodiodeFilterMasterMaxValue_12      11
      #define ODA_photodiodeFilterMasterMaxValue_13      12
      #define ODA_photodiodeFilterMasterMaxValue_14      13
      #define ODA_photodiodeFilterMasterMaxValue_15      14
      #define ODA_photodiodeFilterMasterMaxValue_16      15*/

/*8130, Data Type: OD_edgeInMicrons_t */
      #define OD_edgeInMicrons                           CO_OD_RAM.edgeInMicrons

/*9100, Data Type: INTEGER32, Array[1] */
      #define OD_fieldValue                              CO_OD_RAM.fieldValue
      #define ODL_fieldValue_arrayLength                 1
      #define ODA_fieldValue_fieldValue_1                0

/*9120, Data Type: INTEGER32, Array[1] */
      #define OD_fieldValueMIN                           CO_OD_RAM.fieldValueMIN
      #define ODL_fieldValueMIN_arrayLength              1
      #define ODA_fieldValueMIN_1                        0

/*9121, Data Type: INTEGER32, Array[2] */
      #define OD_processValueMIN                         CO_OD_RAM.processValueMIN
      #define ODL_processValueMIN_arrayLength            2
      #define ODA_processValueMIN_1                      0
      #define ODA_processValueMIN_2                      1

/*9122, Data Type: INTEGER32, Array[1] */
      #define OD_fieldValueMAX                           CO_OD_RAM.fieldValueMAX
      #define ODL_fieldValueMAX_arrayLength              1
      #define ODA_fieldValueMAX_1                        0

/*9123, Data Type: INTEGER32, Array[2] */
      #define OD_processValueMAX                         CO_OD_RAM.processValueMAX
      #define ODL_processValueMAX_arrayLength            2
      #define ODA_processValueMAX_1                      0
      #define ODA_processValueMAX_2                      1

/*9124, Data Type: INTEGER32, Array[2] */
      #define OD_processValueOFFSET                     CO_OD_RAM.processValueOFFSET
      #define ODL_processValueOFFSET_arrayLength        2
      #define ODA_processValueOFFSET_1                  0
      #define ODA_processValueOFFSET_2                  1

/*9130, Data Type: INTEGER32, Array[2] */
      #define OD_processValue                            CO_OD_RAM.processValue
      #define ODL_processValue_arrayLength               2
      #define ODA_processValue_firstEdgeInUm             0
      #define ODA_processValue_secondEdgeInUm            1

/*9140, Data Type: INTEGER32, Array[1] */
      #define OD_fieldValueLimitMin                      CO_OD_RAM.fieldValueLimitMin
      #define ODL_fieldValueLimitMin_arrayLength         1
      #define ODA_fieldValueLimitMin_1                   0

/*9142, Data Type: INTEGER32, Array[1] */
      #define OD_fieldValueLimitMax                      CO_OD_RAM.fieldValueLimitMax
      #define ODL_fieldValueLimitMax_arrayLength         1
      #define ODA_fieldValueLimitMax_1                   0


#endif
