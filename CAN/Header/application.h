


#define EDGE_1_LOCKED           0x0010  // 0000 0000 0001 0000
#define EDGE_2_LOCKED           0x0020  // 0000 0000 0010 0000
#define EDGE_1_INVALID          0x0040  // 0000 0000 0100 0000
#define EDGE_2_INVALID          0x0080  // 0000 0000 1000 0000

#define ANALOG_MODE_AT_CANBUS   0x0800  // 0000 1000 0000 0000



// Global variables:
volatile unsigned char CAN_1msFlag;


// Global function declarations:
void init_CANopen(void);
void reset_CAN_node(void);
//unsigned char CANopen_device_process(unsigned int edgeValue1, unsigned int edgeValue2);
unsigned int make_status_word(void);
unsigned char do_CANopen_tasks(unsigned char msTick, unsigned int edgeValue1, unsigned int edgeValue2);
unsigned char CANopen_main_process(unsigned char msTickFlag, unsigned int edgePosition1, unsigned int edgePosition2);

