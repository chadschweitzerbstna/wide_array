/*******************************************************************************

   File: CO_driver.h

*******************************************************************************/

#ifndef _CO_DRIVER_H
#define _CO_DRIVER_H


#define CAN_MSG_BUFFER_SIZE 16

// -------------------------------------------------------------------------------------------------
// CAN message structure.
typedef struct
{
    unsigned int   COB_ID;
    unsigned char   dataLen;
    unsigned char   data[8];
} CO_CANmsg_t;


// -------------------------------------------------------------------------------------------------
// Structure/buffer for storing incomming and outgoing CAN messages.
// Messages are stored in a circular buffer with a max capacity of 8 messages.
typedef struct {
    CO_CANmsg_t     msg[CAN_MSG_BUFFER_SIZE];
    unsigned char   head;
    unsigned char   tail;
    unsigned char   overflow;
} CO_msg_buf_t;


// -------------------------------------------------------------------------------------------------
// Structure for the CAN module (hardware interface)
// and CAN TX and RX message buffers
typedef struct {
    CO_msg_buf_t    CO_RX_buffer;
    CO_msg_buf_t    CO_TX_buffer;
    unsigned char   TXbusy;
    unsigned char   RXbusy;
    unsigned char   firstMessage;
    unsigned char   CANtxCount;
    unsigned char   CANrxCount;
    unsigned long int errOld;
} CO_CANmodule_t;


// -------------------------------------------------------------------------------------------------
// *** External Globals ***
CO_CANmodule_t CO_CANmodule;


// -------------------------------------------------------------------------------------------------
// *** Function Prototypes ***
void CO_CANmodule_init(void);
signed short int CO_CANsend(CO_CANmsg_t msg);
void CO_CAN_check_for_errors(void);
inline void CO_mem_copy(unsigned char *pDest, unsigned char *pSource, unsigned int numBytes);
inline void memcpySwap2(unsigned char* pDest, unsigned char* pSource);
inline void memcpySwap4(unsigned char* pDest, unsigned char* pSource);


/*******************************************************************************
   Constants: CANopen basic data types

   For PIC32MX:
      CO_OD_ROM_IDENT   - Identifier for CO_OD_ROM varibbles is none, so variables are stored in RAM.
      UNSIGNED8         - Of type unsigned char
      UNSIGNED16        - Of type unsigned short int
      UNSIGNED32        - Of type unsigned long int
      UNSIGNED64        - Of type unsigned long long int
      INTEGER8          - Of type signed char
      INTEGER16         - Of type signed short int
      INTEGER32         - Of type signed long
      INTEGER64         - Of type signed long long int
      REAL32            - Of type float
      REAL64            - Of type long double
      VISIBLE_STRING    - Of type char
      OCTET_STRING      - Of type unsigned char
      DOMAIN            - Application specific
*******************************************************************************/
#define UNSIGNED8       unsigned char
#define UNSIGNED16      unsigned short int
#define UNSIGNED32      unsigned long int
#define UNSIGNED64      unsigned long long int
#define INTEGER8        signed char
#define INTEGER16       signed short int
#define INTEGER24       signed long int
#define INTEGER32       signed long int
#define INTEGER64       signed long long int
#define REAL32          float
#define REAL64          long double
#define VISIBLE_STRING  char
#define OCTET_STRING    unsigned char
#define DOMAIN          unsigned char


/*******************************************************************************
   Constants: CO_ReturnError

   Return values of most CANopen functions. If function was executed
   successfully it returns 0 otherwise it returns <0.

    0 = CO_ERROR_NO                 - Operation completed successfully.
   -1 = CO_ERROR_ILLEGAL_ARGUMENT   - Error in function arguments.
   -2 = CO_ERROR_OUT_OF_MEMORY      - Memory allocation failed.
   -3 = CO_ERROR_TIMEOUT            - Function timeout.
   -4 = CO_ERROR_ILLEGAL_BAUDRATE   - Illegal baudrate passed to function <CO_CANmodule_init>
   -5 = CO_ERROR_RX_OVERFLOW        - Previous message was not processed yet.
   -6 = CO_ERROR_RX_PDO_OWERFLOW    - previous PDO was not processed yet.
   -7 = CO_ERROR_RX_MSG_LENGTH      - Wrong receive message length.
   -8 = CO_ERROR_RX_PDO_LENGTH      - Wrong receive PDO length.
   -9 = CO_ERROR_TX_OVERFLOW        - Previous message is still waiting, buffer full.
   -10 = CO_ERROR_TX_PDO_WINDOW     - Synchronous TPDO is outside window.
   -11 = CO_ERROR_TX_UNCONFIGURED   - Transmit buffer was not confugured properly.
*******************************************************************************/
enum CO_ReturnError
{
    CO_ERROR_NO                 = 0,
    CO_ERROR_ILLEGAL_ARGUMENT   = -1,
    CO_ERROR_OUT_OF_MEMORY      = -2,
    CO_ERROR_TIMEOUT            = -3,
    CO_ERROR_ILLEGAL_BAUDRATE   = -4,
    CO_ERROR_RX_OVERFLOW        = -5,
    CO_ERROR_RX_PDO_OWERFLOW    = -6,
    CO_ERROR_RX_MSG_LENGTH      = -7,
    CO_ERROR_RX_PDO_LENGTH      = -8,
    CO_ERROR_TX_OVERFLOW        = -9,
    CO_ERROR_TX_PDO_WINDOW      = -10,
    CO_ERROR_TX_UNCONFIGURED    = -11
};








#endif