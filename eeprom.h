/* 
 * File:   eeprom.h
 * Author: cls
 *
 * Created on June 23, 2015, 10:07 AM
 */

#ifndef EEPROM_H
#define	EEPROM_H



#include "nvm.h"



// General defines
#define WR_ENABLE_DELAY     1       // micro-seconds to wait after write enable.
#define EEPROM_BASE_ADDR    0x00
#define SYSTEM_DATA_ADDR    EEPROM_BASE_ADDR
#define CAN_DATA_ADDR     ( EEPROM_BASE_ADDR + sizeof(systemData) )



// Global function definitions
void read_eeprom(unsigned char *pDest, unsigned int eepromStartAddr, unsigned int numBytes);
void write_eeprom(unsigned char *pSource, unsigned int eepromStartAddr, unsigned int numBytes);
void update_eeprom(unsigned char *pSource, unsigned int eepromStartAddr, unsigned int numBytes);
void do_eeprom_task(void);
void start_eeprom_write_task(unsigned char *pSource, unsigned int eeStartAddr, unsigned int numBytes);
unsigned char eepromTaskIsIdle(void);
void init_eeprom_task(void);


#endif	/* EEPROM_H */

