/* 
 * File:   analog.h
 * Author: cls
 *
 * Created on June 23, 2015, 8:53 AM
 */

#ifndef ANALOG_H
#define	ANALOG_H



void init_ADC(void);
void select_ADC_channel(unsigned char channel);
unsigned int get_ADC_result(unsigned char channel);



#endif	/* ANALOG_H */

