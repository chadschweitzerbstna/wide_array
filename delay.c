#include <xc.h>
#include "delay.h"

#ifndef SYS_FCY
#define SYS_FCY         80000000UL              // System Clock frequency = 80Mhz
#endif

#define CORE_TMR_FCY    (SYS_FCY/2UL)           // Core Timer frequency = System Clock frequency / 2
#define MILLISEC_CONST  (CORE_TMR_FCY/1000UL)   // Ticks per milliscond
#define MICROSEC_CONST  (CORE_TMR_FCY/1000000UL)// Ticks per microsecond


inline unsigned long __attribute__ ((nomips16))ReadCoreTimer(void);
static volatile unsigned int coreTimerSample;


// -----------------------------------------------------------------------------------------------
//  A generic microsecond delay routine for the PIC32 using the core timer.
//
void delay_us(unsigned int d)
{
    volatile unsigned int startTime = ReadCoreTimer();
    volatile unsigned int delayCount = MICROSEC_CONST * d;

    while ( (ReadCoreTimer() - startTime) < delayCount ) {;;}
}


// -----------------------------------------------------------------------------------------------
//  A generic millisecond delay routine for the PIC32 using the core timer.
//
void delay_ms(unsigned int d)
{
    volatile unsigned int startTime = ReadCoreTimer();
    volatile unsigned int delayCount = MILLISEC_CONST * d;

    while ( (ReadCoreTimer() - startTime) < delayCount );
}


// ------------------------------------------------------------------------------------------------
//  Set the coreTimerSample to the current core timer value.  Used with get_usTimer()
//  to "zero" the elapsed time.
//
void zero_usTimer(void)
{
    coreTimerSample = ReadCoreTimer();
}


// ------------------------------------------------------------------------------------------------
//  Get the elapsed time since zero_usTimer() has been called.
//  Used to measure the time elapsed while doing a process or activity or to generate 
//  non-blocking delays.
//
unsigned int get_usTimer(void)
{
    volatile unsigned int elapsedTime;

    elapsedTime = ReadCoreTimer();
    elapsedTime = elapsedTime - coreTimerSample;
    elapsedTime = elapsedTime / MICROSEC_CONST;

    return elapsedTime;
}


// ------------------------------------------------------------------------------------------------
//  Read the Coprocessor0 (CP0) "Processor Cycle Count" register (register number 9).
inline unsigned long __attribute__ ((nomips16))ReadCoreTimer(void)
{
    volatile register unsigned long tmpLong;

    //asm("mfc0 %0,$9,0" :  "=r"(tmpLong));
    tmpLong = _CP0_GET_COUNT(); 

    return tmpLong;
}