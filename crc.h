/* 
 * File:   crc.h
 * Author: cls
 *
 * Created on June 23, 2015, 9:11 AM
 */

#ifndef CRC_H
#define	CRC_H



#define FAST_CRC                // Trade memory for improved performance
#define STORE_CRC_TABLE_IN_ROM  // Store CRC-8 table in ROM (Otherwise CRC table must be
                                // initialized first and stored in RAM).
#ifdef FAST_CRC
#ifndef STORE_CRC_TABLE_IN_ROM
void crc_table_init(void);
#endif
unsigned char crc8_fast(unsigned char *pData, unsigned int numBytes);
#endif
unsigned char crc8_slow(unsigned char *pData, unsigned int numBytes);



#endif	/* CRC_H */

