/* 
 * File:   can.h
 * Author: cls
 *
 * Created on June 24, 2015, 10:54 AM
 */

#ifndef CAN_H
#define	CAN_H



/*******************************************************************************
   Object: CO_CANrxMsg_t

   CAN receive message structure as aligned in CAN module. Object is passed to
   <Receive CAN message function>. In general, that function only uses _DLC_ and
   _data_ parameters. If it needs to read identifier, it must use function
   <CO_CANrxMsg_readIdent>.
   In PIC32MX this structure is used for both: transmitting and
   receiving to and from CAN module. (Object is ownded by CAN module).

   Variables:
      ident             - Standard Identifier.
      FILHIT            - Filter hit, see PIC32MX documentation.
      CMSGTS            - CAN message timestamp, see PIC32MX documentation.
      DLC               - Data length code (bits 0...3).
      RTR               - Remote Transmission Request bit.
      data              - 8 data bytes.
*******************************************************************************/
typedef struct{
   unsigned       ident    :11;
   unsigned       FILHIT   :5;
   unsigned       CMSGTS   :16;
   unsigned       DLC      :4;
   unsigned                :5;
   unsigned       RTR      :1;
   unsigned                :22;
   unsigned char  data[8];
}CO_CANrxMsg_t;



void init_CAN1_hardware(unsigned char nodeID);
//void CAN_write(void);
//unsigned char CAN_read(unsigned char *msg);
unsigned char CAN_RX_check(void);
void CAN_getMsg(unsigned int *msgId, unsigned char *msgDataLen, unsigned char *msgData);
//void CAN_sendMsg(unsigned int msgId, unsigned char msgDataLen, unsigned char *msgData);



#endif	/* CAN_H */

