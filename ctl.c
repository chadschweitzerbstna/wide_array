#include "ctl.h"


// Multidimensional arrays in C:
// array[row][column]
const unsigned char LINEAR_SLOPE[2][3] = {
    {108, 143, 136},    // 0 = Ultrasonic
    {90, 90, 90}        // 1 = Infrared
};

const unsigned char LINEAR_ORIGIN[2][3] = {
    {138, 131, 128},    // 0 = Ultrasonic
    {138, 138, 138}     // 1 = Infrared
};

/*
const unsigned char MAX_BEAM[33] = {
    1,      // 1.5x3U, 3x3U, 4x6U, 4x12U,   1.5x3L, 3x3L
    7,
    9,      //                              1.5x3.75L, 4x3.75L
    11,     // 1.5x4U
    15,     // 1.5x5U, 4x5U
    19,
    23,
    27,
    31,
    35,     // 4x9U
    39,
    43,
    47,
    51,
    55,
    59,     // 4x14U                        4x14L
    63,
    67,
    71,
    75,
    79,
    83,
    87,
    91,
    95,     // 4x21U
    99,
    103,
    107,
    111,
    115,
    119, 
    123,
    127
};
*/


typedef struct {
    unsigned char maxBeam;
    unsigned char gap;
    unsigned char type;
    unsigned char string[16];
} sensorString_t;



const sensorString_t SENSOR_STRING[33] = {
    1,      // 1.5x3U, 3x3U, 4x6U, 4x12U,   1.5x3L, 3x3L
    7,
    9,      //                              1.5x3.75L, 4x3.75L
    11,     // 1.5x4U
    15,     // 1.5x5U, 4x5U
    19,
    23,
    27,
    31,
    35,     // 4x9U
    39,
    43,
    47,
    51,
    55,
    59,     // 4x14U                        4x14L
    63,
    67,
    71,
    75,
    79,
    83,
    87,
    91,
    95,     // 4x21U
    99,
    103,
    107,
    111,
    115,
    119,
    123,
    127
};



// These three tables define the min, max and default transmit levels for the
// ult and led sensors:
const unsigned char     XMT_LEVEL_MIN[2] = {  1,   1};
const unsigned char     XMT_LEVEL_MAX[2] = { 10, 127};
                           // array[row][column]
const unsigned char XMT_LEVEL_DEFAULT[2][2] = { { 3, 10},
                                                { 5, 10} };

// This array defines the minimum guide points for each model of edge detector.
// In general, the min and max guide points are always 100 counts away from the min and max
// detector range.
/*
const unsigned int MIN_GUIDE_POINT[36] = {
    0x4000,
    0,
    0x4000 - 213,
    0x4000 - 32,        // 1.5x3U, 3x3U, 4x6U, 4x12U,   1.5x3L, 3x3L
    0x4000 - 400,
    0x4000 - 600,
    0x4000 - 800,
    0x4000 - 1200,      // 1.5x5U, 4x5U
    0x4000 - 1600,
    0x4000 - 2000,
    0x4000 - 2400,
    0x4000 - 2800,
    0x4000 - 3200,
    0x4000 - 3600,
    0x4000 - 4000,
    0x4000 - 4400,
    0x4000 - 4800,
    0x4000 - 5200,
    0x4000 - 5600,
    0x4000 - 6000,
    0x4000 - 6400,
    0x4000 - 6800,
    0x4000 - 7200,
    0x4000 - 7600,
    0x4000 - 8000,
    0x4000 - 8400,
    0x4000 - 8800,
    0x4000 - 9200,
    0x4000 - 9600,
    0x4000 - 10000,
    0x4000 - 10400,
    0x4000 - 10800,
    0x4000 - 11200,
    0x4000 - 11600,
    0x4000 - 12000,
    0x4000 - 12400
};
*/
// This array defines the maximum position for various models of edge detectors.
// The range is calculated as 1/2 the number of active beams (total beams minus 3)
// times 200 counts per beam.  This value is then added or subtracted from 0x4000 which is
// considered the center of the detector (guide point of 0.0")
/*
const unsigned int MAX_POSITION[36] = {
    0x4000,
    0x7fff,
    0x4000 + 312,
    0x4000 + 127,       // 1.5x3U, 3x3U, 4x6U, 4x12U,   1.5x3L, 3x3L
    0x4000 + 500,
    0x4000 + 700,
    0x4000 + 900,
    0x4000 + 1300,      // 1.5x5U (16 beams total).  Range is 0x4000 +/- ( (((16-3)/2) * 200) )
    0x4000 + 1700,
    0x4000 + 2100,
    0x4000 + 2500,
    0x4000 + 2900,
    0x4000 + 3300,      // 1.5x9U (36 beams total).  Range is 0x4000 +/- ( (((36-3)/2) * 200) )
    0x4000 + 3700,
    0x4000 + 4100,
    0x4000 + 4500,
    0x4000 + 4900,
    0x4000 + 5300,
    0x4000 + 5700,
    0x4000 + 6100,
    0x4000 + 6500,
    0x4000 + 6900,
    0x4000 + 7300,
    0x4000 + 7700,
    0x4000 + 8100,
    0x4000 + 8500,
    0x4000 + 8900,
    0x4000 + 9300,
    0x4000 + 9700,
    0x4000 + 10100,
    0x4000 + 10500,
    0x4000 + 10900,
    0x4000 + 11300,
    0x4000 + 11700,
    0x4000 + 12100,
    0x4000 + 12500
};
*/
// Constants for setting the MIN, MAX, and DEFAULT sensor parameters.
// {type, length, lengthMax, gap, orientation}
//const sensorConfig_t SENSOR_MAX = {2, 32, 32, 2, 1};
//const sensorConfig_t SENSOR_MIN = {0, 0, 0, 0, 0};
//const sensorConfig_t SENSOR_DEFAULT = {0, 2, 0, 1, 0};
