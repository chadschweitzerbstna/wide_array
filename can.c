#include <GenericTypeDefs.h>
#include <xc.h>
#include <proc/p32mz1024eff100.h>
#include <peripheral/CAN.h>
#include <../../../v1.33/pic32-libs/peripheral/can/source/CANTypes.h>
#include "CAN/Header/CO_OD.h"



#define CAN_CONFIG_MODE 4   //0b100
#define CAN_NORMAL_MODE 0



// 2 buffers * 16 messages * 16 bytes per message
//unsigned char CanFifoMessageBuffers[ (2 * 16 * 16) ];   // 512 bytes total.

// Space for the FIFO message buffers
// FIFO 0, TX, 4 words per message, 8 messages per buffer, 32 words total.
// FIFO 1: RX, 4 words per message, 32 messages per buffer, 128 words total.

// Total space needed = 32 + 128 = 160 words.
unsigned int CanFifoMessageBuffers[160];



// ------------------------------------------------------------------------------------------------
//
void init_CAN1_hardware(unsigned char nodeID)
{
    // If no nodeID is assigned, assign the default node ID.
    // The nodeID is used when configuring the CAN message filters is Step 4 below. 
    if (nodeID == 0)
        nodeID = OD_CANNodeID;

    // <editor-fold defaultstate="collapsed" desc="Step 1: Switch the CAN module ON and set it to Configuration mode">
    //C1CON = 0x00000000;                         // Clear register
    //C1CONbits.ON = 1;                           // Enable CAN module.
    //while(C1CONbits.CANBUSY == 1) {;;}          // Wait.
    //C1CONbits.REQOP = CAN_CONFIG_MODE;          // Set CAN module to Configuration mode.
    //while(C1CONbits.OPMOD != CAN_CONFIG_MODE);  // Wait for mode switch to complete.
    C1CON = 0x04008000;
    while(C1CONbits.OPMOD != CAN_CONFIG_MODE)   // Wait for mode switch to complete.
        ;;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Step 2: Configure the CAN Module Clock">
    // Step 2: Configure the CAN Module Clock.
    // C1CFG was calculated using the Microchip ECAN Bit Rate Calculator plug-in for MPLAB X.
    // Options are as follows:
    //  - CAN bus line filter is not used for wake-up (WAKFIL = 0).
    //  - Bus line is sampled once at the sample point (SAM = 0).
    //  - Phase Segment 2 Time is freely programmable (SEG2PHTS = 1).
    //  - Time Quantum (TQ) = 0.10us
    //  - Propagation Segment = 8xTQ (PRSEG = 7)
    //  - Phase Segment 1 = 8xTQ (SEG1PH = 7)
    //  - Phase Segment 2 = 3xTQ (SEG2PH = 2)
    //  - Synchronization Jump Width = 3xTQ (SJW = 2)
    //  - Desired Bit Rate = 500 kbps
    //  - Bit Rate Prescaler = 3 (BRP = 3) (Bit rate error = 0.00%)
    C1CFG = 0x0002BF83;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Step 3: Initialize FIFO registers and addresses">
    // Step 3: Initialize C1FIFOBA registers with physical address of the CAN FIFO message buffers.
    // Each CAN module should have its own message area.
    // C1FIFOBA: CAN Message Buffer Base Address Register.  This address is a physical address.
    // Bits <1:0> are read-only and read ?0?, forcing the messages to be 32-bit word-aligned in device RAM
    C1FIFOBA = KVA_TO_PA(CanFifoMessageBuffers); // Set CAN1 FIFO Base Address to the allocated memory.
    
    // Set FIFO_0 size to 8 messages and for TX
    C1FIFOCON0bits.FSIZE = 7;       // 8 messages
    C1FIFOCON0bits.TXEN = 1;        // TX channel

    // Set FIFO_1 size to 32 messages and for RX
    C1FIFOCON1bits.FSIZE = 31;      // 32 messages
    C1FIFOCON1bits.TXEN = 0;        // RX channel
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Step 4: Configure filters and masks">
 
    // MASK 0:  Match every bit in SID
    C1RXM0bits.SID = 0x7FF;
    C1RXM0bits.MIDE = 1;        // Match only message types

    // *** NOTE: Filters must be disabled before the corresponding filter bits can be modified ***

    // Set up filter 0 for messages from the NMT master.
    C1FLTCON0bits.FLTEN0 = 0;   // Disable Filter 0
    C1FLTCON0bits.FSEL0 = 1;    // Store messages in FIFO1
    C1FLTCON0bits.MSEL0 = 0;    // Use Mask 0
    C1RXF0bits.SID = 0x000;     // Filter 0 SID
    C1RXF0bits.EXID = 0;        // Filter only SID messages
    C1FLTCON0bits.FLTEN0 = 1;   // Enable Filter 0

    // Set up filter 1 for SYNC messages.
    C1FLTCON0bits.FLTEN1 = 0;   // Disable Filter 1
    C1FLTCON0bits.FSEL1 = 1;    // Store messages in FIFO1
    C1FLTCON0bits.MSEL1 = 0;    // Use Mask 0
    C1RXF1bits.SID = 0x080;     // Filter 1 SID
    C1RXF1bits.EXID = 0;        // Filter only SID messages
    C1FLTCON0bits.FLTEN1 = 1;   // Enable Filter 1

    // Set up filter 2 for LSS master messages.
    C1FLTCON0bits.FLTEN2 = 0;   // Disable Filter 2
    C1FLTCON0bits.FSEL2 = 1;    // Store messages in FIFO1
    C1FLTCON0bits.MSEL2 = 0;    // Use Mask 0
    C1RXF2bits.SID = 0x7E5;     // Filter 2 SID
    C1RXF2bits.EXID = 0;        // Filter only SID messages
    C1FLTCON0bits.FLTEN2 = 1;   // Enable Filter 2
   
    //  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // MASK 1:  Match only the node ID
    C1RXM1bits.SID = 0x0FF;
    C1RXM1bits.MIDE = 1;        // Match only message types

    // *** NOTE: Filters must be disabled before the corresponding filter bits can be modified ***

    // Set up filter 4 using MASK 1 to allow all PDOs and SDOs for our device.
    C1FLTCON0bits.FLTEN3 = 0;   // Disable Filter 3
    C1FLTCON0bits.FSEL3 = 1;    // Store messages in FIFO1
    C1FLTCON0bits.MSEL3 = 1;    // Use Mask 1
    C1RXF3bits.SID = nodeID;    // Filter 3 SID (our current node ID)
    C1RXF3bits.EXID = 0;        // Filter only SID messages
    C1FLTCON0bits.FLTEN3 = 1;   // Enable Filter 3

    //  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // MASK 2:  Match only the High Byte (for MUX PDO RX)
    C1RXM2bits.SID = 0x700;
    C1RXM2bits.MIDE = 1;        // Match only message types

    // *** NOTE: Filters must be disabled before the corresponding filter bits can be modified ***

    // Set up filter 4 for the ekr500 digital MUX PDO.
    C1FLTCON1bits.FLTEN4 = 0;   // Disable Filter 4
    C1FLTCON1bits.FSEL4 = 1;    // Store messages in FIFO1
    C1FLTCON1bits.MSEL4 = 2;    // Use Mask 2
    C1RXF4bits.SID = 0x500;     // Filter 4 SID (ekr500 digital MUX PDO)
    C1RXF4bits.EXID = 0;        // Filter only SID messages
    C1FLTCON1bits.FLTEN4 = 1;   // Enable Filter 4

    //  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/*
    // MASK 3:  Everything gets through (for DEBUG only)
    C1RXM3bits.SID = 0x000;     // Nothing has to match.
    C1RXM3bits.MIDE = 1;

    // Filter 5
    C1FLTCON1bits.FSEL5 = 1;    // Store messages in FIFO1
    C1FLTCON1bits.MSEL5 = 3;    // Use Mask 3
    C1RXF5bits.SID = 0xFFF;     // Filter 5 SID
    C1RXF5bits.EXID = 0;        // Filter only SID messages
    C1FLTCON1bits.FLTEN5 = 1;   // Enable Filter 5
*/
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Step 5: Enable interrupts and events">
    // Configure CAN1 first layer interrupts.
    IEC1bits.CAN1IE = 0;    // CAN1 interrupts disabled at the moment (will be enabled later).
    IFS1bits.CAN1IF = 0;    // Clear interrupt flag bit
    IPC11bits.CAN1IP = 5;   // CAN1 interrupt priority 5
    
    // Configure CAN1 second layer interrupts.
    C1INT = 0x0000;         // Clear all CAN1 interrupt bits
    //C1INTbits.TBIE = 1;     // Enable CAN1 TX Buffer Interrupt.
    //C1INTbits.RBIE = 1;     // Enable CAN1 RX Buffer Interrupt.
    C1INTbits.RBOVIE = 1;   // Enable CAN1 RX Overflow Interrupt.

    // Configure CAN1 third layer (FIFO) interrupts.
    C1FIFOINT0 = 0x0000;            // Clear all CAN1 FIFO 0 interrupt bits
    //C1FIFOINT0bits.TXEMPTYIE = 0;   // Enable FIFO_0 TX Buffer Empty interrupt.
    C1FIFOINT1 = 0x0000;            // Clear all CAN1 FIFO 1 interrupt bits
    //C1FIFOINT1bits.RXOVFLIE = 1;    // Enable FIFO_1 RX overflow interrupt.

    // Enable CAN1 interrupts.
    IEC1bits.CAN1IE = 1;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Step 6: Switch the CAN module to normal mode">
    // Place CAN1 module in normal mode
    C1CONbits.REQOP = CAN_NORMAL_MODE;
    while (C1CONbits.OPMOD != CAN_NORMAL_MODE)
        ;; // wait for module to enter mode
    // </editor-fold>

}


/*
// ------------------------------------------------------------------------------------------------
//
void CAN_sendMsg(unsigned int msgId, unsigned char msgDataLen, unsigned char *msgData)
{
    CANTxMessageBuffer *txMessage;      // pointer to CAN TX message buffer
    unsigned char i;

    // Disable interrupts
    __builtin_disable_interrupts();

    // If prevous message has been sent...
    //if ( (C1FIFOCON0 & 0x08) == 0 )
    //{
        // Get the address of the CAN message buffer.
        txMessage = (CANTxMessageBuffer *) (PA_TO_KVA1(C1FIFOUA0));

        // Clear the FIFO at the location returned to us
        txMessage->messageWord[0] = 0;
        txMessage->messageWord[1] = 0;
        txMessage->messageWord[2] = 0;
        txMessage->messageWord[3] = 0;

        // Copy the message to be sent.
        // First validate the message length.
        if (msgDataLen > 8)
            msgDataLen = 8;

        // Copy the message data
        for (i=0; i<msgDataLen; i++)
            txMessage->data[i] = *(msgData+i);

        // Copy the message ID
        txMessage->msgSID.SID = msgId;
        txMessage->msgEID.IDE = 0;

        // Copy the data length
        txMessage->msgEID.DLC = msgDataLen;

        // Send the message
        C1FIFOCON0SET = 0x2000; //same as C1FIFOCON0bits.UINC = 1; // why doesn't this work? bc microchip sucks
        C1FIFOCON0SET = 0x8; //same as C1FIFOCON0bits.TXREQ = 1;
    //}

    // Enable interrupts
    __builtin_enable_interrupts();


}
*/
/*
// ------------------------------------------------------------------------------------------------
//
void CAN_write(void)
{
    // get the address of the message buffer to write to
    // clear the memory
    // load the settings and data and set the UINC bit
    // set the TXREQ bit to send the message
    //   Msg 0
    transmitMessage = (CANTxMessageBuffer *) (PA_TO_KVA1(C1FIFOUA0));
    transmitMessage->messageWord[0] = 0;
    transmitMessage->messageWord[1] = 0;
    transmitMessage->messageWord[2] = 0;
    transmitMessage->messageWord[3] = 0;
    //transmitMessage->msgSID.SID = 0x100; // Message SID
    transmitMessage->msgSID.SID = 0x733;
    transmitMessage->msgEID.IDE = 0;
    //transmitMessage->msgEID.DLC = 0x8; // Data Length is 8 bytes
    transmitMessage->msgEID.DLC = 0x1;
    //transmitMessage->data[0] = i++; // first byte
    transmitMessage->data[0] = 0; // first byte
    C1FIFOCON0SET = 0x2000; //same as C1FIFOCON0bits.UINC = 1; // why doesn't this work? bc microchip sucks
    C1FIFOCON0SET = 0x8; //same as C1FIFOCON0bits.TXREQ = 1;
}
*/

// ------------------------------------------------------------------------------------------------
//
void CAN_getMsg(unsigned int *msgId, unsigned char *msgDataLen, unsigned char *msgData)
{
    CANRxMessageBuffer *rxMessage;      // pointer to CAN RX message buffer
    unsigned int tmpInt = 0;
    unsigned char i, length, tmpChar;

    // check if there is a message to read on FIFO1
    //if (C1FIFOINT1bits.RXNEMPTYIF == 1)
    //{
        // get the address of the buffer to read
        rxMessage = PA_TO_KVA1(C1FIFOUA1);

        // Copy ID
        tmpInt = rxMessage->msgSID.SID;
        *msgId = tmpInt;

        // Copy Data Length
        length = rxMessage->msgEID.DLC;
        *msgDataLen = length;

        // Copy Data
        for (i=0; i<length; i++)
        {
            tmpChar = rxMessage->data[i];
            *(msgData+i) = tmpChar;
        }

        // update message buffer pointer
        C1FIFOCON1SET = 0x2000; //C1FIFOCON0bits.UINC = 1; // why doesn't this work? bc microchip sucks
        
        //return 1;
    //}
    //else
    //    return 0;
}


/*
// ------------------------------------------------------------------------------------------------
//
unsigned char CAN_read(unsigned char *msg)
{
    // check if there is a message to read on FIFO0
    if (C1FIFOINT0bits.RXNEMPTYIF == 1)
    {
        // get the address of the buffer to read
        rxMessage = PA_TO_KVA1(C1FIFOUA0);

        // update message buffer pointer
        C1FIFOCON0SET = 0x2000; //C1FIFOCON0bits.UINC = 1; // why doesn't this work? bc microchip sucks

        // send the data in the message to the computer, not all the bytes have data tho
        sprintf(msg, "ID: %d, DLC: %d, DATA: %d %d %d %d\r\n", rxMessage->msgSID.SID, rxMessage->msgEID.DLC, rxMessage->data[0], rxMessage->data[1], rxMessage->data[2], rxMessage->data[3]);

        return 1;
    }
    else
        return 0;
}
*/

// ------------------------------------------------------------------------------------------------
//
unsigned char CAN_RX_check(void)
{
    // check if there is a message to read on FIFO1
    if (C1FIFOINT1bits.RXNEMPTYIF == 1)
        return 1;
    else
        return 0;
}
