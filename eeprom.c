#include <proc/p32mz1024eff100.h>
#include "eeprom.h"
#include "main.h"
#include "delay.h"
#include "timer.h"


// EEPROM instruction set
#define SPI_EE_READ         0x03    // Read data
#define SPI_EE_WRITE        0x02    // Write data
#define SPI_EE_WREN         0x06    // Set write enable latch

// EEPROM SPI pins
#define SPI_EE_CS           EECS    // Output: SPI EEPROM Chip Select


// Type and structure declarations.
typedef enum {
    EEPROM_IDLE = 0,
    EEPROM_READ,
    EEPROM_WRITE_INIT,
    EEPROM_WRITE_PAGE,
    EEPROM_WRITE_WAIT,
    EEPROM_FINAL_WRITE_WAIT
} eepromState_t;

typedef struct {
    eepromState_t  state;
    eepromState_t  lastState;
    unsigned char  command;
    unsigned char *pRAM;
    unsigned int   eeStartAddr;
    unsigned int   numBytes;
    unsigned int   byteIndex;
} eepromTask_t;

typedef enum {
    EEUPDATE_IDLE = 0,
    EEUPDATE_READ,
    EEUPDATE_COMPARE,
    EEUPDATE_WRITE
} eeUpdateState_t;

typedef struct {
    eeUpdateState_t state;
    eeUpdateState_t lastState;
    unsigned char   command;
    unsigned char  *pRAM;
    unsigned int    eeStartAddr;
    unsigned int    numBytes;
    unsigned int    byteIndex;
    unsigned char   eeByte;
} eeUpdateTask_t;


// File-scope variables
static unsigned char EEPROM_busy = 0;
static eepromTask_t eepromTask;
//static eeUpdateTask_t eeUpdateTask;


// File-scope functions
void delay_for_20_nops(void);


// ------------------------------------------------------------------------------------------------
//
void init_eeprom_task(void)
{
    eepromTask.state = EEPROM_IDLE;
    eepromTask.lastState = EEPROM_IDLE;
}


// ------------------------------------------------------------------------------------------------
//  Reads data from the external EEPROM chip.  Returns the number of bytes read.
//  Returns 0 if EEPROM is busy.
//
void read_eeprom(unsigned char *pDest, unsigned int eepromStartAddr, unsigned int numBytes)
{
    unsigned char tmpByte;
    unsigned int addr, i;

    // Check if SPI EEPROM is busy
    if (EEPROM_busy)
        return;  // Error - previous EEPROM operation still in progress.

    // Flag EEPROM as BUSY
    EEPROM_busy = 1;

    // Clear Buffer Full (SSP2STATbits.BF) flag by reading SPI data register
    tmpByte = SPI3BUF;

    // Select the EEPROM chip.
    SPI_EE_CS = 0;

    // Wait at least 250ns for Chip Select set-up time.
    delay_for_20_nops();

    // Initalize the address from which to start reading
    addr = eepromStartAddr;

    // Send SPI EEPROM read command.
    SPI3BUF = SPI_EE_READ;
    while (SPI3STATbits.SPIRBF == 0) {;;}
    tmpByte = SPI3BUF;

    // Send EEPROM address high byte.
    SPI3BUF = (addr >> 8) & 0xFF;
    while (SPI3STATbits.SPIRBF == 0) {;;}
    tmpByte = SPI3BUF;

    // Send EEPROM address low byte.
    SPI3BUF = (addr & 0xFF);
    while (SPI3STATbits.SPIRBF == 0) {;;}
    tmpByte = SPI3BUF;

    // Read data bytes sequentually
    for (i=0; i<numBytes; i++)
    {
        // Send "0x00" to get byte from EEPROM.
        SPI3BUF = 0x00;
        while (SPI3STATbits.SPIRBF == 0) {;;}
        *(pDest + i) = SPI3BUF;  // Data from EEPROM, written to destination
    }

    // Deselect the EEPROM chip.
    SPI_EE_CS = 1;

    // Clear EEPROM busy flag.
    EEPROM_busy = 0;
}


// ------------------------------------------------------------------------------------------------
//  Quickly write sequential data to external EEPROM.
//  Data can be a single byte, or many bytes across multiple pages if necessary.
//  WriteReturns the number of bytes written.
//  Returns 0 if EEPROM is busy.
//
void write_eeprom(unsigned char *pSource, unsigned int eepromStartAddr, unsigned int numBytes)
{
    unsigned char tmpByte;
    unsigned int addr, i;
    unsigned int newPageAddr;

    // Check if SPI EEPROM is busy
    if (EEPROM_busy)
        return;  // Error - previous EEPROM operation still in progress.

    // Flag EEPROM as BUSY
    EEPROM_busy = 1;

    // Clear Buffer Full flag by reading SPI data register
    tmpByte = SPI3BUF;

    // Write the first address and continue to write until we reach the end of a page.
    // When we reach the end of the page, de-select the chip to save data.
    // Then issue a new write enble command followed by more bytes until all writes are complete.
    i = 0;
    while (i < numBytes)
    {
        // Current Address (Max number of bytes = 8192 for 25xx640)
        addr = eepromStartAddr + i;

        // Get the boundary of the current EEPROM page based on the current address.
        // A page address begins with XXX0 0000 and ends with XXX1 1111 (page = 32 bytes).
        newPageAddr = (addr & 0xFFE0) + 0x20;

        // Select the EEPROM chip.
        SPI_EE_CS = 0;   // Chip select (active low).

        // Wait at least 250ns for Chip Select set-up time.
        delay_for_20_nops();

        // Enable EEPROM write
        SPI3BUF = SPI_EE_WREN;
        while (SPI3STATbits.SPIRBF == 0) {;;}
        tmpByte = SPI3BUF;

        // Deselect the EEPROM chip.
        SPI_EE_CS = 1;   // Chip select (active low).

        // Wait
        delay_us(WR_ENABLE_DELAY);

        // Select the EEPROM chip.
        SPI_EE_CS = 0;   // Chip select (active low).

        // Wait at least 250ns for Chip Select set-up time.
        delay_for_20_nops();

        // Send SPI EEPROM write command.
        SPI3BUF = SPI_EE_WRITE;
        while (SPI3STATbits.SPIRBF == 0) {;;}
        tmpByte = SPI3BUF;

        // Send EEPROM address high byte.
        SPI3BUF = (addr >> 8) & 0xFF;
        while (SPI3STATbits.SPIRBF == 0) {;;}
        tmpByte = SPI3BUF;

        // Send EEPROM address low byte.
        SPI3BUF = addr & 0xFF;
        while (SPI3STATbits.SPIRBF == 0) {;;}
        tmpByte = SPI3BUF;

        // Write until we reach the end of the page (max 32 bytes) or run out of bytes to write.
        do {
            // Write byte to EEPROM.
            SPI3BUF = *(pSource + i);
            while (SPI3STATbits.SPIRBF == 0) {;;}
            tmpByte = SPI3BUF;

            // Increment byte count and address
            i++;
            addr++;
        } while ( (addr != newPageAddr) && (i < numBytes) );

        // Deselect the EEPROM chip (writes page data).
        SPI_EE_CS = 1;  // (active low)

        // Wait 5ms for write to execute in EEPROM
        // Initialize wait timer.
        timers[EEPROM_WRITE_TIMER].reset = 1;
        timers[EEPROM_WRITE_TIMER].done = 0;    // Clear time-out flag
        // Wait for write time to expire.
        while (timers[EEPROM_WRITE_TIMER].done != 1)
        {;;}
    }

    // Clear EEPROM busy flag.
    EEPROM_busy = 0;
}


// ------------------------------------------------------------------------------------------------
//  Cycle through bytes in EEPROM and compare to bytes in RAM (*pSource).
//  If they are different then write the value in EEPROM to match the value in RAM.
//
void update_eeprom(unsigned char *pSource, unsigned int eepromStartAddr, unsigned int numBytes)
{
    unsigned char tmpByte;
    unsigned int addr, i;

    // Cycle through bytes and compare.
    i = 0;
    while (i<numBytes)
    {
        // Read a byte at the current address.
        addr = eepromStartAddr + i;
        read_eeprom(&tmpByte, addr, 1);

        // If the byte in EEPROM is different then the byte in RAM, update the EEPROM value.
        if (tmpByte != *pSource)
            write_eeprom(pSource, addr, 1);

        // Increment address and byte count.
        i++;
        pSource++;
    }
}


// ------------------------------------------------------------------------------------------------
//  Wait for at least 250ns ( 250ns * 80MHz = 20 )
//
void delay_for_20_nops(void)
{
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
}


// ------------------------------------------------------------------------------------------------
//
void start_eeprom_write_task(unsigned char *pSource, unsigned int eeStartAddr, unsigned int numBytes)
{
    if (eepromTask.state != EEPROM_IDLE)
        return;     // Don't do anything if an EEPROM operation is already in process.

    // Set the various address and data length.
    eepromTask.pRAM = pSource;
    eepromTask.eeStartAddr = eeStartAddr;
    eepromTask.numBytes = numBytes;

    // Reset the byte index.
    eepromTask.byteIndex = 0;

    // Start EEPROM write.
    eepromTask.state = EEPROM_WRITE_INIT;
}


// ------------------------------------------------------------------------------------------------
//
void do_eeprom_task(void)
{
    unsigned char tmpByte;
    unsigned int addr, newPageAddr;

    switch (eepromTask.state)
    {
        case EEPROM_IDLE:   // Nothing to do.
            eepromTask.lastState = eepromTask.state;
            break;


        case EEPROM_READ:   // Read data from external eeprom.
            if (eepromTask.state != eepromTask.lastState)
            {
                // State change - this code is only executed once when arriving at this state.
                eepromTask.lastState = eepromTask.state;

                // *** Set-up the external EEPROM read ***

                // Clear Buffer Full (SSP2STATbits.BF) flag by reading SPI data register
                tmpByte = SPI3BUF;

                // Select the EEPROM chip.
                SPI_EE_CS = 0;

                // Wait at least 250ns for Chip Select set-up time.
                delay_for_20_nops();

                // Initalize the address from which to start reading
                addr = eepromTask.eeStartAddr;

                // Send SPI EEPROM read command.
                SPI3BUF = SPI_EE_READ;
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Send EEPROM address high byte.
                SPI3BUF = (addr >> 8) & 0xFF;
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Send EEPROM address low byte.
                SPI3BUF = (addr & 0xFF);
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Reset index
                eepromTask.byteIndex = 0;

                // Load the first dummy byte (0x00) to SPI register to start the SPI read/write.
                SPI3BUF = 0x00;
            }   // *** Read set-up done ***

            // Insure that byte send/receive operation has completed.
            while (SPI3STATbits.SPIRBF == 0) {;;}

            // Copy incomming byte from SPI register to destination address in RAM.
            *(eepromTask.pRAM + eepromTask.byteIndex) = SPI3BUF;

            // Increment index
            eepromTask.byteIndex++;

            // Check if there are more bytes to read.
            if (eepromTask.byteIndex < eepromTask.numBytes)
                SPI3BUF = 0;    // Do another SPI read/write.
            else
            {   // *** Bytes have all been read ***
                // Deselect the EEPROM chip.
                SPI_EE_CS = 1;

                // Set state machine to IDLE state.
                eepromTask.state = EEPROM_IDLE;
            }
            break;


        case EEPROM_WRITE_INIT:  // Write data to external eeprom.
            if (eepromTask.state != eepromTask.lastState)
            {
                // State change - this code is only executed once when arriving at this state.
                eepromTask.lastState = eepromTask.state;

                // *** Set-up the external EEPROM write ***

                // Clear Buffer Full flag by reading SPI data register
                tmpByte = SPI3BUF;

                // Reset index
                eepromTask.byteIndex = 0;
            }

            // Start writing (one byte or many bytes)
            eepromTask.state = EEPROM_WRITE_PAGE;
            break;


        case EEPROM_WRITE_PAGE:  // Write data to external eeprom.
            if (eepromTask.state != eepromTask.lastState)
            {
                // State change - this code is only executed once when arriving at this state.
                eepromTask.lastState = eepromTask.state;

                // Current Address (Max number of bytes = 8192 for 25xx640)
                addr = eepromTask.eeStartAddr + eepromTask.byteIndex;

                // Select the EEPROM chip.
                SPI_EE_CS = 0;   // Chip select (active low).

                // Wait at least 250ns for Chip Select set-up time.
                delay_for_20_nops();

                // Enable EEPROM write
                SPI3BUF = SPI_EE_WREN;
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Deselect the EEPROM chip.
                SPI_EE_CS = 1;   // Chip select (active low).

                // Wait for EEPROM write enable to complete.
                delay_us(WR_ENABLE_DELAY);

                // Select the EEPROM chip again.
                SPI_EE_CS = 0;

                // Wait at least 250ns for Chip Select set-up time.
                delay_for_20_nops();

                // Send SPI EEPROM write command.
                SPI3BUF = SPI_EE_WRITE;
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Send EEPROM address high byte.
                SPI3BUF = (addr >> 8) & 0xFF;
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Send EEPROM address low byte.
                SPI3BUF = addr & 0xFF;
                while (SPI3STATbits.SPIRBF == 0) {;;}
                tmpByte = SPI3BUF;

                // Send the first byte for this particular page in EEPROM
                SPI3BUF = *(eepromTask.pRAM + eepromTask.byteIndex);
            }

            // Insure that byte send/receive operation has completed.
            while (SPI3STATbits.SPIRBF == 0) {;;}

            // Dummy read to clear the SPIRBF flag.
            tmpByte = SPI3BUF;

            // Increment the byte index.
            eepromTask.byteIndex++;

            // Get the boundary of the current EEPROM page based on the current address.
            // A page address begins with XXX0 0000 and ends with XXX1 1111 (page = 32 bytes).
            addr = eepromTask.eeStartAddr + eepromTask.byteIndex;
            newPageAddr = (addr & 0xFFE0) + 0x20;

            // Write until we reach the end of the page (max 32 bytes) or run out of bytes to write.
            if (addr == newPageAddr)
            {
                // End of page - pause the routine to allow EEPROM to save data, then continue.

                // Deselect the EEPROM chip (writes page data).
                SPI_EE_CS = 1;  // (active low)

                // Initialize wait timer.
                timers[EEPROM_WRITE_TIMER].reset = 1;
                timers[EEPROM_WRITE_TIMER].done = 0;    // Clear time-out flag

                // Switch to wait state.
                eepromTask.state = EEPROM_WRITE_WAIT;
            }
            else if (eepromTask.byteIndex == eepromTask.numBytes)
            {
                // No more bytes to write - allow the EEPROM to save data, then exit.

                // Deselect the EEPROM chip (writes page data).
                SPI_EE_CS = 1;  // (active low)

                // Initialize wait timer.
                timers[EEPROM_WRITE_TIMER].reset = 1;
                timers[EEPROM_WRITE_TIMER].done = 0;    // Clear time-out flag

                // Switch to final wait state.
                eepromTask.state = EEPROM_FINAL_WRITE_WAIT;
            }
            else
                SPI3BUF = *(eepromTask.pRAM + eepromTask.byteIndex);    // Send another byte
            break;


        case EEPROM_WRITE_WAIT:  // Write data to external eeprom.
            // Wait for external eeprom data write to complete.
            if (eepromTask.state != eepromTask.lastState)
                eepromTask.lastState = eepromTask.state;

            // Check if time has expired.  If so, EEPROM write is now complete.
            if (timers[EEPROM_WRITE_TIMER].done == 1)
                eepromTask.state = EEPROM_WRITE_PAGE;
            break;


        case EEPROM_FINAL_WRITE_WAIT:
            // Wait for external eeprom data write to complete.
            if (eepromTask.state != eepromTask.lastState)
                eepromTask.lastState = eepromTask.state;

            // Check if time has expired.  If so, EEPROM write is now complete.
            if (timers[EEPROM_WRITE_TIMER].done == 1)
                eepromTask.state = EEPROM_IDLE;
            break;


        default:    // Unknown state - switch to IDLE.
            eepromTask.state = EEPROM_IDLE;
            break;
    }

}


// ------------------------------------------------------------------------------------------------
//
unsigned char eepromTaskIsIdle(void)
{
    if (eepromTask.state == EEPROM_IDLE)
        return 1;
    else
        return 0;
}