/* 
 * File:   calibrate.h
 * Author: cls
 *
 * Created on June 23, 2015, 11:25 AM
 */

#ifndef CALIBRATE_H
#define	CALIBRATE_H


#define RCV_TARGET_VALUE            204 // The nominal target receive signal strength for most sensors (80% of 255).
#define MODIFIED_RCV_TARGET_VALUE   204 // The target receive signal strength for 4" gap IR sensor (80% of 255).


// States for the sensor calibration process
enum {
    CAL_IDLE = 0,               // --> Normal operation - no calibration occuring
                                // _
    INIT_CAL_UNBLOCKED,         //  |
    GET_COURSE_UNBLOCKED_VALUE, //  |--> Sensor UN-BLOCKED calibration routines
    GET_FINE_UNBLOCKED_VALUE,   //  |
    SELECT_NEXT_BEAM,           // _|
                                // _
    INIT_CAL_BLOCKED,           //  |
    STREAM_BLOCKED_VALUE,       //  |--> Sensor BLOCKED calibration routine
    GET_BLOCKED_VALUE,          // _|
} calState, lastCalState;


unsigned int CalibrationProgress;

unsigned int CalibrationValueMin,
             CalibrationValueMax,
             CalibrationValue;

void calibration_task(void);



#endif	/* CALIBRATE_H */

