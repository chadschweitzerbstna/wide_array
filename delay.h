/* 
 * File:   delay.h
 * Author: cls
 *
 * Created on June 23, 2015, 8:55 AM
 */

#ifndef DELAY_H
#define	DELAY_H



void delay_us(unsigned int d);
void delay_ms(unsigned int d);
void zero_usTimer(void);
unsigned int get_usTimer(void);



#endif	/* DELAY_H */

